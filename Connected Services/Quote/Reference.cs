﻿namespace ServiceReference1
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5", ConfigurationName="ServiceReference1.SSFConnectPortSoap")]
    public interface SSFConnectPortSoap
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="https://www.ssfparts.com/ssfconnect-v1.5:QuoteSoapIn", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<ServiceReference1.QuoteResponse1> QuoteAsync(ServiceReference1.QuoteRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="https://www.ssfparts.com/ssfconnect-v1.5:CreatePurchaseOrderSoapIn", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<ServiceReference1.CreatePurchaseOrderResponse1> CreatePurchaseOrderAsync(ServiceReference1.CreatePurchaseOrderRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class Quote
    {
        
        private string addRequestforQuoteBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string AddRequestforQuoteBOD
        {
            get
            {
                return this.addRequestforQuoteBODField;
            }
            set
            {
                this.addRequestforQuoteBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(IPOCreatePurchaseOrderConfirmResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(IPOCreatePurchaseOrderAcknowledgePurchaseOrderResponse))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOCreatePurchaseOrderResponse
    {
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOCreatePurchaseOrderConfirmResponse : IPOCreatePurchaseOrderResponse
    {
        
        private string confirmBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ConfirmBOD
        {
            get
            {
                return this.confirmBODField;
            }
            set
            {
                this.confirmBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOCreatePurchaseOrderAcknowledgePurchaseOrderResponse : IPOCreatePurchaseOrderResponse
    {
        
        private string acknowledgePurchaseOrderBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string AcknowledgePurchaseOrderBOD
        {
            get
            {
                return this.acknowledgePurchaseOrderBODField;
            }
            set
            {
                this.acknowledgePurchaseOrderBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class CreatePurchaseOrderResponse
    {
        
        private IPOCreatePurchaseOrderResponse createPurchaseOrderResultField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public IPOCreatePurchaseOrderResponse CreatePurchaseOrderResult
        {
            get
            {
                return this.createPurchaseOrderResultField;
            }
            set
            {
                this.createPurchaseOrderResultField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class CreatePurchaseOrder
    {
        
        private string processPurchaseOrderBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ProcessPurchaseOrderBOD
        {
            get
            {
                return this.processPurchaseOrderBODField;
            }
            set
            {
                this.processPurchaseOrderBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(IPOQuoteAddQuoteResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(IPOQuoteConfirmResponse))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOQuoteResponse
    {
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOQuoteAddQuoteResponse : IPOQuoteResponse
    {
        
        private string addQuoteBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string AddQuoteBOD
        {
            get
            {
                return this.addQuoteBODField;
            }
            set
            {
                this.addQuoteBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class IPOQuoteConfirmResponse : IPOQuoteResponse
    {
        
        private string confirmBODField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ConfirmBOD
        {
            get
            {
                return this.confirmBODField;
            }
            set
            {
                this.confirmBODField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5")]
    public partial class QuoteResponse
    {
        
        private IPOQuoteResponse quoteResultField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public IPOQuoteResponse QuoteResult
        {
            get
            {
                return this.quoteResultField;
            }
            set
            {
                this.quoteResultField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class QuoteRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5", Order=0)]
        public ServiceReference1.Quote Quote;
        
        public QuoteRequest()
        {
        }
        
        public QuoteRequest(ServiceReference1.Quote Quote)
        {
            this.Quote = Quote;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class QuoteResponse1
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5", Order=0)]
        public ServiceReference1.QuoteResponse QuoteResponse;
        
        public QuoteResponse1()
        {
        }
        
        public QuoteResponse1(ServiceReference1.QuoteResponse QuoteResponse)
        {
            this.QuoteResponse = QuoteResponse;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class CreatePurchaseOrderRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5", Order=0)]
        public ServiceReference1.CreatePurchaseOrder CreatePurchaseOrder;
        
        public CreatePurchaseOrderRequest()
        {
        }
        
        public CreatePurchaseOrderRequest(ServiceReference1.CreatePurchaseOrder CreatePurchaseOrder)
        {
            this.CreatePurchaseOrder = CreatePurchaseOrder;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class CreatePurchaseOrderResponse1
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="https://www.ssfparts.com/ssfconnect-v1.5", Order=0)]
        public ServiceReference1.CreatePurchaseOrderResponse CreatePurchaseOrderResponse;
        
        public CreatePurchaseOrderResponse1()
        {
        }
        
        public CreatePurchaseOrderResponse1(ServiceReference1.CreatePurchaseOrderResponse CreatePurchaseOrderResponse)
        {
            this.CreatePurchaseOrderResponse = CreatePurchaseOrderResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface SSFConnectPortSoapChannel : ServiceReference1.SSFConnectPortSoap, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class SSFConnectPortSoapClient : System.ServiceModel.ClientBase<ServiceReference1.SSFConnectPortSoap>, ServiceReference1.SSFConnectPortSoap
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public SSFConnectPortSoapClient() : 
                base(SSFConnectPortSoapClient.GetDefaultBinding(), SSFConnectPortSoapClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.SSFConnectPortSoap.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public SSFConnectPortSoapClient(EndpointConfiguration endpointConfiguration) : 
                base(SSFConnectPortSoapClient.GetBindingForEndpoint(endpointConfiguration), SSFConnectPortSoapClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public SSFConnectPortSoapClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(SSFConnectPortSoapClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public SSFConnectPortSoapClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(SSFConnectPortSoapClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public SSFConnectPortSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.QuoteResponse1> ServiceReference1.SSFConnectPortSoap.QuoteAsync(ServiceReference1.QuoteRequest request)
        {
            return base.Channel.QuoteAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.QuoteResponse1> QuoteAsync(ServiceReference1.Quote Quote)
        {
            ServiceReference1.QuoteRequest inValue = new ServiceReference1.QuoteRequest();
            inValue.Quote = Quote;
            return ((ServiceReference1.SSFConnectPortSoap)(this)).QuoteAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ServiceReference1.CreatePurchaseOrderResponse1> ServiceReference1.SSFConnectPortSoap.CreatePurchaseOrderAsync(ServiceReference1.CreatePurchaseOrderRequest request)
        {
            return base.Channel.CreatePurchaseOrderAsync(request);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.CreatePurchaseOrderResponse1> CreatePurchaseOrderAsync(ServiceReference1.CreatePurchaseOrder CreatePurchaseOrder)
        {
            ServiceReference1.CreatePurchaseOrderRequest inValue = new ServiceReference1.CreatePurchaseOrderRequest();
            inValue.CreatePurchaseOrder = CreatePurchaseOrder;
            return ((ServiceReference1.SSFConnectPortSoap)(this)).CreatePurchaseOrderAsync(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.SSFConnectPortSoap))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                result.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.Transport;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.SSFConnectPortSoap))
            {
                return new System.ServiceModel.EndpointAddress("https://www.ssfparts.com/ssfconnect-v1.5/ipo.asmx");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return SSFConnectPortSoapClient.GetBindingForEndpoint(EndpointConfiguration.SSFConnectPortSoap);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return SSFConnectPortSoapClient.GetEndpointAddress(EndpointConfiguration.SSFConnectPortSoap);
        }
        
        public enum EndpointConfiguration
        {            
            SSFConnectPortSoap,
        }
    }
}
