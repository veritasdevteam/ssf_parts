﻿using System;
using System.Web;
using System.ComponentModel;

public static class Enums
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public enum BusinessObjectDocumentEnvironment
    {
        Test,
        Production,
    }

    public enum QuoteOrPO
    {
        Quote,
        PO
    }

    public enum Line_Response_Codes
    {
        [Description("OK")]
        OK,
        [Description("ITEM - SUPERCEDING (New/Supersede-to Part)")]
        I01,
        [Description("ITEM - ALTERNATE (Optional Brands, Cross References)")]
        I02,
        [Description("ITEM - NOT FOUND (Invalid Item or Item/Mfg Combination)")]
        I04,
        [Description("QTY - MINIMUM ORDER QUANTITY OR MULTIPLES OF QTY")]
        Q01,
        [Description("QTY- INSUFFICIENT AVAILABLE INVENTORY")]
        Q03,
        [Description("QTY - NOT AVAILABLE (0 AVAILABLE INVENTORY)")]
        Q04,
        [Description("QTY - NOT FOR SALE (SUPERSEDED)")]
        Q05,
        [Description("SSF - ITEM CROSS REFERENCED")]
        S01,
        [Description("SSFParts - MULTIPLE MANUFACTURERS AVAILABLE")]
        S02,
        [Description("SHIP - Ship Date Changed")]
        D01,
        [Description("SHIP - Ship Method Changed")]
        D03
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum Temperature
    {
        Celsius,
        Fahrenheit,
        Kelvin
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum TemperatureScale
    {
        Celsius,
        Fahrenheit,
        Kelvin
    }

    public enum Rating
    {
        Good,
        Bad,
        Ugly
    }

    public enum ProjectTransactionType
    {
        Billed,
        Cash,
        Cost,
        Revenue
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum AcknowledgementType
    {
        Always,
        OnChange,
        Never,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum Confirmation
    {
        [System.Xml.Serialization.XmlEnumAttribute("0")]
        Item0,
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        Item1,
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        Item2,
        Never,
        OnError,
        Always,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum ConfirmType
    {
        Always,       
        OnChange,
        Never,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum LicenseType
    {
        Import,
        Export,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType
    {
        BrokerDocumentId,
        CarrierDocumentId,
        CustomerDocumentId,
        DocumentId,
        LogisticsProviderDocumentId,
        ShippersDocumentId,
        SupplierDocumentId,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType1
    {
        CatalogDocumentReference,
        ContractDocumentReference,
        InvoiceDocumentReference,
        LedgerDocumentReference,
        MaintenanceOrderReference,
        ProjectReference,
        PurchaseOrderDocumentReference,
        QuoteDocumentReference,
        RFQDocumentReference,
        ReceiptDocumentReference,
        RequisitionDocumentReference,
        SalesOrderDocumentReference,
        UOMGroupReference,
        VoucherDocumentReference,
    }
   

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType2
    {
        DayOfMonth,
        DueDate,
        NumberOfDays,
        PaymentTermsDate,
        ProximoNumberMonth,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType5
    {
        Contact,
        ContactAbs,
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType7
    {
        BuyerItemId,
        CarrierItemId,
        CustomerItemId,
        ManufacturerItemId,
        ShipFromItemId,
        SupplierItemId,
    }
    

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public enum PaymentMethod
    {
        [System.Xml.Serialization.XmlEnumAttribute("Credit Card")]
        CreditCard,
        [System.Xml.Serialization.XmlEnumAttribute("Debit Card")]
        DebitCard,
        [System.Xml.Serialization.XmlEnumAttribute("Cash")]
        Cash,
        [System.Xml.Serialization.XmlEnumAttribute("Check")]
        Check,
        [System.Xml.Serialization.XmlEnumAttribute("Other")]
        Other,
    }

    public enum TaxCode
    {
        VAT1,
        VAT2,
        VAT3,
        VAT4,
        VAT5,
        US_Federal_Tax
    }

    public enum ShippingLocations
    {
        [Description("South San Francisco, CA")]
        SF,
        [Description("Carson, CA")]
        CN,
        [Description("San Diego, CA")]
        SD,
        [Description("Phoenix, AZ")]
        PH,
        [Description("Orange County, CA")]
        OC,
        [Description(" Atlanta, GA")]
        AT,
        [Description("Pompano Beach, FL")]
        PB
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
    public enum ItemsChoiceType6
    {
        BillToParty,
        BrokerParty,
        CarrierParty,
        CustomerParty,
        EmployeeParty,
        ExporterParty,
        FreightBillToParty,
        HoldAtParty,
        ImporterParty,
        JointVentureParty,
        ManufacturerParty,
        PayFromParty,
        PublisherParty,
        RemitToParty,
        ReturnToParty,
        ShipFromParty,
        ShipToParty,
        SoldToParty,
        SupplierParty,
    }  

    public class ShippingServices
    {
        public enum ShippingServicesEnum
        {
            [Description("Ground")]
            G,
            [Description("3rd Day")]
            Three,
            [Description("2nd Day")]
            Two,
            [Description(" Next Day PM")]
            One,
            [Description("Next Day AM")]
            A,
            [Description(" Non-48 US 2Day")]
            Nine,
            [Description("2nd Day GLS")]
            M,
            [Description("Next Day PM GLS")]
            K,
            [Description("Next Day Noon GLS")]
            N,
            [Description("Next Day AM GLS")]
            J,
            [Description("Next Day Early GLS")]
            E,
            [Description("2nd Day CX")]
            C,
            [Description("Next Day CX")]
            T,
            [Description("Next Day HI (SF)")]
            H,
            [Description("USPS Mail")]
            P,
            [Description("First Class Package")]
            F,
            [Description("Ocean(SF)")]
            O,
            [Description("LTL")]
            L
        }
       
        public string GetValueOFShippingServicesEnum(ShippingServicesEnum shippingServices)
        {
            string enumValue = string.Empty;
            switch (shippingServices)
            {
                case ShippingServicesEnum.One:
                    enumValue = "1";
                    break;
                case ShippingServicesEnum.Two:
                    enumValue = "2";
                    break;
                case ShippingServicesEnum.Three:
                    enumValue = "3";
                    break;
                case ShippingServicesEnum.Nine:
                    enumValue = "9";
                    break;
                default:
                    enumValue = shippingServices.ToString();
                    break;
            }
            return enumValue;
        }
    }
    public enum ManufacturerType
    {
        [Description("Aftermarket")]
        A,
        [Description("Genuine")]
        G,
        [Description("OEM")]
        O
    }
    
}
