﻿using System;
using System.Data;
using System.Net;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using SSF_AutoParts.SQL;
using System.Web.Services.Protocols;
using SSF_AutoParts.Functions;
using System.Collections.Generic;

namespace SSF_AutoParts
{
    public class Instance_Functs
    {
        public string GetMakeIDFromMake(string make)
        {
            make = make.ToUpper().Trim();
            string makeID = ConfigurationManager.AppSettings.Get(make);
            return makeID;
        }      

        public class SOAP
        {
            public string SendSoap(Public_Classes.SoapParams soapParams, Uri website)
            {
                string document = Query(soapParams, website);
                HttpWebRequest webRequest = Request(soapParams.URL, soapParams.Action, document);

                string result = string.Empty;

                var soapResult = Response(webRequest);
                return soapResult;
            }
            internal string Query(Public_Classes.SoapParams soapParams, Uri personnelInfoUrl)
            {
                var doc = @"<?xml version='1.0' encoding='UTF-8'?><s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'><s:Body xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'><PersonnelInfo xmlns='{0}'>";
                doc = string.Format(doc, personnelInfoUrl);
                doc += "<PersonnelInfoRequest xmlns=''><username>{0}</username><password>{1}</password><reg_code>{2}</reg_code><date>{3}</date>";

                doc += "<country>{4}</country><language>ENG</language></PersonnelInfoRequest></PersonnelInfo></s:Body></s:Envelope>";
                doc = string.Format(doc, soapParams.User, soapParams.Pass, soapParams.Regc, soapParams.Date, soapParams.Country);
                return doc;
            }
            internal HttpWebRequest Request(Uri url, string action, string doc)
            {
                var soapEnvelope = new XmlDocument();
                soapEnvelope.LoadXml(doc);
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Headers.Add("SOAPAction", action);
                string website = ConfigurationManager.AppSettings.Get("QuoteAndCreatePurchaseOrderUrl");
                webRequest.Headers.Add("Host", website);
                webRequest.ContentType = "text/xml;charset=\"utf-8\"";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";

                // Need to check if this is correct...               
                //webRequest.Host = website.ToString();

                using (var stream = webRequest.GetRequestStream())
                {
                    soapEnvelope.Save(stream);
                }
                return webRequest;
            }

            internal string Response(HttpWebRequest webRequest)
            {
                var asyncResult = webRequest.BeginGetResponse(null, null);
                asyncResult.AsyncWaitHandle.WaitOne();
                using (var webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    var responsesteam = webResponse.GetResponseStream();
                    if (responsesteam == null) return default(string);
                    using (var rd = new StreamReader(responsesteam))
                    {
                        var soapResult = rd.ReadToEnd();
                        return soapResult;
                    }
                }
            }
        }
        public string BuildEnvelope(string bodyData)
        {
            string envelope = "<soap:Envelope xmlns:xsi=";
            envelope += "\"http://www.w3.org/2001/XMLSchema-instance\"";
            envelope += "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"";
            envelope += "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
            envelope += BeginHeader();
            envelope += GetUserTokenXML();
            envelope += CloseHeader();
            envelope += GetBody(bodyData);

            // Close Envelope...
            envelope += "</soap:Envelope>";            

            return envelope;
        }

        public string BeginHeader()
        {
            string OpenHeader = "<soap:Header>";
            return OpenHeader;
        }

        public string CloseHeader()
        {
            string closedHeader = "</soap:Header>";
            return closedHeader;
        }

        public string GetUserTokenXML()
        {
            string userName = ConfigurationManager.AppSettings.Get("Username");
            string password = ConfigurationManager.AppSettings.Get("Pwd");

            string userTokenXML = "<wsse:Security><wsse:UsernameToken>";
            userTokenXML += "<wsse:Username>" + userName + "</wsse:Username>";
            userTokenXML += "<wsse:Password>" + password + "</wsse:Password>";
            userTokenXML += "</wsse:UsernameToken>";
            userTokenXML += "</wsse:Security>";

            return userTokenXML;
        }

        public string GetBody(string bodyData)
        {
            string body = "<soap:Body>";
            body += "<AddRequestforQuoteBOD>" + bodyData + "</AddRequestforQuoteBOD>";
            body += "</soap:Body>";
            return body;
        }       

        public bool GetQuote(Quote quoteRequest)
        {
            string baseURL = ConfigurationManager.AppSettings.Get("QuoteAndPOBaseUrl");
            string fullURL = String.Format(baseURL, "Quote");
            
            Quote ipoQuote = new Quote();
            global::AddRequestForQuote ipoQuoteRequest = new();
            IPOQuoteResponse ipoQuoteResponse = new();

            //string temp = ipoQuote.AddRequestforQuoteBOD;
            //string temp2 = ipoQuoteRequest.Quote.AddRequestforQuoteBOD;
            //IPO.IPOQuoteResponse qr = ipoQuoteResponse.QuoteResult;

            return true;
        }

        //public RequestForQuoteLine GetQuoteRequest(global::AddRequestForQuote_XML addReqQuote)
        //{

        //    RequestForQuote reqQuote = new();
        //    reqQuote.Header = new();
        //    RequestForQuoteLine line = new();

        //    return line;   
        //}

        //public RequestForQuoteLine PopulateQuoteLine(Public_Classes.PartSearch data)
        //{
        //    RequestForQuoteLine ql = new();            

        //    return ql;
        //}

        public bool CreatePurchaseOrdert(string poAsXml)
        {
            string baseURL = ConfigurationManager.AppSettings.Get("QuoteAndPOBaseUrl");
            string fullURL = String.Format(baseURL, "CreatePurchaseOrder");

            ServiceReference1.CreatePurchaseOrderRequest poReq = new();
            poReq.CreatePurchaseOrder.ProcessPurchaseOrderBOD = poAsXml;
       
            return true;
        }

     

        public bool CalculateFreight()
        {
            string baseURL = ConfigurationManager.AppSettings.Get("ValueAddedBaseUrl");
            string fullURL = String.Format(baseURL, "CalculateFreight");

            ServiceReference2.CalculateFreightRequest calShipReq = new();
            ServiceReference2.CalculateFreightRequestBody calcShipBodyReq = new();

            return true;
        }

        public bool GetFreightOptions()
        {
            string baseURL = ConfigurationManager.AppSettings.Get("ValueAddedBaseUrl");
            string fullURL = String.Format(baseURL, "GetFreightOptions");

            getFreightOptions_Info freightOpts = new getFreightOptions_Info();

            return true;
        }

        public bool Check_PartNumber()
        { 
            string baseURL = ConfigurationManager.AppSettings.Get("ValueAddedBaseUrl");
            string fullURL = String.Format(baseURL, "check_partNumber");

            ServiceReference2.check_partNumberRequest partReq = new();
            ServiceReference2.check_partNumberRequestBody partReqBody = new();
          
   
            return true;

        }

        public bool Cutoff_Departure()
        {
            string baseURL = ConfigurationManager.AppSettings.Get("ValueAddedBaseUrl");
            string fullURL = String.Format(baseURL, "cutoff_departure");
         
            route_cutoff_departure_WSInfo info = new route_cutoff_departure_WSInfo();
            ServiceReference2.cutoff_departureRequestBody depReqBody = new();           
          

            return true;
        }

        public bool VerifyStatusCode(HttpStatusCode actualCode, HttpStatusCode expectedCode)
        {
            if (actualCode == expectedCode)
            {
                return true;
            }

            return false;
        }

        public string ConvertValueAddedArgsToSOAPCall(string[] args)
        {
            string XML = string.Empty;
            string apiURL = string.Empty;
            Globals glob = new Globals(ConfigurationManager.AppSettings.Get("ValueAddedBaseUrl"));          

            List<string> vServices = new();
            vServices.Add("CALCULATEFREIGHT");
            vServices.Add("GETFREIGHTOPTIONS");
            vServices.Add("CHECKPARTNUMBER");
            vServices.Add("CUTOFFDEPARTURE");
            vServices.Add("GETPARTIMAGE");
       
            string valueAddedService = args[0].Trim().ToUpper();
            if (vServices.Contains(valueAddedService))
            {
                switch (valueAddedService)
                {
                    case "CALCULATEFREIGHT":
                        apiURL = ConfigurationManager.AppSettings.Get(valueAddedService);
                        XML += "<CalculateFreight> " + args[1] + " " + args[2] + " " + args[3] + " </CalculateFreight>" + Environment.NewLine;
                        glob.ValueAdded_CalculateFreight();
                        break;
                    case "GETFREIGHTOPTIONS":
                        apiURL = ConfigurationManager.AppSettings.Get(valueAddedService);
                        XML += "<GetFreightOptions>  " + args[2] + " " + args[4] + " " + args[6] + " </GetFreightOptions> " + Environment.NewLine;
                        glob.ValueAdded_GetFreightOptions();
                        break;
                    case "CHECKPARTNUMBER":
                        apiURL = ConfigurationManager.AppSettings.Get(valueAddedService);
                        XML += "<Check_partNumber> " + args[1] + " " + args[3] + " " + args[5] + " </Check_partNumber> " + Environment.NewLine;
                        glob.ValueAdded_CheckPartNumber();
                        break;
                    case "CUTOFFDEPARTURE":
                        apiURL = ConfigurationManager.AppSettings.Get(valueAddedService);
                        XML += "<Cutoff_departure>" +
                            "" + args[3] + args[4] + args[5] + " <Cutoff_departure/>";
                        glob.ValueAdded_CutOffDeparture();
                        break;
                    default: // GetPartIamge is not used
                        Console.WriteLine("Unsupported ValueAdded Service: " + valueAddedService);
                        Console.ReadLine();
                        break;
                }
            }
            else
            {
                Console.WriteLine("NO Values sent to Instance_Functs.ConvertArgsToXML()!");
            }

            return XML;
        }



        public XmlDocument CreateXMLFromString(string XMLtext)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XMLtext);
            return xmlDoc;
        }

        public DataSet CreateDataSetFromXML(XmlReader xmlRead)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(xmlRead, XmlReadMode.ReadSchema);
            return ds;
        }

        public HttpWebRequest CreateSOAPWebRequest_POST(string urlAsString)
        {
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(urlAsString);
            Req.Headers.Add(@"SOAP:Action");
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            Req.Method = "POST";
            return Req;
        }

        public HttpWebRequest CreateSOAPWebRequest_GET(string urlAsString, string datatoSend)
        {
            Byte[] byteData = datatoSend.Select(c => (byte)c).ToArray();
            // Making Web Request    
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@urlAsString);
            //SOAPAction    
            Req.Headers.Add(@"SOAPAction:" + urlAsString);
            //Content_type    
            Req.ContentType = "text/xml;charset=\'utf-8\'";
            Req.ContentLength = byteData.Length;
            Stream mystream = Req.GetRequestStream();
           // mystream.Write(byteData, 0, byteData.Length);
            Req.Accept = "text/xml";
            //HTTP method    
            Req.Method = "Get";

            //return HttpWebRequest    
            return Req;
        }

        public string GetSoapDataAsString(int a, int b)
        {
               string soap = @"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                <soap:Body>
                 <Addition xmlns=""http://tempuri.org/"">
                    <a>" + a + @"</a>
                    <b>" + b + @"</b>
                    </Addition>
                    </soap:Body>
                    </soap:Envelope>";

            return soap;
        }
       

        public void WriteOutputBeforeSerialize(SoapMessage message)
        {            
            if (!Globals.IsTest)
            {
                return;
            }

            string filename = ConfigurationManager.AppSettings.Get("FileWriteToLocation_Output");

            FileStream myFileStream =
               new FileStream(filename, FileMode.Append, FileAccess.Write);
            StreamWriter myStreamWriter = new StreamWriter(myFileStream);
            myStreamWriter.WriteLine("================================== Request at "
               + DateTime.Now);
            myStreamWriter.WriteLine("The method that has been invoked is: ");
            myStreamWriter.WriteLine("\t" + message.MethodInfo);
            myStreamWriter.WriteLine(
               "The contents of the SOAPAction HTTP header is:");
            myStreamWriter.WriteLine("\t" + message.Action);
            myStreamWriter.WriteLine("The contents of HTTP Content-type header is:");
            myStreamWriter.WriteLine("\t" + message.ContentType);
            if (message.OneWay)
                myStreamWriter.WriteLine(
                   "The method invoked on the client shall not wait"
                   + " till the server finishes");
            else
                myStreamWriter.WriteLine(
                   "The method invoked on the client shall wait"
                   + " till the server finishes");
            myStreamWriter.WriteLine(
               "The site where the XML Web service is available is:");
            myStreamWriter.WriteLine("\t" + message.Url);
            myStreamWriter.WriteLine("The values of the in parameters are:");
            myStreamWriter.WriteLine("Value of first in parameter: {0}",
               message.GetInParameterValue(0));
            myStreamWriter.WriteLine("Value of second in parameter: {0}",
               message.GetInParameterValue(1));
            myStreamWriter.WriteLine();
            myStreamWriter.Flush();
            myStreamWriter.Close();
            myFileStream.Close();
        }

        //// Write the contents of the incoming SOAP message to the log file.
        public void WriteInputAfterDeserialize(SoapMessage message)
        {           
            if (!Globals.IsTest)
            {
                return;
            }
            string filename = ConfigurationManager.AppSettings.Get("FileWriteToLocation_Input");

            FileStream myFileStream =
               new FileStream(filename, FileMode.Append, FileAccess.Write);
            StreamWriter myStreamWriter = new StreamWriter(myFileStream);
            myStreamWriter.WriteLine();
      
            myStreamWriter.WriteLine("The value of the out parameter is: {0}",
               message.GetOutParameterValue(0));
            myStreamWriter.WriteLine("The value of the return parameter is: {0}",
               message.GetReturnValue());
            myStreamWriter.Flush();
            myStreamWriter.Close();
            myFileStream.Close();
        }

        public string GetCatalogSearchXML_By_Make_Year_Model(string makeID, string makeDescr, string modelID, string modelDescr, int year)
        {
            string catalogSearchTemplate_Line_Year_Model = "<ssf:CatalogSearch> ";
            catalogSearchTemplate_Line_Year_Model += @"<ssf:CatalogMake ID = ""{0}"">";
            catalogSearchTemplate_Line_Year_Model += "{1}</ssf:CatalogMake>";
            catalogSearchTemplate_Line_Year_Model += " <ssf:CatalogYear>{2}</ssf:CatalogYear> ";
            catalogSearchTemplate_Line_Year_Model += @"<ssf:CatalogModel ID =""{3}"" /> ";
            catalogSearchTemplate_Line_Year_Model += " <ssf:CatalogGroup ID = SearchLogId= 0 /> ";
            catalogSearchTemplate_Line_Year_Model += " <ssf:CatalogSearch ID = SearchLogId= 0 /> ";
            catalogSearchTemplate_Line_Year_Model += " <ssf:CatalogCategory ID = SearchLogId= 0 /> ";
            catalogSearchTemplate_Line_Year_Model += " </ssf:CatalogSearch> ";
            catalogSearchTemplate_Line_Year_Model += " <ssf:SearchId /> ";

            string catalogSearchXML = String.Format(catalogSearchTemplate_Line_Year_Model, makeID, makeDescr, modelID, modelDescr, year.ToString());

            return catalogSearchXML;

        }

        public string GetCatalogSearchXML_By_Make_Year(string makeID, string makeDescr, int year)
        {
            string catalogSearchTemplate_Line_Year = "<ssf:CatalogSearch> ";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogMake ID =""{0}"">";
            catalogSearchTemplate_Line_Year += "{1}</ssf:CatalogMake>";
            catalogSearchTemplate_Line_Year += "<ssf:CatalogYear>{2}</ssf:CatalogYear>";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogModel ID = """"/> ";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogGroup ID ="""" SearchLogId= ""0"" />";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogSearch ID ="""" SearchLogId= ""0"" />";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogCategory ID ="""" SearchLogId= ""0"" />";
            catalogSearchTemplate_Line_Year += "</ssf:CatalogSearch>";
            catalogSearchTemplate_Line_Year += "<ssf:SearchId />";

            string catalogSearchXML = String.Format(catalogSearchTemplate_Line_Year, makeID, makeDescr, year.ToString());

            return catalogSearchXML;
        }

        public string GetCatalogSearchXML_By_Make_Year_Model_Group(string makeID, string makeDescr, string modelID, string modelDescr, int year, string SearchLogId)
        {
            string catalogSearchTemplate_Line_Year = "<ssf:CatalogSearch>";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogMake ID =""{0}"">";
            catalogSearchTemplate_Line_Year += "{1}</ssf:CatalogMake>";
            catalogSearchTemplate_Line_Year += "<ssf:CatalogYear>{2}</ssf:CatalogYear>";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogModel ID = ""{3}""/>{4}";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogGroup ID ="""" SearchLogId= ""0"" />";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogSearch ID ="""" SearchLogId= ""0"" />";
            catalogSearchTemplate_Line_Year += @"<ssf:CatalogCategory ID ="""" SearchLogId= ""{5}"" />";
            catalogSearchTemplate_Line_Year += "</ssf:CatalogSearch>";
            catalogSearchTemplate_Line_Year += "<ssf:SearchId />";

            string catalogSearchXML = String.Format(catalogSearchTemplate_Line_Year, makeID, makeDescr, year.ToString(), modelID, modelDescr, SearchLogId);            
            return catalogSearchXML;
        }
    }
}




