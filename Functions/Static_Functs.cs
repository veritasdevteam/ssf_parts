﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using SSF_AutoParts.SQL;
using System.Net;
using SSF_AutoParts.XML_Classes;
using System.Data;
using System.Xml.Serialization;

namespace SSF_AutoParts.Functions
{
    public static class Static_Functs
    {
       public static void InitializeData()
        {
            setIsTest();
            SQL_Select ss = new();
            ss.SetPartNoFromClaimDetail();           
            ss.SetClaimIDFromClaimDetail();
            ss.FillServiceCenterData();
            ss.SetPartNoFromClaimDetail();
            ss.SetJobNoFromClaimDetail();
            ss.SetClaimDetailQuoteIDfromClaimDetailQuote();
            ss.SetReqQtyFromClaimDetail();
            fillWarehouseLocations();
            fillWarehouseLocationsDict();
        }
        
        private static void setIsTest()
        {
            string temp = ConfigurationManager.AppSettings.Get("Environment").ToUpper().Trim();
            Globals.IsTest = (temp == "TEST");
            if (Globals.IsTest)
            {
                SQL_Master.MasterConnString = ConfigurationManager.ConnectionStrings["ConnStringTest"].ToString().Trim();
            }
            else
            {
                SQL_Master.MasterConnString = ConfigurationManager.ConnectionStrings["ConnStringProd"].ToString().Trim();
            }
        }

        private static void FillDictionaryWithShippingInfo()
        {
            Globals.ShippingInfoDict.Add("G", "Ground");
            Globals.ShippingInfoDict.Add("3", "3rd Day");
            Globals.ShippingInfoDict.Add("2", "2nd Day");
            Globals.ShippingInfoDict.Add("1", "Next Day PM");
            Globals.ShippingInfoDict.Add("A", "Next Day AM");
            Globals.ShippingInfoDict.Add("9", "Non-48 US 2Day");
            Globals.ShippingInfoDict.Add("M", "2nd Day GLS");
            Globals.ShippingInfoDict.Add("K", "Next Day PM GLS");
            Globals.ShippingInfoDict.Add("N", "Next Day Noon GLS");
            Globals.ShippingInfoDict.Add("J", "Next Day AM GLS");
            Globals.ShippingInfoDict.Add("E", "Next Day Early GLS");
            Globals.ShippingInfoDict.Add("C", "2nd Day CX");
            Globals.ShippingInfoDict.Add("T", "Next Day CX");
            Globals.ShippingInfoDict.Add("H", "Next Day HI (SF)");
            Globals.ShippingInfoDict.Add("P", "USPS Mail");
            Globals.ShippingInfoDict.Add("F", "First Class Package");
            Globals.ShippingInfoDict.Add("O", "Ocean (SF)");
            Globals.ShippingInfoDict.Add("L", "LTL");
        }

        private static void fillWarehouseLocationsDict()
        {
            foreach(string location in Globals.WarehouseLocations)
            {
                string zipCode = ConfigurationManager.AppSettings.Get(location);
                Globals.WarehouseLocationsDict.Add(location, zipCode);
            }
            return;     
        }

        public static void fillWarehouseLocations()
        {
            string locations = ConfigurationManager.AppSettings.Get("WarehouseStates");
            Globals.WarehouseLocations = locations.Split(',').ToList();
        }



        public static void ConvertXMLDocToString()
        {
            //var s = new XmlSerializer();
            //var newXmlStr = s.serializeToString(Globals.XMLDoc);
            //Globals.XMLDoc
        }

      

        private static void initializeXmlReader()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            XmlReader temp = XmlReader.Create(Globals.Path, settings);

            // get the root element    
            Globals.Root2 = XElement.Load(temp);
            Console.WriteLine(GetOutline(0, Globals.Root2));
        }

        private static string GetOutline(int indentLevel, XElement element)
        {
            StringBuilder result = new StringBuilder();

            if (element.Attribute("name") != null)
            {
                result = result.AppendLine(new string(' ', indentLevel * 2) + element.Attribute("name").Value);
            }
           
            foreach (XElement childElement in element.Elements())
            {
                string temp = RemoveNameSpacefromName(childElement.Name);
                Console.WriteLine(temp + " = " + childElement.Value);
                result.Append(GetOutline(indentLevel + 1, childElement));
            }

            return result.ToString();
        }

        public static XmlNodeList GetAllNodesWithName(string name)
        {
            XmlNodeList elemList = Globals.XMLDoc.GetElementsByTagName(name);
            for (int i = 0; i < elemList.Count; i++)
            {
                Console.WriteLine(elemList[i].InnerXml);
            }
            return elemList;
        }

        public static string RemoveNameSpacefromName(XName name)
        {            
            string cleanName = name.ToString().Replace(Globals.XNstr, "").Replace(Globals.XN2, "").Replace(Globals.XN3, "");
            return cleanName;
        }

        public static IEnumerable<XElement> GetRoot2Elements()
        {
            IEnumerable<XElement> list = Globals.Root2.Elements();

            List<string> listofXML = new List<string>();
            string xmlPath = "C:\\temp\\Test.xml";
            foreach (var element in list)
            {
                IEnumerable<XElement> listChildren = element.Elements();
                string cleanName = RemoveNameSpacefromName(element.Name.ToString());
                string temp = "Element.Name = " + cleanName + Environment.NewLine + "Value = " + element.Value + Environment.NewLine;
                listofXML.Add(temp);
                foreach (XElement childElement in listChildren)
                {
                    cleanName = RemoveNameSpacefromName(childElement.Name);
                    temp = "Element.Name = " + childElement.Name + Environment.NewLine + "Value = " + childElement.Value + Environment.NewLine;
                    listofXML.Add(temp);
                }
            }
            if (File.Exists(xmlPath))
            {
                File.Delete(xmlPath);
            }
            foreach (string xmlLine in listofXML)
            {
                using (StreamWriter sw = File.CreateText(xmlPath))
                {
                    sw.WriteLine(xmlLine);
                    sw.WriteLine(sw.NewLine);
                }
            }

            return list;
        }


        public static void ParseXML()
        {
            XDocument xml = XDocument.Load("C:\\VeritasRepos\\SSF_Parts\\Test_XML_Files\\AddQuote_Response2.xml");

            foreach (var node in xml.Descendants())
            {
                if (node is XElement)
                {
                    Console.WriteLine("Node: " + node.Name + " -  Node.value" + node.Value);
                    //some code...
                }
            }
            Console.ReadKey();
        }
        public static string MergeDataWithTemplate(string template, string data)
        {
            string blendedValue = string.Format(template, data);
            return blendedValue;
        }

        public static string GetValueFromStringDictionary(Dictionary<string, string> dic, string key)
        {
            string value = string.Empty;
            if (dic.ContainsKey(key))
            {
                if (!dic.TryGetValue(key, out value))
                {
                    Console.WriteLine("Error: Dictionary does not Contain 'Key' value: " + key);
                }
            }
            else
            {
                Console.WriteLine("Error: Dictionary does not Contain 'Key' value: " + key);
                if (!Globals.IsTest)
                {
                    Console.ReadKey();
                }
            }

            return value;
        }
    

        public static string GetZipcodeForWarehouse(string warehouseCode)
        {
            string warehouseZip = ConfigurationManager.AppSettings.Get(warehouseCode.ToUpper().Trim());
            return warehouseZip;
        }

        public static void SetClosestWarehouse()
        {
            
            string apiURL = ConfigurationManager.AppSettings.Get("ZipcodeApiURL");
            string apiAccessKey = ConfigurationManager.AppSettings.Get("ZipcodeApiKey");
            string closestLocation = string.Empty;

            Dictionary<string, decimal> dict = new();
            decimal distance = 0;
            var rand = new Random();
            foreach (string location in Globals.WarehouseLocations)
            {
                string zipCode = GetZipcodeForWarehouse(location);
                distance = CallZipcodeAPI(zipCode);
                Console.WriteLine("Location: " + location + ", Distance: " + distance);
                dict.Add(location, distance);
            }

            decimal minValue = distance;
            foreach (KeyValuePair<string, decimal> dic in dict)
            {
                if (dic.Value < minValue)
                {
                    closestLocation = dic.Key;
                    minValue = dic.Value;
                }
            }
            Globals.ClosestWarehouse = closestLocation;
        }

        public static void FillStateList()
        {
            string listPreSpilt = ConfigurationManager.AppSettings.Get("WarehouseStates");
            Globals.WarehouseLocations = listPreSpilt.Split(",").ToList();
        }

        public static Decimal CallZipcodeAPI(string fromZIP)
        {            
            string apiURL = "http://www.zipcodeapi.com/rest/{0}/distance.xml/{1}/{2}/mile";

            string apiKey = ConfigurationManager.AppSettings.Get("ZipcodeApiKey");
            apiURL = string.Format(apiURL, apiKey, fromZIP, SQL_Select.Ship.ShipToPostalCode);
            WebRequest request = WebRequest.Create(apiURL);
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string rt = reader.ReadToEnd();
            reader.Close();
            response.Close();
            rt = rt.Replace("<?xml version=\"1.0\"?>\n<response><distance>", "");
            rt = rt.Replace("</distance></response>", "");          
            decimal value = decimal.Parse(rt);          
            return value;
        }

        private static decimal cleanUpXMlDecimalResponse()
        {
            XmlElement xe = Globals.XMLDoc.DocumentElement;
            string response = GetElementValueByTagname("response");
            return Decimal.Parse(response);
        }

        public static string GetElementValueByTagname(string tagName)
        {
            string value = string.Empty;
            string eleementValue = string.Empty;
            var elements = Globals.XMLDoc.GetElementsByTagName(tagName);

            if (elements.Count == 0)
            {
                Console.WriteLine("NO Elements found for tagname: " + tagName);
                Console.ReadKey();
                return string.Empty;
            }
            else if (elements.Count == 1)
            {
                Console.WriteLine(elements[0].InnerText);
                foreach (XmlNode node in elements[0].ChildNodes)
                {
                    Console.WriteLine(node.InnerText + Environment.NewLine);
                    foreach (XmlNode n in node.ChildNodes)
                    {
                        Console.WriteLine(n.InnerText + Environment.NewLine);
                    }
                }

                value = elements[0].Name;
            }
            else
            {
                foreach (XmlNode element in elements)
                {
                    XmlNodeList nodeList;
                    XmlElement root = Globals.XMLDoc.DocumentElement;
                    nodeList = root.SelectNodes(tagName);
                    foreach (XmlNode node in nodeList)
                    {
                        Console.WriteLine(node.Value);
                    }
                }
            }
            return value;
        }

        public static string GetDescriptionOfShippingService(Enums.ShippingServices shippingService)
        {
            Type type = typeof(Enums.ShippingServices);
            var memInfo = type.GetMember(shippingService.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((DescriptionAttribute)attributes[0]).Description;
        }

        public static List<string> WriteFeatureValuesDictionaryAsList()
        {
            List<string> temp = new();

            foreach (KeyValuePair<string, string> kvp in Globals.FeatureValueDict)
            {
                temp.Add("Key: " + kvp.Key + ", Value: " + kvp.Value);
            }

            if (Globals.IsTest)
            {
                WriteFileFromList(temp);
            }
            return temp;
        }


        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static void WriteFileFromList(List<string> list)
        {
            string dirLocation = ConfigurationManager.AppSettings.Get("DirectoryToWriteTo");
            string filePath = dirLocation + "FeatureValues.txt";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using StreamWriter file = new(filePath);
            foreach (string line in list)
            {
                file.WriteLine(line);
            }
            file.Close();
        }

        public static List<string> GetFeatureValueDictionaryAsList()
        {
            List<string> temp = new();

            foreach (KeyValuePair<string, string> kvp in Globals.FeatureValueDict)
            {
                temp.Add("Key: " + kvp.Key + ", Value: " + kvp.Value);
            }

            if (Globals.IsTest)
            {
                WriteFileFromList(temp);
            }
            return temp;
        }


    }
}


