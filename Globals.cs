﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;
using System.ComponentModel;
using System.ServiceModel;
using DBO;
using System.Runtime.Remoting;
using SSF_AutoParts.Functions;
using System.Xml;
using System.Xml.Linq;

namespace SSF_AutoParts
{
    public class Globals
    {
        public static string Path = "C:\\Temp\\AddQuote_Response.xml";
        public static string TestPath = ConfigurationManager.AppSettings.Get("DirectoryToWriteTo");
        public static string ValueAddedURLTemplate = ConfigurationManager.AppSettings.Get("ValueAddedBaseUrlTemplate");
        public static string QuoteAndPoURLTemplate = ConfigurationManager.AppSettings.Get("QuoteAndPOBaseUrlTemplate");
        public static string QuoteAndPoURL = ConfigurationManager.AppSettings.Get("QuoteAndCreatePurchaseOrderUrl");
        public static string UserName = ConfigurationManager.AppSettings.Get("Username");
        public static string Password = ConfigurationManager.AppSettings.Get("Pwd");

        public static Dictionary<string, string> ShippingInfoDict = new();
        public static Dictionary<string, string> WarehouseLocationsDict = new();
        public static List<string> WarehouseLocations = new();
        public static int ClaimID = 0;
        public static int ClaimDetailID = 0;
        public static int ClaimDetailQuoteID = 0;
        public static ushort? PartNo = 0;
        public static byte ReqQty = 0;
        public static decimal ExtendedPrice = 0;
        public static string JobNo = string.Empty;
        public static string BODId = string.Empty;
        public static string ClosestWarehouse = string.Empty;
        // Various NameSpaces to remove from XML values...
        public static XNamespace XN = "http://www.openapplications.org/oagis";
        public static string XNstr = "{http://www.openapplications.org/oagis}";
        public static string XN2 = "{http://www.aftermarket.org/oagis}";
        public static string XN3 = "{https://www.ssfparts.com/ssfconnect}";

        public static XElement Root1 = null;
        public static XElement Root2 = null;
        public static XmlDocument XMLDoc = new();
        public static XDocument xDoc = new();
        public static XmlReader XmlReader;

        public static string ApiURL = string.Empty;
        public static string SoapAction = string.Empty;
        private Instance_Functs fun = new();
        public static bool IsTest = false;
        public static bool WriteTestFiles = false;
        public static int LineItemNumber = 1;

        public static Dictionary<string, string> FeatureValueDict = new();

        public Globals() { }
        public Globals(string valueAdded)
        {
            ValueAddedURLTemplate = valueAdded;
        }

        public bool SetIsTest()
        {
            bool isTest = false;
            if (ConfigurationManager.AppSettings.Get("Environment").ToUpper().Trim() == "TEST")
            {
                isTest = true;
                WriteTestFiles = bool.Parse((ConfigurationManager.AppSettings.Get("WriteTestFiles")));
            }
            else
            {
                isTest = false;
                WriteTestFiles = false;
            }
            return isTest;
        }

        [System.Web.Services.WebServiceBindingAttribute(Name = "AddQuote", Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/")]
        public class AddQuote : System.Web.Services.Protocols.SoapHttpClientProtocol
        {
            [System.Diagnostics.DebuggerStepThroughAttribute()]
            public AddQuote()
            {
                string quoteURL = ConfigurationManager.AppSettings.Get("AddQuote_URL");
            }
        }

        [System.Web.Services.WebServiceBindingAttribute(Name = "AddQuoteRequest", Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/")]
        public class AddQuoteRequest : System.Web.Services.Protocols.SoapHttpClientProtocol
        {
            [System.Diagnostics.DebuggerStepThroughAttribute()]
            public AddQuoteRequest()
            {
                string quoteURL = ConfigurationManager.AppSettings.Get("AddQuoteRequest_URL");
            }
        }

        public static int GetCurrentLineNumber()
        {
            int temp = LineItemNumber;
            LineItemNumber += 1;
            return temp;
        }

        public static void ResetLineNumber()
        {
            LineItemNumber = 1;
        }       

        [System.Web.Services.WebServiceBindingAttribute(Name = "ProcessPurchaseOrder", Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/")]
        public class ProcessPurchaseOrder : System.Web.Services.Protocols.SoapHttpClientProtocol
        {
            [System.Diagnostics.DebuggerStepThroughAttribute()]
            public ProcessPurchaseOrder()
            {
                string poURL = ConfigurationManager.AppSettings.Get("ProcessPO_URL");
            }
        }

        [System.Web.Services.WebServiceBindingAttribute(Name = "AcknowledgePO", Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/")]
        public class AcknowledgePO : System.Web.Services.Protocols.SoapHttpClientProtocol
        {
            [System.Diagnostics.DebuggerStepThroughAttribute()]
            public AcknowledgePO()
            {
                string ackPoURL = ConfigurationManager.AppSettings.Get("AcknowledgePO_URL");
            }
        }

        public bool QuoteOrPO_Service(Enums.QuoteOrPO qpo)
        {
            if (qpo.Equals(Enums.QuoteOrPO.Quote))
            {
                SoapAction = ConfigurationManager.AppSettings.Get("QuoteSoapAction").Trim();
                ApiURL = Static_Functs.MergeDataWithTemplate(QuoteAndPoURLTemplate, "Quote");
            }
            else if (qpo.Equals(Enums.QuoteOrPO.PO))
            {
                SoapAction = ConfigurationManager.AppSettings.Get("POSoapAction").Trim();
                ApiURL = Static_Functs.MergeDataWithTemplate(QuoteAndPoURLTemplate, "CreatePurchaseOrder");
            }
            else
            {
                Console.WriteLine("Error: Method: QuoteOrPO_Service -> Not a Quote or CreatePurchaseOrder!");
                return false;
            }

            return true;
        }

        // ***************************  Below are ValueAdded Services  ******************
        public bool ValueAdded_CalculateFreight()
        {
            ApiURL = String.Format(ValueAddedURLTemplate, "CalculateFreight");
            SoapAction = ConfigurationManager.AppSettings.Get("CalculateFreightSoapAction").Trim();

            return true;
        }

        public bool ValueAdded_GetFreightOptions()
        {
            ApiURL = String.Format(ValueAddedURLTemplate, "GetFreightOptions");
            SoapAction = ConfigurationManager.AppSettings.Get("FreightOptionsSoapAction").Trim();

            return true;
        }

        public bool ValueAdded_CheckPartNumber()
        {
            ApiURL = String.Format(ValueAddedURLTemplate, "check_partNumber");
            SoapAction = ConfigurationManager.AppSettings.Get("CheckPartNumSoapAction").Trim();

            return true;
        }

        public bool ValueAdded_CutOffDeparture()
        {
            ApiURL = String.Format(ValueAddedURLTemplate, "CutOffDeparture");
            SoapAction = ConfigurationManager.AppSettings.Get("CutOffDepartureSoapAction").Trim();

            return true;
        }

        public bool GetPartImage()
        {
            ApiURL = String.Format(ValueAddedURLTemplate, "GetPartImage");
            SoapAction = ConfigurationManager.AppSettings.Get("PartImageSoapAction").Trim();
            return true;
        }

        public class ItemStatus
        {
            public string ItemStatusBegin = " <ItemStatus>";
            public string ChangeBegin = "<Change>";
            public string ChangeTo = " <To>{0}</To>";
            public string ChangeDescr = @" <Description lang=""{0}"">{1}</Description>";
            public string ChangeEnd = "</Change>";
            public string ItemStatusEnd = " </ItemStatus>";

            public string concatenaClasses(string templateData)
            {
                string sumOfstrings = this.ItemStatusBegin + this.ChangeBegin + this.ChangeTo;
                sumOfstrings += this.ChangeDescr + this.ChangeEnd + this.ItemStatusEnd;

                string tempString = string.Format(sumOfstrings, templateData);
                return tempString;
            }
        }
        public class FeatureValue
        {
            public string FeatureValueBegin = "<FeatureValue>";
            public string imageAvailable = @" <NameValue name=""Image Available"">{0}</NameValue>";
            public string OEM = @" < NameValue name=""OEM"">{0}</NameValue>";
            public string ManufacturerType = @" <NameValue name=""manufacturerType"">{0}</NameValue>";
            public string RequestedQty = @" <NameValue name=""Requested Quantity"">{0}</NameValue>";
            public string YourPriceWithCore = @" <NameValue name=""YourPrice withCore"">{0}</NameValue>";
            public string CoreUnit = @" <NameValue name=""CoreUnit"">{0}</NameValue>";
            public string PartOnSale = @" <NameValue name=""Part On Sale"">{0}</NameValue>";
            public string PartSupercededToReference = @" <NameValue name=""PartSuperceded To Reference"">{0}</NameValue>";
            public string FeeatureValueEnd = "</FeatureValue>";

            public string TagAroundFeatureValue(string featureValue, string data)
            {
                string blendedData = Static_Functs.MergeDataWithTemplate(featureValue, data);
                blendedData += this.FeatureValueBegin;
                blendedData += featureValue;
                blendedData += this.FeeatureValueEnd;
                return blendedData;
            }
        }     
       
    }
}



