﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.Web.Services.Protocols;
using System.Data;
using SSF_AutoParts.Functions;
using SSF_AutoParts.SQL;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections.Generic;
using SSF_AutoParts.XML_Classes;
using SSF_AutoParts.Functions;



namespace SSF_AutoParts
{
    class Program
    {
        static void Main(string[] args)
        {
            Globals.ClaimDetailID = int.Parse(args[0]);
            Static_Functs.InitializeData();
            Instance_Functs fun = new();
            string temp = fun.BuildEnvelope("blah");


            var grr = AddRequestForQuote_Builder.AddRequestForQuote();
            var brr = ProcessPurchaseOrder_Builder.ProcessPurchaseOrder();

            //AddQuote_Extractor.AddQuote();


           
            TEST.XmlNodeTypeDisplay();       
      
            //  var text = new XmlTextReader(temp);

            //int claimDetailID = 384786;
            //SQL_Select sel = new();
            //temp = sel.GetPartNoFromClaimDetail();
            //float value = sel.GetReqQtyFromClaimDetail();

            // TEST.XmlNodeTypeDisplay();
            // string makeID, string makeDescr, string modelID, string modelDescr, int year, string SearchLogId          



        
  // -----  UNCOMMENT OUT the following code when it is implemented.
            

            //if (args.Length == 0)
            //{
            //    Console.WriteLine("NO ARGUMENTS PASSED! Hit any key to exit.");
            //    if (!Globals.IsTest)
            //    {
            //        Console.ReadKey();
            //    }
            //    Environment.Exit(0);
            //}
            
            // Got here?  We have Parameters to work with!          

            //string toDo = args[0].ToUpper().Trim();
            
            //switch (toDo)
            //{
            //    case "QUOTE": // Args: 0 = Claim
            //        if (args.Length == 3)
            //        {
            //            Globals.ClaimDetailID = int.Parse(args[1]);
            //            Globals.PartNo = ushort.Parse(args[2].Trim());
            //            // Call PartSeach
            //        }
            //        break;
            //    case "PO":
            //        //bool itWorked = Static_Functs.
            //        break;
            //    case "CALCSHIP":
            //        break;
            //    case "SHIPOPT":
            //        break;
            //    case "CUTOFF":
            //        break;
            //    case "PARTNUM":
            //        break;
            //    default:
            //        Console.WriteLine("INVALID PARAMETER: " + toDo);
            //        if (Globals.IsTest)
            //        {
            //            Console.ReadKey();
            //        }
            //        Environment.Exit(0);
            //        break;
            //}

        }

        public static Public_Classes.ClaimDetailPurchase fillClaimDetailPurchase()
        {
            Public_Classes.ClaimDetailPurchase cd = new();
            cd.ClaimCompanyID = 1;
            cd.ClaimDetailID = 2;
            cd.ClaimDetailQuoteID = 3;
            cd.ClaimID = 4;
            cd.DeliveryDate = System.DateTime.Now;
            cd.InStock = "0";
            cd.ListPrice = 3.56M;
            cd.OEM = true;
            cd.OrderBy = 666;
            cd.OrderDate = System.DateTime.Now.AddDays(10);
            cd.OrderID = "orderID";
            cd.PartCost = 1.33M;
            cd.PartDesc = "part desc";
            cd.PartNo = "part number";
            cd.Qty = 666;
            cd.QuoteID = "QUOTE ID";
            cd.Shipping = 45.33M;
            cd.TotalOrderCost = 27272.33M;
            cd.YourCost = 1123.99M;
            return cd;
        }

    }
}

