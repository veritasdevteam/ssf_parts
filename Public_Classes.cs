﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Data;
using System.Xml;

namespace SSF_AutoParts
{
    public class Public_Classes
    {
        public class ClaimData
        {
            public int ContractID  { get; set; }
            public string RONumber  { get; set; }
            public string ClaimNo { get; set; }
            public int LossMile { get; set; }
        }

        public class PriceAndQty
        {
            public decimal Amount { get; set; }
            public float PerQuantity { get; set; }
        }

        public class ClaimDetailPurchase : ClaimDetailPurchase_Subset
        {
            public int ClaimDetailID { get; set; }
            public int ClaimDetailQuoteID { get; set; }
            public int ClaimCompanyID { get; set; }
            public string QuoteID { get; set; }
            public string PartNo { get; set; }
            public DateTime DeliveryDate { get; set; }
            public string PartDesc { get; set; }
            public string OrderID { get; set; }    
            public int OrderBy { get; set; }
            public DateTime OrderDate { get; set; }
        }

        public class ClaimDetailPurchase_Subset
        {
            // Value which cannot be NULL: ClaimID, InStock, Qty, ListPrice, YourCost, OEM, PartCost, Shipping, TotalOrderCost
            public int ClaimID { get; set; }
            public string InStock { get; set; }
            public int Qty { get; set; }
            public decimal YourCost { get; set; }
            public decimal ListPrice { get; set; }
            public bool OEM { get; set; }
            public Decimal PartCost { get; set; }
            public Decimal Shipping { get; set; }
            public decimal TotalOrderCost { get; set; }         
        }

        public class ClaimPart
        {
            public string ClaimID { get; set; }
            public string ClaimDetailID { get; set; }
            public string PartNo { get; set; }
            public string PartDesc { get; set; }
            public string Qty { get; set; }
            public string Cost { get; set; }
            public string TaxAmt { get; set; }
            public string ClaimPayeeID { get; set; }
            public string CreDate { get; set; }
            public string CreBy { get; set; }
            public string ModDate { get; set; }
            public string ModBy { get; set; }
            public string SCSPartID { get; set; }
            public string LaborAmt { get; set; }
            public string LaborRate { get; set; }
            public string LossCode { get; set; }
        }

        public class ClaimDetailQuote
        {
            public int ClaimDetailID { get; set; }
            public int ClaimCompanyID { get; set; }
            public string QuoteID { get; set; }
            public string PartNo { get; set; }
            public DateTime DeliveryDate { get; set; }
            public string PartDesc { get; set; }
            public string InStock { get; set; }
            public int Qty { get; set; }
            public decimal ListPrice { get; set; }
            public decimal YourCost { get; set; }
            public byte OEM { get; set; }
            public decimal PartCost { get; set; }
            public decimal Shipping { get; set; }
            public string OrderID { get; set; }
            public decimal TotalOrderCost { get; set; }
        }

        public class OrderRequest
        {
            public string adjuster { get; set; }
            public string adjusteremail { get; set; }
            public string contractnumber { get; set; }
            public string authorizationnumber { get; set; }
            public string repairordernumber { get; set; }
            public string owner { get; set; }
            public string vin { get; set; }
            public string mileage { get; set; }
            public string repairsite { get; set; }
            public string address { get; set; }
            public string postalcode { get; set; }
            public string phone { get; set; }
            public string contact { get; set; }
            public string warranty { get; set; }
            public string eocmileage { get; set; }
            public string eocdate { get; set; }
            public string note { get; set; }
            public OrderRequestPart[] parts { get; set; }
        }
        public class OrderRequestPart
        {
            public string quoteid { get; set; }
            public string partnumber { get; set; }
            public int quantity { get; set; }
            public bool aftermarketoption { get; set; }
        }

        public class PartSearch
        {
            public string ClaimDetailID { get; set; }
            public string PartNum { get; set; }
            public float QtyRequested { get; set; }
            public ShipTo ShipTo { get; set; }
            public BillTo BillTo { get; set; }
        }
        public class ShipTo
        {
            public string ShipToPartyID { get; set; }
            public string ShipToName { get; set; }
            public string ShipToAdd1 { get; set; }
            public string ShipToAdd2 { get; set; }
            public string ShipToCity { get; set; }
            public string ShipToState { get; set; }
            public string ShipToPostalCode { get; set; }
            public string ShipToCountry { get; set; }
            public string ShipToPhone { get; set; }
            public string ShipToEmail { get; set; }
            public string ShipToContact { get; set; }
        }
        public class BillTo
        {
            public string BillToPartyID { get; set; }
            public string BillToName { get; set; }
            public string BillToAdd1 { get; set; }
            public string BillToAdd2 { get; set; }
            public string BillToCity { get; set; }
            public string BillToState { get; set; }
            public string BillToCountry { get; set; }
            public string BillToPostalCode { get; set; }
        }
        public class BasicSublineValues
        {
            public int RequestedQTY { get; set; }
            public string ItemID { get; set; }
            public string MfrID { get; set; }
            public int SSFInventory { get; set; }
            public string ShipFrom { get; set; }
            public DateTime AvailDate { get; set; }
            public int OrderQty { get; set; }
            public int PromisedDays { get; set; }
            public string Status { get; set; }
            public string StatusMeaning { get; set; }
        }
        public class ClientPartRequest
        {
            public string PartSearched { get; set; }
            public string Mfr_Supplied { get; set; }
            public int RequestedQTY { get; set; }
            public string ShipFromRequested { get; set; }
            public string LineOrSubline { get; set; }
        }
        public class ClientPartResponse
        {
            public string SupplierItemID { get; set; }
            public string MfrCode { get; set; }
            public string ShipFrom { get; set; }
            public string QtyAvailForWholesale { get; set; }
            public int OrderQty { get; set; }
            public DateTime? PromisedShipDate { get; set; }
            public string Status { get; set; }
            public string StatusDescr { get; set; }
        }
        public class CatalogSearchData
        {
            public class ItemIDs
            {
                public string SupplierItemIdFormatted { get; set; }
                public CatalogSearch CatSearch { get; set; }
                public int SearchID_Sequence { get; set; }
                public string HazardMaterialInt { get; set; }
                public Availability Available { get; set; }
                public string PrimaryShipFrom_Location { get; set; }
            }
            public class CatalogSearch
            {
                public string CatalogMake { get; set; }
                public string CatalogYear { get; set; }
                public string CatlogModel { get; set; }
                public CatalogGroup CatalogGroup { get; set; }
                public CatalogCategory CatalogCategory { get; set; }
                public CatalogSearchID CatalogSearchID { get; set; } // Not in use
            }
            public class CatalogGroup
            {
                public string ID { get; set; }
                public string SearchLogID { get; set; } // Not In use
            }
            public class CatalogCategory
            {
                public string ID { get; set; }
                public string SearchLogID { get; set; }
            }
            public class CatalogSearchID // Not in use
            {
                public string ID { get; set; } // Not in use
                public int Sequence { get; set; }// Not in use
            }
            public class Availability
            {
                public int SF { get; set; }
                public int OC { get; set; }
                public int LB { get; set; }
                public int SD { get; set; }
                public int PH { get; set; }
                public int LA { get; set; }
                public int AT { get; set; }
            }
        }
        public class AddQuote : CatalogSearchData
        {
            public string AddQuoteAck { get; set; }
        }
        public class AcknowledgePurchaseOrder : CatalogSearchData
        {
            public string PurchaseOrderAck { get; set; }
        }
        public class AddRequestForQuote : CatalogSearchData
        {
            public string AddRequestForQuoteAck { get; set; }
        }
        public class ProcessPurchaseOrder : CatalogSearchData
        {
            public string ProcessPurchaseOrderAck { get; set; }
        }
        public class SoapParams
        {
            // These are used to create the Document
            public string User { get; set; }
            public string Pass { get; set; }
            public DateTime Date { get; set; }
            public string Regc { get; set; }
            public string Country { get; set; }

            // These are part of the request
            public string Action { get; set; }
            public Uri URL { get; set; }
        }

        public static class testXML
        {
            public static DataSet BindXML()
            {
                var url = "C:\\VeritasRepos\\SSF_Parts\\Test_XML_Files\\AddQuote_Response.xml";
                var httpContent = new StringContent(url, Encoding.UTF8, "text/xml");
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                    HttpResponseMessage response = client.GetAsync(url).Result;
                    DataSet ds = new();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        XDocument xdoc = XDocument.Parse(response.Content.ReadAsStringAsync().Result);
                        StringReader sr = new(xdoc.ToString());
                        ds.ReadXml(sr);
                    }
                    return ds;
                }
            }

            public static XmlTextWriter GetXmlString(string strFile)
            {
                // Load the xml file into XmlDocument object.
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.Load(strFile);
                }
                catch (XmlException e)
                {
                    Console.WriteLine(e.Message);
                }
                // Now create StringWriter object to get data from xml document.
                StringWriter sw = new StringWriter();
                XmlTextWriter xw = new XmlTextWriter(sw);
                xmlDoc.WriteTo(xw);
                return xw;
            }
        }

        public static Public_Classes.ClaimDetailQuote FillClaimDetailQuoteForTesting(Dictionary<string, string> dic)
        {
            Public_Classes.ClaimDetailQuote cdq = new();
            cdq.ClaimCompanyID = 1; // int.Parse(GetValueFromClaimQuoteDictionary(dic, "ClaimComaanyID"));
            cdq.ClaimDetailID = 1101;
            cdq.DeliveryDate = System.DateTime.Now;
            cdq.InStock = "yes";
            cdq.ListPrice = 29.23M;
            cdq.OEM = 1;
            cdq.OrderID = "orderID123";
            cdq.PartCost = 13.13M;
            cdq.PartDesc = "PartDescr";
            cdq.PartNo = "PartNumber";
            cdq.Qty = 99;
            cdq.QuoteID = "";
            cdq.Shipping = 38.32M;
            cdq.TotalOrderCost = 1010;
            cdq.YourCost = 1212.12M;
            return cdq;
        }
    }
}


