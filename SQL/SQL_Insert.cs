﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBO;
using SSF_AutoParts.SQL;

namespace SSF_AutoParts.SQL
{
    public class SQL_Insert : SQL_Master
    {
        public static bool InsertIntoClaimDetailQuote(Public_Classes.ClaimDetailQuote cdq)
        {
            try
            {
                MasterSql = "Select * from ClaimDetailQuote";
                SQL_Master sm = new();
                sm.OpenDB(true);
                SQL_Master.SQLDB.NewRow();

                string sqlTemplate = "insert into ClaimDetailQuote (ClaimDetailID, ClaimCompanyID, ";
                sqlTemplate += "QuoteID, PartNo, DeliveryDate, PartDesc, InStock, Qty, ListPrice, ";
                sqlTemplate += "YourCost, OEM, PartCost, Shipping, OrderID, TotalOrderCost) ";
                sqlTemplate += "VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8}, {9}, {10}, ";
                sqlTemplate += "{11}, {12}, '{13}', {14})";
                string[] args = { cdq.ClaimDetailID.ToString(), cdq.ClaimCompanyID.ToString(), cdq.QuoteID, cdq.PartNo, cdq.DeliveryDate.ToString(), cdq.PartDesc, cdq.InStock, cdq.Qty.ToString(), cdq.ListPrice.ToString(), cdq.YourCost.ToString(), cdq.OEM.ToString(), cdq.PartCost.ToString(), cdq.Shipping.ToString(), cdq.OrderID, cdq.TotalOrderCost.ToString() };

                MasterSql = string.Format(sqlTemplate, args);
                sm.RunSQL();
            }
            catch(Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                if (Globals.IsTest)
                {
                    Console.ReadKey();
                }
                return false;
            }
            return true;
        }

        public static bool InsertIntoClaimDetailPurchase(Public_Classes.ClaimDetailPurchase cdp)
        {
            try
            {
                MasterSql = "Select * from ClaimDetailPurchase";
                SQL_Master sm = new();
                sm.OpenDB(true);
                SQL_Master.SQLDB.NewRow();

                string sqlTemplate = "insert into ClaimDetailPurchase (ClaimID, ClaimDetailID, ClaimCompanyID, "; //3
                sqlTemplate += "QuoteID, PartNo, DeliveryDate, PartDesc, InStock, Qty, ListPrice, "; // 7
                sqlTemplate += "YourCost, OEM, PartCost, Shipping, OrderID, TotalOrderCost, OrderBy, OrderDate) "; // 8
                sqlTemplate += "VALUES ({0}, {1}, {2}, '{3}', '{4}', '{5}', '{6}', {7}, {8}, {9}, {10}, ";
                sqlTemplate += "{11}, {12}, {13}, '{14}', {15}, {16}, '{17}')";
                string[] args = { cdp.ClaimID.ToString(), cdp.ClaimDetailID.ToString(), cdp.ClaimCompanyID.ToString(), cdp.QuoteID, cdp.PartNo, cdp.DeliveryDate.ToString(), cdp.PartDesc, cdp.InStock, cdp.Qty.ToString(), cdp.ListPrice.ToString(), cdp.YourCost.ToString(), Convert.ToInt32(cdp.OEM).ToString(), cdp.PartCost.ToString(), cdp.Shipping.ToString(), cdp.OrderID, cdp.TotalOrderCost.ToString(), cdp.OrderBy.ToString(), cdp.OrderDate.ToString() };
                
                MasterSql = string.Format(sqlTemplate, args);              
                sm.RunSQL();
                sm.SaveDB();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                if (Globals.IsTest)
                {
                    Console.ReadKey();
                }
                return false;
            }
            return true;
        }

        public static bool InsertIntoClaimDetailPurchaseSubset(Public_Classes.ClaimDetailPurchase_Subset cps)
        {
            // Value which cannot be NULL: ClaimID, InStock, Qty, ListPrice, YourCost, OEM, PartCost, Shipping, Total OrderCost
            try
            {
                MasterSql = "select * from claimdetailpurchase " +
                "where claimdetailquoteid = " + Globals.ClaimDetailQuoteID;
                SQL_Master sm = new();
                sm.OpenDB(true);
                SQL_Master.SQLDB.NewRow();

                string sqlTemplate = "insert into ClaimDetailPurchase (ClaimID, InStock, Qty, ListPrice, YourCost, OEM, PartCost, Shipping, TotalOrderCost) ";
                sqlTemplate += "VALUES ( {0}, '{1}', {2}, {3}, {4}, {5}, {6}, {7}, {8} ) ";
                string[] args = { cps.ClaimID.ToString(), cps.InStock, cps.Qty.ToString(), cps.ListPrice.ToString(), cps.YourCost.ToString(), Convert.ToInt32(cps.OEM).ToString(), cps.PartCost.ToString(), cps.Shipping.ToString(), cps.TotalOrderCost.ToString()};
                             
                MasterSql = string.Format(sqlTemplate, args);
                sm.RunSQL();
                sm.SaveDB();                
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                if (Globals.IsTest)
                {
                    Console.ReadKey();
                }
                return false;
            }

            return true;
        }










    }
}
