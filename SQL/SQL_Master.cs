﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBO;
using System.Configuration;
using System.Data.SqlClient;

namespace SSF_AutoParts.SQL
{
    public class SQL_Master
    {
        public static string MasterSql = string.Empty;
        public static string MasterConnString = string.Empty;
        public static clsDBO SQLDB = new clsDBO();


        public bool OpenDB(bool expectedRowsReturned)
        {
            SQLDB.OpenDB(MasterSql, MasterConnString);
            if (SQLDB.RowCount != 0)
            {
                if (!expectedRowsReturned)
                {
                    Console.WriteLine("Rows Returned!" + Environment.NewLine + "SQL = " + MasterSql);
                    return false;
                }
                else
                {                    
                    return false;
                }
            }
            return true;            
        }

        public bool RunSQL()
        {
            try
            {
                SQLDB.RunSQL(MasterSql, MasterConnString);
            }
            catch(Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                return false;
            }

            return true;
        }

        public bool SaveDB()
        {
            try
            {
                SQLDB.SaveDB();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                return false;
            }
            return true;
        }
    }
}
