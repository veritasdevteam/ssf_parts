﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBO;

namespace SSF_AutoParts.SQL
{
    public class SQL_Select : SQL_Master
    {
        public static Public_Classes.ShipTo Ship = new Public_Classes.ShipTo();

        public void SetPartNoFromClaimDetail()
        {
            string partNo = string.Empty;
            SQL_Master sm = new();
            MasterSql = "Select PartNo from ClaimDetail where ClaimDetailID = " + Globals.ClaimDetailID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                partNo = SQLDB.get_Fields("PartNo");
                int len = partNo.Length;
                partNo = partNo.Substring(len - 4);

                Globals.PartNo = ushort.Parse(partNo);
            }

           
        }

        public string GetVIN()
        {
            MasterSql = "select * from claim cl " +
                    "inner join contract c on c.contractid = cl.contractid " +
                    "where claimid = " + Globals.ClaimID;
            SQL_Master sm = new();
            sm.OpenDB(true);
            string VIN = string.Empty;
            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                VIN = SQLDB.get_Fields("vin");
            }

            return VIN;

        }

        public void SetJobNoFromClaimDetail()
        {
            string jobNo = string.Empty;
            SQL_Master sm = new();
            MasterSql = "Select JobNo from ClaimDetail where ClaimDetailID = " + Globals.ClaimDetailID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                Globals.JobNo = SQLDB.get_Fields("JobNo");
            }           
        }


        public void SetReqQtyFromClaimDetail()
        {           
            SQL_Master sm = new();
            MasterSql = "Select ReqQty from ClaimDetail where ClaimDetailID = " + Globals.ClaimDetailID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                Globals.ReqQty = byte.Parse(SQLDB.get_Fields("ReqQty"));
            }           
        }
        public void SetClaimDetailQuoteIDfromClaimDetailQuote()
        {
            SQL_Master sm = new();
            MasterSql = "Select ClaimDetailQuoteID from ClaimDetailQuote where ClaimDetailID = " + Globals.ClaimDetailID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                Globals.ClaimDetailQuoteID = int.Parse(SQLDB.get_Fields("ClaimDetailQuoteID"));
            }
        }

        public void SetClaimIDFromClaimDetail()
        {
            SQL_Master sm = new();
            MasterSql = "Select ClaimID from ClaimDetail where ClaimDetailID = " + Globals.ClaimDetailID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                Globals.ClaimID = int.Parse(SQLDB.get_Fields("ClaimID"));
            }
            else
            {
                Console.WriteLine("ERROR! NO ROW RETURNED for ClaimDetailID = " + Globals.ClaimDetailID + "!!");
                if (Globals.IsTest)
                {
                    Console.ReadKey();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }

        // Gets the ContractID, RONumber, ClaimNo, and LossMile from the Claim:
        public Public_Classes.ClaimData GetClaimData()
        {
            SQL_Master sm = new();
            Public_Classes.ClaimData cd = new();
            MasterSql = "select * from claim where claimid = " + Globals.ClaimID;
            sm.OpenDB(true);
            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                cd.ContractID = int.Parse(SQLDB.get_Fields("ContractID"));
                cd.RONumber = SQLDB.get_Fields("RONumber").Trim();
                cd.ClaimNo = SQLDB.get_Fields("ClaimNo").Trim();
                cd.LossMile = int.Parse(SQLDB.get_Fields("lossMile"));
            }

            return cd;
        }

        public void FillServiceCenterData()
        {
            SQL_Master sm = new();
            MasterSql = "Select ServiceCenterID from Claim where ClaimID = " + Globals.ClaimID;
            sm.OpenDB(true);

            if (SQLDB.RowCount > 0)
            {
                SQLDB.GetRow();
                int serviceCenterID = int.Parse(SQLDB.get_Fields("ServiceCenterID"));
                MasterSql = "Select * from ServiceCenter where ServiceCenterID = " + serviceCenterID;
                sm.OpenDB(true);
                if (SQLDB.RowCount > 0)
                {
                    SQLDB.GetRow();
                    Ship.ShipToPartyID = SQLDB.get_Fields("ServiceCenterNo");
                    Ship.ShipToName = SQLDB.get_Fields("ServiceCenterName");
                    Ship.ShipToAdd1 = SQLDB.get_Fields("Addr1");
                    Ship.ShipToAdd2 = SQLDB.get_Fields("Addr2");
                    Ship.ShipToCity = SQLDB.get_Fields("City");
                    Ship.ShipToState = SQLDB.get_Fields("State");
                    Ship.ShipToPostalCode = SQLDB.get_Fields("Zip");
                    Ship.ShipToEmail = SQLDB.get_Fields("EMail");
                    Ship.ShipToPhone = SQLDB.get_Fields("Phone");
                    Ship.ShipToContact = SQLDB.get_Fields("Contact");
                }
            }
        }
    }
}
