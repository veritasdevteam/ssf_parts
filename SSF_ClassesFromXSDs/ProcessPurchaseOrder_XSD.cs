﻿//using System.Xml.Serialization;

//[System.Diagnostics.DebuggerStepThroughAttribute()]
//public partial class ProcessPurchaseOrderXML : ItemIds.BusinessObjectDocument
//{
//    //private ProcessPurchaseOrderDataArea dataAreaField;
//    //public ProcessPurchaseOrderDataArea DataArea
//    //{
//    //    get
//    //    {
//    //        return this.dataAreaField;
//    //    }
//    //    set
//    //    {
//    //        this.dataAreaField = value;
//    //    }
//    //}
//}


//[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//[System.SerializableAttribute()]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//public partial class ProcessPurchaseOrderDataArea : ItemIds.DataArea
//{
//    private Process processField;

//    //private OrderLineBase.PurchaseOrder[] purchaseOrderField;

//    public Process Process
//    {
//        get
//        {
//            return this.processField;
//        }
//        set
//        {
//            this.processField = value;
//        }
//    }

    
//    [System.Xml.Serialization.XmlElementAttribute("PurchaseOrder")]
//    //public OrderLineBase.PurchaseOrder[] PurchaseOrder
//    //{
//    //    get
//    //    {
//    //        return this.purchaseOrderField;
//    //    }
//    //    set
//    //    {
//    //        this.purchaseOrderField = value;
//    //    }
//    //}
//}


//[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//[System.SerializableAttribute()]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//public partial class Process : AcknowledgableVerb
//{
//}


//[System.Xml.Serialization.XmlIncludeAttribute(typeof(Process))]
//[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//[System.SerializableAttribute()]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//public abstract partial class AcknowledgableVerb : ConfirmableVerb
//{

//    private Enums.AcknowledgementType acknowledgeField;

//    private bool acknowledgeFieldSpecified;


//    [System.Xml.Serialization.XmlAttributeAttribute()]
//    public Enums.AcknowledgementType acknowledge
//    {
//        get
//        {
//            return this.acknowledgeField;
//        }
//        set
//        {
//            this.acknowledgeField = value;
//        }
//    }

//    [System.Xml.Serialization.XmlIgnoreAttribute()]
//    public bool acknowledgeSpecified
//    {
//        get
//        {
//            return this.acknowledgeFieldSpecified;
//        }
//        set
//        {
//            this.acknowledgeFieldSpecified = value;
//        }
//    }
//}




//[System.Xml.Serialization.XmlIncludeAttribute(typeof(AcknowledgableVerb))]
//[System.Xml.Serialization.XmlIncludeAttribute(typeof(Process))]
//public partial class ConfirmableVerb : Verb
//{     
//}


//[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConfirmableVerb))]
//[System.Xml.Serialization.XmlIncludeAttribute(typeof(AcknowledgableVerb))]
//[System.Xml.Serialization.XmlIncludeAttribute(typeof(Process))]
//[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//[System.SerializableAttribute()]
//[System.Diagnostics.DebuggerStepThroughAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//public partial class Verb
//{
//}

//public partial class TaxCode
//{
//}

//public partial class License
//{
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Temperature
//    {

//        private TemperatureScale scaleField;

//        private System.DateTime entryDateTimeField;

//        private bool entryDateTimeFieldSpecified;

//        private decimal valueField;


//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public TemperatureScale scale
//        {
//            get
//            {
//                return this.scaleField;
//            }
//            set
//            {
//                this.scaleField = value;
//            }
//        }

//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public System.DateTime entryDateTime
//        {
//            get
//            {
//                return this.entryDateTimeField;
//            }
//            set
//            {
//                this.entryDateTimeField = value;
//            }
//        }


//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool entryDateTimeSpecified
//        {
//            get
//            {
//                return this.entryDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.entryDateTimeFieldSpecified = value;
//            }
//        }


//        [System.Xml.Serialization.XmlTextAttribute()]
//        public decimal Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public enum TemperatureScale
//    {
//        Celsius,
//        Fahrenheit,
//        Kelvin
//    }


//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ItemIdType))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyAssignedItemId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class ItemIdBase
//    {
//        private string idField;
//        private ItemId eANUCC13Field;
//        private ItemId uPCField;

//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

//        public ItemId EANUCC13
//        {
//            get
//            {
//                return this.eANUCC13Field;
//            }
//            set
//            {
//                this.eANUCC13Field = value;
//            }
//        }

//        public ItemId UPC
//        {
//            get
//            {
//                return this.uPCField;
//            }
//            set
//            {
//                this.uPCField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("EANUCC13", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class ItemId
//    {

//        private string urlField;

//        private string valueField;


//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string url
//        {
//            get
//            {
//                return this.urlField;
//            }
//            set
//            {
//                this.urlField = value;
//            }
//        }


//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }


//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderLineBase))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderSchedule))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderSchedule))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderSubLine))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderLine))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderLine))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class DocumentLine
//    {
//        private string lineNumberField;

//        public string LineNumber
//        {
//            get
//            {
//                return this.lineNumberField;
//            }
//            set
//            {
//                this.lineNumberField = value;
//            }
//        }
//    }


//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderSchedule))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderSchedule))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderSubLine))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderLine))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderLine))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class OrderLineBase : DocumentLine
//    {
//        private OrderItem orderItemField;
//        private Quantity orderQuantityField;
//        private AmountPerQuantity unitPriceField;
//        private Amount extendedPriceField;
//        private Amount totalAmountField;
//        private string earliestShipDateField;
//        private Quantity backOrderedQuantityField;
//        private string needDeliveryDateField;
//        private string promisedDeliveryDateField;
//        private string promisedShipDateField;
//        private bool taxWithholdingExemptField;
//        private bool taxWithholdingExemptFieldSpecified;
//        private Description[] descriptionField;
//        private Note[] noteField;
//        private Note[] specialHandlingNoteField;
//        private Note[] shippingNoteField;
//        private Temperature[] actualTemperatureField;
//        private Temperature[] loadingTemperatureField;
//        private Temperature[] deliveryTemperatureField;
//        private Temperature maximumTemperatureField;
//        private Temperature minimumTemperatureField;
//        private License[] licenseField;
//        private string freightClassField;
//        private Note[] shipNoteField;
//        private string priorityField;
//        private string reasonCodeField;
//        private string packingMaterialField;
//        private bool dropShipIndField;
//        private bool dropShipIndFieldSpecified;
//        private bool backOrderedIndField;
//        private bool backOrderedIndFieldSpecified;
//        private bool shipPriorToDueDateIndField;
//        private bool shipPriorToDueDateIndFieldSpecified;
//        private bool substitutionAllowedIndField;
//        private bool substitutionAllowedIndFieldSpecified;
//        private Distribution[] distributionField;
//        private TransportationTerm transportationTermField;

//        public OrderItem OrderItem
//        {
//            get
//            {
//                return this.orderItemField;
//            }
//            set
//            {
//                this.orderItemField = value;
//            }
//        }
//        public Quantity OrderQuantity
//        {
//            get
//            {
//                return this.orderQuantityField;
//            }
//            set
//            {
//                this.orderQuantityField = value;
//            }
//        }
//        public AmountPerQuantity UnitPrice
//        {
//            get
//            {
//                return this.unitPriceField;
//            }
//            set
//            {
//                this.unitPriceField = value;
//            }
//        }
//        public Amount ExtendedPrice
//        {
//            get
//            {
//                return this.extendedPriceField;
//            }
//            set
//            {
//                this.extendedPriceField = value;
//            }
//        }
//                public Amount TotalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }


//        public string EarliestShipDate
//        {
//            get
//            {
//                return this.earliestShipDateField;
//            }
//            set
//            {
//                this.earliestShipDateField = value;
//            }
//        }


//        public Quantity BackOrderedQuantity
//        {
//            get
//            {
//                return this.backOrderedQuantityField;
//            }
//            set
//            {
//                this.backOrderedQuantityField = value;
//            }
//        }


//        public string NeedDeliveryDate
//        {
//            get
//            {
//                return this.needDeliveryDateField;
//            }
//            set
//            {
//                this.needDeliveryDateField = value;
//            }
//        }


//        public string PromisedDeliveryDate
//        {
//            get
//            {
//                return this.promisedDeliveryDateField;
//            }
//            set
//            {
//                this.promisedDeliveryDateField = value;
//            }
//        }


//        public string PromisedShipDate
//        {
//            get
//            {
//                return this.promisedShipDateField;
//            }
//            set
//            {
//                this.promisedShipDateField = value;
//            }
//        }


//        public bool TaxWithholdingExempt
//        {
//            get
//            {
//                return this.taxWithholdingExemptField;
//            }
//            set
//            {
//                this.taxWithholdingExemptField = value;
//            }
//        }


//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool TaxWithholdingExemptSpecified
//        {
//            get
//            {
//                return this.taxWithholdingExemptFieldSpecified;
//            }
//            set
//            {
//                this.taxWithholdingExemptFieldSpecified = value;
//            }
//        }


//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("SpecialHandlingNote")]
//        public Note[] SpecialHandlingNote
//        {
//            get
//            {
//                return this.specialHandlingNoteField;
//            }
//            set
//            {
//                this.specialHandlingNoteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ShippingNote")]
//        public Note[] ShippingNote
//        {
//            get
//            {
//                return this.shippingNoteField;
//            }
//            set
//            {
//                this.shippingNoteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ActualTemperature")]
//        public Temperature[] ActualTemperature
//        {
//            get
//            {
//                return this.actualTemperatureField;
//            }
//            set
//            {
//                this.actualTemperatureField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("LoadingTemperature")]
//        public Temperature[] LoadingTemperature
//        {
//            get
//            {
//                return this.loadingTemperatureField;
//            }
//            set
//            {
//                this.loadingTemperatureField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("DeliveryTemperature")]
//        public Temperature[] DeliveryTemperature
//        {
//            get
//            {
//                return this.deliveryTemperatureField;
//            }
//            set
//            {
//                this.deliveryTemperatureField = value;
//            }
//        }

        
//        public Temperature MaximumTemperature
//        {
//            get
//            {
//                return this.maximumTemperatureField;
//            }
//            set
//            {
//                this.maximumTemperatureField = value;
//            }
//        }

        
//        public Temperature MinimumTemperature
//        {
//            get
//            {
//                return this.minimumTemperatureField;
//            }
//            set
//            {
//                this.minimumTemperatureField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("License")]
//        public License[] License
//        {
//            get
//            {
//                return this.licenseField;
//            }
//            set
//            {
//                this.licenseField = value;
//            }
//        }

        
//        public string FreightClass
//        {
//            get
//            {
//                return this.freightClassField;
//            }
//            set
//            {
//                this.freightClassField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ShipNote")]
//        public Note[] ShipNote
//        {
//            get
//            {
//                return this.shipNoteField;
//            }
//            set
//            {
//                this.shipNoteField = value;
//            }
//        }

        
//        public string Priority
//        {
//            get
//            {
//                return this.priorityField;
//            }
//            set
//            {
//                this.priorityField = value;
//            }
//        }

        
//        public string ReasonCode
//        {
//            get
//            {
//                return this.reasonCodeField;
//            }
//            set
//            {
//                this.reasonCodeField = value;
//            }
//        }

        
//        public string PackingMaterial
//        {
//            get
//            {
//                return this.packingMaterialField;
//            }
//            set
//            {
//                this.packingMaterialField = value;
//            }
//        }

        
//        public bool DropShipInd
//        {
//            get
//            {
//                return this.dropShipIndField;
//            }
//            set
//            {
//                this.dropShipIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool DropShipIndSpecified
//        {
//            get
//            {
//                return this.dropShipIndFieldSpecified;
//            }
//            set
//            {
//                this.dropShipIndFieldSpecified = value;
//            }
//        }

        
//        public bool BackOrderedInd
//        {
//            get
//            {
//                return this.backOrderedIndField;
//            }
//            set
//            {
//                this.backOrderedIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool BackOrderedIndSpecified
//        {
//            get
//            {
//                return this.backOrderedIndFieldSpecified;
//            }
//            set
//            {
//                this.backOrderedIndFieldSpecified = value;
//            }
//        }

        
//        public bool ShipPriorToDueDateInd
//        {
//            get
//            {
//                return this.shipPriorToDueDateIndField;
//            }
//            set
//            {
//                this.shipPriorToDueDateIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool ShipPriorToDueDateIndSpecified
//        {
//            get
//            {
//                return this.shipPriorToDueDateIndFieldSpecified;
//            }
//            set
//            {
//                this.shipPriorToDueDateIndFieldSpecified = value;
//            }
//        }

        
//        public bool SubstitutionAllowedInd
//        {
//            get
//            {
//                return this.substitutionAllowedIndField;
//            }
//            set
//            {
//                this.substitutionAllowedIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool SubstitutionAllowedIndSpecified
//        {
//            get
//            {
//                return this.substitutionAllowedIndFieldSpecified;
//            }
//            set
//            {
//                this.substitutionAllowedIndFieldSpecified = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Distribution")]
//        public Distribution[] Distribution
//        {
//            get
//            {
//                return this.distributionField;
//            }
//            set
//            {
//                this.distributionField = value;
//            }
//        }

        
//        public TransportationTerm TransportationTerm
//        {
//            get
//            {
//                return this.transportationTermField;
//            }
//            set
//            {
//                this.transportationTermField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class OrderItem
//    {

//        private ItemIds itemIdsField;

//        private Status itemStatusField;

//        private string[] commodityField;

//        private ItemCategoryId itemCategoryIdField;

//        private string itemTypeField;

//        private Description[] definitionField;

//        private Description[] descriptionField;

//        private string[] serialNumberField;

//        private string parentSerialNumberField;

//        private HazardousMaterial hazardousMaterialField;

//        private Id productLineField;

//        private Lot[] lotField;

//        private FeatureValue featureValueField;

//        private Attachment[] attachmentsField;

//        private UserArea userAreaField;

        
//        public ItemIds ItemIds
//        {
//            get
//            {
//                return this.itemIdsField;
//            }
//            set
//            {
//                this.itemIdsField = value;
//            }
//        }

        
//        public Status ItemStatus
//        {
//            get
//            {
//                return this.itemStatusField;
//            }
//            set
//            {
//                this.itemStatusField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Commodity")]
//        public string[] Commodity
//        {
//            get
//            {
//                return this.commodityField;
//            }
//            set
//            {
//                this.commodityField = value;
//            }
//        }

        
//        public ItemCategoryId ItemCategoryId
//        {
//            get
//            {
//                return this.itemCategoryIdField;
//            }
//            set
//            {
//                this.itemCategoryIdField = value;
//            }
//        }

        
//        public string ItemType
//        {
//            get
//            {
//                return this.itemTypeField;
//            }
//            set
//            {
//                this.itemTypeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Definition")]
//        public Description[] Definition
//        {
//            get
//            {
//                return this.definitionField;
//            }
//            set
//            {
//                this.definitionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("SerialNumber")]
//        public string[] SerialNumber
//        {
//            get
//            {
//                return this.serialNumberField;
//            }
//            set
//            {
//                this.serialNumberField = value;
//            }
//        }

        
//        public string ParentSerialNumber
//        {
//            get
//            {
//                return this.parentSerialNumberField;
//            }
//            set
//            {
//                this.parentSerialNumberField = value;
//            }
//        }

        
//        public HazardousMaterial HazardousMaterial
//        {
//            get
//            {
//                return this.hazardousMaterialField;
//            }
//            set
//            {
//                this.hazardousMaterialField = value;
//            }
//        }

        
//        public Id ProductLine
//        {
//            get
//            {
//                return this.productLineField;
//            }
//            set
//            {
//                this.productLineField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Lot")]
//        public Lot[] Lot
//        {
//            get
//            {
//                return this.lotField;
//            }
//            set
//            {
//                this.lotField = value;
//            }
//        }

        
//        public FeatureValue FeatureValue
//        {
//            get
//            {
//                return this.featureValueField;
//            }
//            set
//            {
//                this.featureValueField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Attachment[] Attachments
//        {
//            get
//            {
//                return this.attachmentsField;
//            }
//            set
//            {
//                this.attachmentsField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class ItemIds
//    {

//        private ItemIdType itemIdField;

//        private PartyAssignedItemId[] itemsField;

//        private ItemsChoiceType7[] itemsElementNameField;

        
//        public ItemIdType ItemId
//        {
//            get
//            {
//                return this.itemIdField;
//            }
//            set
//            {
//                this.itemIdField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("BuyerItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlElementAttribute("CarrierItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlElementAttribute("CustomerItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlElementAttribute("ManufacturerItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlElementAttribute("ShipFromItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlElementAttribute("SupplierItemId", typeof(PartyAssignedItemId))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public PartyAssignedItemId[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemsChoiceType7[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyAssignedItemId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("ItemId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class ItemIdType : ItemIdBase
//    {

//        private Revision revisionField;

        
//        public Revision Revision
//        {
//            get
//            {
//                return this.revisionField;
//            }
//            set
//            {
//                this.revisionField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Revision
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("SupplierItemId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyAssignedItemId : ItemIdType
//    {

//        private PartyIdType assigningPartyIdField;

        
//        public PartyIdType AssigningPartyId
//        {
//            get
//            {
//                return this.assigningPartyIdField;
//            }
//            set
//            {
//                this.assigningPartyIdField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyAssignedPartyId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("AssigningPartyId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyIdType
//    {

//        private PartyId idField;

//        private PartyId sCACField;

//        private PartyId dUNSField;

        
//        public PartyId Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public PartyId SCAC
//        {
//            get
//            {
//                return this.sCACField;
//            }
//            set
//            {
//                this.sCACField = value;
//            }
//        }

        
//        public PartyId DUNS
//        {
//            get
//            {
//                return this.dUNSField;
//            }
//            set
//            {
//                this.dUNSField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("SCAC", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyId : PartyIdAny
//    {
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public abstract partial class PartyIdAny
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemsChoiceType7
//    {        
//        BuyerItemId,        
//        CarrierItemId,        
//        CustomerItemId,        
//        ManufacturerItemId,        
//        ShipFromItemId,        
//        SupplierItemId
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Status
//    {

//        private string codeField;

//        private Description[] descriptionField;

//        private StateChange[] changeField;

//        private UserArea userAreaField;

        
//        public string Code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Change")]
//        public StateChange[] Change
//        {
//            get
//            {
//                return this.changeField;
//            }
//            set
//            {
//                this.changeField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Description : LingualString
//    {

//        private string ownerField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string owner
//        {
//            get
//            {
//                return this.ownerField;
//            }
//            set
//            {
//                this.ownerField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Note))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Description))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Name))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class LingualString
//    {

//        private string langField;

//        private string valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
//        public string lang
//        {
//            get
//            {
//                return this.langField;
//            }
//            set
//            {
//                this.langField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Name : LingualString
//    {
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class StateChange
//    {

//        private string fromField;

//        private string toField;

//        private string changeDateField;

//        private Description[] descriptionField;

//        private UserArea userAreaField;

        
//        public string From
//        {
//            get
//            {
//                return this.fromField;
//            }
//            set
//            {
//                this.fromField = value;
//            }
//        }

        
//        public string To
//        {
//            get
//            {
//                return this.toField;
//            }
//            set
//            {
//                this.toField = value;
//            }
//        }

        
//        public string ChangeDate
//        {
//            get
//            {
//                return this.changeDateField;
//            }
//            set
//            {
//                this.changeDateField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class UserArea
//    {

//        private System.Xml.XmlElement[] anyField;

        
//        [System.Xml.Serialization.XmlAnyElementAttribute()]
//        public System.Xml.XmlElement[] Any
//        {
//            get
//            {
//                return this.anyField;
//            }
//            set
//            {
//                this.anyField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class ItemCategoryId : Id
//    {
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ItemCategoryId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Id
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class HazardousMaterial
//    {

//        private HazardousMaterialCode codeField;

//        private Description[] descriptionField;

//        private UserArea userAreaField;

        
//        public HazardousMaterialCode Code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
//    public partial class HazardousMaterialCode
//    {

//        private string issuingAgencyField;

//        private string valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string issuingAgency
//        {
//            get
//            {
//                return this.issuingAgencyField;
//            }
//            set
//            {
//                this.issuingAgencyField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Lot
//    {

//        private Id lotIdField;

//        private Id subLotIdField;

//        private TimePeriod effectivePeriodField;

//        private UserArea userAreaField;

        
//        public Id LotId
//        {
//            get
//            {
//                return this.lotIdField;
//            }
//            set
//            {
//                this.lotIdField = value;
//            }
//        }

        
//        public Id SubLotId
//        {
//            get
//            {
//                return this.subLotIdField;
//            }
//            set
//            {
//                this.subLotIdField = value;
//            }
//        }

        
//        public TimePeriod EffectivePeriod
//        {
//            get
//            {
//                return this.effectivePeriodField;
//            }
//            set
//            {
//                this.effectivePeriodField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class TimePeriod
//    {

//        private System.DateTime fromField;

//        private bool fromFieldSpecified;

//        private object itemField;

//        private bool inclusiveField;

//        public TimePeriod()
//        {
//            this.inclusiveField = true;
//        }

        
//        public System.DateTime From
//        {
//            get
//            {
//                return this.fromField;
//            }
//            set
//            {
//                this.fromField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool FromSpecified
//        {
//            get
//            {
//                return this.fromFieldSpecified;
//            }
//            set
//            {
//                this.fromFieldSpecified = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Duration", typeof(string), DataType = "duration")]
//        [System.Xml.Serialization.XmlElementAttribute("To", typeof(System.DateTime))]
//        public object Item
//        {
//            get
//            {
//                return this.itemField;
//            }
//            set
//            {
//                this.itemField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(true)]
//        public bool inclusive
//        {
//            get
//            {
//                return this.inclusiveField;
//            }
//            set
//            {
//                this.inclusiveField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class FeatureValue : NameValueBase
//    {

//        private TimePeriod effectivePeriodField;

//        private string[] qualificationField;

//        private string uOMField;

//        private PartyReference[] partyReferencesField;

//        private string sequenceField;

//        private UserArea userAreaField;

        
//        public TimePeriod EffectivePeriod
//        {
//            get
//            {
//                return this.effectivePeriodField;
//            }
//            set
//            {
//                this.effectivePeriodField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Qualification")]
//        public string[] Qualification
//        {
//            get
//            {
//                return this.qualificationField;
//            }
//            set
//            {
//                this.qualificationField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "NMTOKEN")]
//        public string UOM
//        {
//            get
//            {
//                return this.uOMField;
//            }
//            set
//            {
//                this.uOMField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public PartyReference[] PartyReferences
//        {
//            get
//            {
//                return this.partyReferencesField;
//            }
//            set
//            {
//                this.partyReferencesField = value;
//            }
//        }

        
//        public string Sequence
//        {
//            get
//            {
//                return this.sequenceField;
//            }
//            set
//            {
//                this.sequenceField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyReference
//    {

//        private PartyAssignedPartyId partyIdField;

//        private Name[] nameField;

//        private UserArea userAreaField;

        
//        public PartyAssignedPartyId PartyId
//        {
//            get
//            {
//                return this.partyIdField;
//            }
//            set
//            {
//                this.partyIdField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Name")]
//        public Name[] Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("BillToPartyId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyAssignedPartyId : PartyIdType
//    {

//        private PartyIdType assigningPartyIdField;

        
//        public PartyIdType AssigningPartyId
//        {
//            get
//            {
//                return this.assigningPartyIdField;
//            }
//            set
//            {
//                this.assigningPartyIdField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FeatureValue))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NameValue))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class NameValueBase
//    {

//        private NameValuePair nameValueField;

//        private Description[] descriptionField;

//        private Note[] noteField;

        
//        public NameValuePair NameValue
//        {
//            get
//            {
//                return this.nameValueField;
//            }
//            set
//            {
//                this.nameValueField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class NameValuePair
//    {

//        private string nameField;

//        private string typeField;

//        private string valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Note : LingualString
//    {

//        private string authorField;

//        private System.DateTime entryDateTimeField;

//        private bool entryDateTimeFieldSpecified;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string author
//        {
//            get
//            {
//                return this.authorField;
//            }
//            set
//            {
//                this.authorField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public System.DateTime entryDateTime
//        {
//            get
//            {
//                return this.entryDateTimeField;
//            }
//            set
//            {
//                this.entryDateTimeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool entryDateTimeSpecified
//        {
//            get
//            {
//                return this.entryDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.entryDateTimeFieldSpecified = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Attachment
//    {

//        private object itemField;

//        private ItemChoiceType1 itemElementNameField;

//        private string documentDateField;

//        private Description[] descriptionField;

//        private string fileTypeField;

//        private decimal fileSizeField;

//        private bool fileSizeFieldSpecified;

//        private Note[] noteField;

//        private LingualString[] titleField;

//        private UserArea userAreaField;

//        private bool inlineField;

//        public Attachment()
//        {
//            this.inlineField = false;
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("EmbeddedData", typeof(EmbeddedData))]
//        [System.Xml.Serialization.XmlElementAttribute("FileName", typeof(string))]
//        [System.Xml.Serialization.XmlElementAttribute("ISBN", typeof(string))]
//        [System.Xml.Serialization.XmlElementAttribute("URI", typeof(string), DataType = "anyURI")]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
//        public object Item
//        {
//            get
//            {
//                return this.itemField;
//            }
//            set
//            {
//                this.itemField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemChoiceType1 ItemElementName
//        {
//            get
//            {
//                return this.itemElementNameField;
//            }
//            set
//            {
//                this.itemElementNameField = value;
//            }
//        }

        
//        public string DocumentDate
//        {
//            get
//            {
//                return this.documentDateField;
//            }
//            set
//            {
//                this.documentDateField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public string FileType
//        {
//            get
//            {
//                return this.fileTypeField;
//            }
//            set
//            {
//                this.fileTypeField = value;
//            }
//        }

        
//        public decimal FileSize
//        {
//            get
//            {
//                return this.fileSizeField;
//            }
//            set
//            {
//                this.fileSizeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool FileSizeSpecified
//        {
//            get
//            {
//                return this.fileSizeFieldSpecified;
//            }
//            set
//            {
//                this.fileSizeFieldSpecified = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Title")]
//        public LingualString[] Title
//        {
//            get
//            {
//                return this.titleField;
//            }
//            set
//            {
//                this.titleField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(false)]
//        public bool inline
//        {
//            get
//            {
//                return this.inlineField;
//            }
//            set
//            {
//                this.inlineField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class EmbeddedData
//    {

//        private object dataField;

//        private string encodingField;

        
//        public object Data
//        {
//            get
//            {
//                return this.dataField;
//            }
//            set
//            {
//                this.dataField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string encoding
//        {
//            get
//            {
//                return this.encodingField;
//            }
//            set
//            {
//                this.encodingField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemChoiceType1
//    {

        
//        EmbeddedData,

        
//        FileName,

        
//        ISBN,

        
//        URI,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Quantity
//    {

//        private string uomField;

//        private decimal valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "NMTOKEN")]
//        public string uom
//        {
//            get
//            {
//                return this.uomField;
//            }
//            set
//            {
//                this.uomField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public decimal Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class AmountPerQuantity
//    {

//        private Amount amountField;

//        private Amount functionalAmoutField;

//        private Quantity perQuantityField;

        
//        public Amount Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

        
//        public Amount FunctionalAmout
//        {
//            get
//            {
//                return this.functionalAmoutField;
//            }
//            set
//            {
//                this.functionalAmoutField = value;
//            }
//        }

        
//        public Quantity PerQuantity
//        {
//            get
//            {
//                return this.perQuantityField;
//            }
//            set
//            {
//                this.perQuantityField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FunctionalAmount))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Amount
//    {

//        private string currencyField;

//        private decimal valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string currency
//        {
//            get
//            {
//                return this.currencyField;
//            }
//            set
//            {
//                this.currencyField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public decimal Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class FunctionalAmount : Amount
//    {

//        private decimal conversionFactorField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public decimal conversionFactor
//        {
//            get
//            {
//                return this.conversionFactorField;
//            }
//            set
//            {
//                this.conversionFactorField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Distribution
//    {

//        private Amounts amountField;

//        private string ledgerField;

//        private GLEntity gLEntityField;

//        private OrganizationalUnit businessField;

//        private string costCenterField;

//        private string profitCenterField;

//        private string fundField;

//        private Project projectField;

//        private UserArea userAreaField;

        
//        public Amounts Amount
//        {
//            get
//            {
//                return this.amountField;
//            }
//            set
//            {
//                this.amountField = value;
//            }
//        }

        
//        public string Ledger
//        {
//            get
//            {
//                return this.ledgerField;
//            }
//            set
//            {
//                this.ledgerField = value;
//            }
//        }

        
//        public GLEntity GLEntity
//        {
//            get
//            {
//                return this.gLEntityField;
//            }
//            set
//            {
//                this.gLEntityField = value;
//            }
//        }

        
//        public OrganizationalUnit Business
//        {
//            get
//            {
//                return this.businessField;
//            }
//            set
//            {
//                this.businessField = value;
//            }
//        }

        
//        public string CostCenter
//        {
//            get
//            {
//                return this.costCenterField;
//            }
//            set
//            {
//                this.costCenterField = value;
//            }
//        }

        
//        public string ProfitCenter
//        {
//            get
//            {
//                return this.profitCenterField;
//            }
//            set
//            {
//                this.profitCenterField = value;
//            }
//        }

        
//        public string Fund
//        {
//            get
//            {
//                return this.fundField;
//            }
//            set
//            {
//                this.fundField = value;
//            }
//        }

        
//        public Project Project
//        {
//            get
//            {
//                return this.projectField;
//            }
//            set
//            {
//                this.projectField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Amounts
//    {

//        private Amount actualField;

//        private FunctionalAmount[] convertedField;

        
//        public Amount Actual
//        {
//            get
//            {
//                return this.actualField;
//            }
//            set
//            {
//                this.actualField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Converted")]
//        public FunctionalAmount[] Converted
//        {
//            get
//            {
//                return this.convertedField;
//            }
//            set
//            {
//                this.convertedField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class GLEntity
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("Business", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class OrganizationalUnit
//    {

//        private string idField;

//        private string functionField;

//        private Name nameField;

//        private RelatedUnitType[] relatedUnitField;

//        private UserArea userAreaField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public string Function
//        {
//            get
//            {
//                return this.functionField;
//            }
//            set
//            {
//                this.functionField = value;
//            }
//        }

        
//        public Name Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("RelatedUnit")]
//        public RelatedUnitType[] RelatedUnit
//        {
//            get
//            {
//                return this.relatedUnitField;
//            }
//            set
//            {
//                this.relatedUnitField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class RelatedUnitType
//    {

//        private string relationshipField;

//        private OrganizationalUnit unitField;

        
//        public string Relationship
//        {
//            get
//            {
//                return this.relationshipField;
//            }
//            set
//            {
//                this.relationshipField = value;
//            }
//        }

        
//        public OrganizationalUnit Unit
//        {
//            get
//            {
//                return this.unitField;
//            }
//            set
//            {
//                this.unitField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Project : Noun
//    {

//        private string idField;

//        private string statusField;

//        private System.DateTime documentDateTimeField;

//        private bool documentDateTimeFieldSpecified;

//        private TimePeriodAny effectivePeriodField;

//        private AuthorizationType[] authorizationField;

//        private string transactionTypeField;

//        private Amounts totalCostField;

//        private GLEntity sourceGLEntityField;

//        private NameValue[] gLElementField;

//        private ProjectResourceCategory[] projectResourceCategoryField;

//        private ProjectActivity[] activityField;

//        private DocumentReference[] documentReferencesField;

//        private Attachment[] attachmentsField;

//        private UserArea userAreaField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public string Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public System.DateTime DocumentDateTime
//        {
//            get
//            {
//                return this.documentDateTimeField;
//            }
//            set
//            {
//                this.documentDateTimeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool DocumentDateTimeSpecified
//        {
//            get
//            {
//                return this.documentDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.documentDateTimeFieldSpecified = value;
//            }
//        }

        
//        public TimePeriodAny EffectivePeriod
//        {
//            get
//            {
//                return this.effectivePeriodField;
//            }
//            set
//            {
//                this.effectivePeriodField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Authorization")]
//        public AuthorizationType[] Authorization
//        {
//            get
//            {
//                return this.authorizationField;
//            }
//            set
//            {
//                this.authorizationField = value;
//            }
//        }

        
//        public string TransactionType
//        {
//            get
//            {
//                return this.transactionTypeField;
//            }
//            set
//            {
//                this.transactionTypeField = value;
//            }
//        }

        
//        public Amounts TotalCost
//        {
//            get
//            {
//                return this.totalCostField;
//            }
//            set
//            {
//                this.totalCostField = value;
//            }
//        }

        
//        public GLEntity SourceGLEntity
//        {
//            get
//            {
//                return this.sourceGLEntityField;
//            }
//            set
//            {
//                this.sourceGLEntityField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("GLElement")]
//        public NameValue[] GLElement
//        {
//            get
//            {
//                return this.gLElementField;
//            }
//            set
//            {
//                this.gLElementField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ProjectResourceCategory")]
//        public ProjectResourceCategory[] ProjectResourceCategory
//        {
//            get
//            {
//                return this.projectResourceCategoryField;
//            }
//            set
//            {
//                this.projectResourceCategoryField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Activity")]
//        public ProjectActivity[] Activity
//        {
//            get
//            {
//                return this.activityField;
//            }
//            set
//            {
//                this.activityField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentReference[] DocumentReferences
//        {
//            get
//            {
//                return this.documentReferencesField;
//            }
//            set
//            {
//                this.documentReferencesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Attachment[] Attachments
//        {
//            get
//            {
//                return this.attachmentsField;
//            }
//            set
//            {
//                this.attachmentsField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class TimePeriodAny
//    {

//        private string fromField;

//        private string itemField;

//        private ItemChoiceType itemElementNameField;

//        private bool inclusiveField;

//        public TimePeriodAny()
//        {
//            this.inclusiveField = true;
//        }

        
//        public string From
//        {
//            get
//            {
//                return this.fromField;
//            }
//            set
//            {
//                this.fromField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Duration", typeof(string), DataType = "duration")]
//        [System.Xml.Serialization.XmlElementAttribute("To", typeof(string))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
//        public string Item
//        {
//            get
//            {
//                return this.itemField;
//            }
//            set
//            {
//                this.itemField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemChoiceType ItemElementName
//        {
//            get
//            {
//                return this.itemElementNameField;
//            }
//            set
//            {
//                this.itemElementNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(true)]
//        public bool inclusive
//        {
//            get
//            {
//                return this.inclusiveField;
//            }
//            set
//            {
//                this.inclusiveField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemChoiceType
//    {

        
//        Duration,

        
//        To,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class AuthorizationType
//    {

//        private string idField;

//        private string typeField;

//        private Status statusField;

//        private string dateAuthorizedField;

//        private TimePeriodAny effectivePeriodField;

//        private string authorizationCodeField;

//        private UserArea userAreaField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public string Type
//        {
//            get
//            {
//                return this.typeField;
//            }
//            set
//            {
//                this.typeField = value;
//            }
//        }

        
//        public Status Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public string DateAuthorized
//        {
//            get
//            {
//                return this.dateAuthorizedField;
//            }
//            set
//            {
//                this.dateAuthorizedField = value;
//            }
//        }

        
//        public TimePeriodAny EffectivePeriod
//        {
//            get
//            {
//                return this.effectivePeriodField;
//            }
//            set
//            {
//                this.effectivePeriodField = value;
//            }
//        }

        
//        public string AuthorizationCode
//        {
//            get
//            {
//                return this.authorizationCodeField;
//            }
//            set
//            {
//                this.authorizationCodeField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("GLElement", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class NameValue : NameValueBase
//    {

//        private UserArea userAreaField;

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class ProjectResourceCategory
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class ProjectActivity
//    {

//        private string idField;

//        private string statusField;

//        private Amounts costField;

//        private Description[] descriptionField;

//        private Note[] noteField;

//        private UserArea userAreaField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public string Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public Amounts Cost
//        {
//            get
//            {
//                return this.costField;
//            }
//            set
//            {
//                this.costField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GenericDocumentReference))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderDocumentReference))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class DocumentReference
//    {
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyBase))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyInstitutional))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Project))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Location))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Document))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Order))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrder))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Noun
//    {
//    }
    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyInstitutional))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public abstract partial class PartyBase : Noun
//    {
//        private PartyIdType partyIdField;
//        private PartyAssignedPartyId[] alternatePartyIdsField;
//        private bool activeField;
//        private bool oneTimeField;

//        public PartyBase()
//        {
//            this.activeField = false;
//            this.oneTimeField = false;
//        }        
//        public PartyIdType PartyId
//        {
//            get
//            {
//                return this.partyIdField;
//            }
//            set
//            {
//                this.partyIdField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public PartyAssignedPartyId[] AlternatePartyIds
//        {
//            get
//            {
//                return this.alternatePartyIdsField;
//            }
//            set
//            {
//                this.alternatePartyIdsField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(false)]
//        public bool active
//        {
//            get
//            {
//                return this.activeField;
//            }
//            set
//            {
//                this.activeField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(false)]
//        public bool oneTime
//        {
//            get
//            {
//                return this.oneTimeField;
//            }
//            set
//            {
//                this.oneTimeField = value;
//            }
//        }
//    }
    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Order))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrder))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Document : Noun
//    {
//        private PurchaseOrderHeader headerField;
//        private PurchaseOrderLine[] lineField;
        
//        public PurchaseOrderHeader Header
//        {
//            get
//            {
//                return this.headerField;
//            }
//            set
//            {
//                this.headerField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlElementAttribute("Line")]
//        public PurchaseOrderLine[] Line
//        {
//            get
//            {
//                return this.lineField;
//            }
//            set
//            {
//                this.lineField = value;
//            }
//        }
//    }
    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("Header", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PurchaseOrderHeader : OrderHeader
//    {
//        private UserArea userAreaField;
        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }
    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderHeader))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public abstract partial class OrderHeader : DocumentOrderHeader
//    {
//        private OrderStatus orderStatusField;
//        private string specialPriceAuthorizationField;
//        private bool taxWithholdingExemptField;
//        private bool taxWithholdingExemptFieldSpecified;
//        private License[] licenseField;
//        private string freightClassField;
//        private Note[] shipNoteField;
//        private bool dropShipIndField;
//        private bool dropShipIndFieldSpecified;
//        private bool backOrderedIndField;
//        private bool backOrderedIndFieldSpecified;
//        private bool shipPriorToDueDateIndField;
//        private bool shipPriorToDueDateIndFieldSpecified;
//        private string priorityField;
//        private string reasonCodeField;
//        private string earliestShipDateField;
//        private string needDeliveryDateField;
//        private string promisedDeliveryDateField;
//        private string promisedShipDateField;
//        private Amount[] extendedPriceField;
//        private Amount[] totalAmountField;
//        private TransportationTerm transportationTermField;
//        private PaymentTerms[] paymentTermsField;
//        private Charges chargesField;
//        private Distribution[] distributionField;
//        private PartyBase[] partiesField;
        
//        public OrderStatus OrderStatus
//        {
//            get
//            {
//                return this.orderStatusField;
//            }
//            set
//            {
//                this.orderStatusField = value;
//            }
//        }        
//        public string SpecialPriceAuthorization
//        {
//            get
//            {
//                return this.specialPriceAuthorizationField;
//            }
//            set
//            {
//                this.specialPriceAuthorizationField = value;
//            }
//        }        
//        public bool TaxWithholdingExempt
//        {
//            get
//            {
//                return this.taxWithholdingExemptField;
//            }
//            set
//            {
//                this.taxWithholdingExemptField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool TaxWithholdingExemptSpecified
//        {
//            get
//            {
//                return this.taxWithholdingExemptFieldSpecified;
//            }
//            set
//            {
//                this.taxWithholdingExemptFieldSpecified = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlElementAttribute("License")]
//        public License[] License
//        {
//            get
//            {
//                return this.licenseField;
//            }
//            set
//            {
//                this.licenseField = value;
//            }
//        }        
//        public string FreightClass
//        {
//            get
//            {
//                return this.freightClassField;
//            }
//            set
//            {
//                this.freightClassField = value;
//            }
//        }        
//        [System.Xml.Serialization.XmlElementAttribute("ShipNote")]
//        public Note[] ShipNote
//        {
//            get
//            {
//                return this.shipNoteField;
//            }
//            set
//            {
//                this.shipNoteField = value;
//            }
//        }        
//        public bool DropShipInd
//        {
//            get
//            {
//                return this.dropShipIndField;
//            }
//            set
//            {
//                this.dropShipIndField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool DropShipIndSpecified
//        {
//            get
//            {
//                return this.dropShipIndFieldSpecified;
//            }
//            set
//            {
//                this.dropShipIndFieldSpecified = value;
//            }
//        }        
//        public bool BackOrderedInd
//        {
//            get
//            {
//                return this.backOrderedIndField;
//            }
//            set
//            {
//                this.backOrderedIndField = value;
//            }
//        }  
        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool BackOrderedIndSpecified
//        {
//            get
//            {
//                return this.backOrderedIndFieldSpecified;
//            }
//            set
//            {
//                this.backOrderedIndFieldSpecified = value;
//            }
//        }       
//        public bool ShipPriorToDueDateInd
//        {
//            get
//            {
//                return this.shipPriorToDueDateIndField;
//            }
//            set
//            {
//                this.shipPriorToDueDateIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool ShipPriorToDueDateIndSpecified
//        {
//            get
//            {
//                return this.shipPriorToDueDateIndFieldSpecified;
//            }
//            set
//            {
//                this.shipPriorToDueDateIndFieldSpecified = value;
//            }
//        }        
//        public string Priority
//        {
//            get
//            {
//                return this.priorityField;
//            }
//            set
//            {
//                this.priorityField = value;
//            }
//        }        
//        public string ReasonCode
//        {
//            get
//            {
//                return this.reasonCodeField;
//            }
//            set
//            {
//                this.reasonCodeField = value;
//            }
//        }        
//        public string EarliestShipDate
//        {
//            get
//            {
//                return this.earliestShipDateField;
//            }
//            set
//            {
//                this.earliestShipDateField = value;
//            }
//        }        
//        public string NeedDeliveryDate
//        {
//            get
//            {
//                return this.needDeliveryDateField;
//            }
//            set
//            {
//                this.needDeliveryDateField = value;
//            }
//        }        
//        public string PromisedDeliveryDate
//        {
//            get
//            {
//                return this.promisedDeliveryDateField;
//            }
//            set
//            {
//                this.promisedDeliveryDateField = value;
//            }
//        }

        
//        public string PromisedShipDate
//        {
//            get
//            {
//                return this.promisedShipDateField;
//            }
//            set
//            {
//                this.promisedShipDateField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ExtendedPrice")]
//        public Amount[] ExtendedPrice
//        {
//            get
//            {
//                return this.extendedPriceField;
//            }
//            set
//            {
//                this.extendedPriceField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("TotalAmount")]
//        public Amount[] TotalAmount
//        {
//            get
//            {
//                return this.totalAmountField;
//            }
//            set
//            {
//                this.totalAmountField = value;
//            }
//        }

        
//        public TransportationTerm TransportationTerm
//        {
//            get
//            {
//                return this.transportationTermField;
//            }
//            set
//            {
//                this.transportationTermField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("PaymentTerms")]
//        public PaymentTerms[] PaymentTerms
//        {
//            get
//            {
//                return this.paymentTermsField;
//            }
//            set
//            {
//                this.paymentTermsField = value;
//            }
//        }

        
//        public Charges Charges
//        {
//            get
//            {
//                return this.chargesField;
//            }
//            set
//            {
//                this.chargesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Distribution")]
//        public Distribution[] Distribution
//        {
//            get
//            {
//                return this.distributionField;
//            }
//            set
//            {
//                this.distributionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute("PartyType", IsNullable = false)]
//        public PartyBase[] Parties
//        {
//            get
//            {
//                return this.partiesField;
//            }
//            set
//            {
//                this.partiesField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class OrderStatus
//    {

//        private OrderStatusCode codeField;

//        private Description[] descriptionField;

//        private Status[] acknowledgementDetailField;

//        private UserArea userAreaField;

//        private System.DateTime entryDateTimeField;

//        private bool entryDateTimeFieldSpecified;

        
//        public OrderStatusCode Code
//        {
//            get
//            {
//                return this.codeField;
//            }
//            set
//            {
//                this.codeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("AcknowledgementDetail")]
//        public Status[] AcknowledgementDetail
//        {
//            get
//            {
//                return this.acknowledgementDetailField;
//            }
//            set
//            {
//                this.acknowledgementDetailField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public System.DateTime entryDateTime
//        {
//            get
//            {
//                return this.entryDateTimeField;
//            }
//            set
//            {
//                this.entryDateTimeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool entryDateTimeSpecified
//        {
//            get
//            {
//                return this.entryDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.entryDateTimeFieldSpecified = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
//    public enum OrderStatusCode
//    {

        
//        Open,

        
//        Closed,

        
//        Cancelled,

        
//        Blocked,

        
//        Hold,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class TransportationTerm
//    {

//        private TransportationTermTermCode termCodeField;

//        private Location placeOfOwnershipTransferField;

//        private string freightTermsField;

//        private UserArea userAreaField;

        
//        public TransportationTermTermCode TermCode
//        {
//            get
//            {
//                return this.termCodeField;
//            }
//            set
//            {
//                this.termCodeField = value;
//            }
//        }

        
//        public Location PlaceOfOwnershipTransfer
//        {
//            get
//            {
//                return this.placeOfOwnershipTransferField;
//            }
//            set
//            {
//                this.placeOfOwnershipTransferField = value;
//            }
//        }

        
//        public string FreightTerms
//        {
//            get
//            {
//                return this.freightTermsField;
//            }
//            set
//            {
//                this.freightTermsField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
//    public partial class TransportationTermTermCode : Id
//    {

//        private string issuingAgencyField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string issuingAgency
//        {
//            get
//            {
//                return this.issuingAgencyField;
//            }
//            set
//            {
//                this.issuingAgencyField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Location : Noun
//    {

//        private Name[] nameField;

//        private string idField;

//        private Description[] descriptionField;

//        private PostalAddress postalAddressField;

//        private GPSCoordinates gPSCoordinatesField;

//        private ProximalLocation[] locationRelationshipField;

//        private Note[] noteField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlElementAttribute("Name")]
//        public Name[] Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public PostalAddress PostalAddress
//        {
//            get
//            {
//                return this.postalAddressField;
//            }
//            set
//            {
//                this.postalAddressField = value;
//            }
//        }

        
//        public GPSCoordinates GPSCoordinates
//        {
//            get
//            {
//                return this.gPSCoordinatesField;
//            }
//            set
//            {
//                this.gPSCoordinatesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("LocationRelationship")]
//        public ProximalLocation[] LocationRelationship
//        {
//            get
//            {
//                return this.locationRelationshipField;
//            }
//            set
//            {
//                this.locationRelationshipField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PostalAddress : PostalAddressBase
//    {

//        private UserArea userAreaField;

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Address))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PostalAddress))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class PostalAddressBase
//    {

//        private AddressId[] addressIdField;

//        private string[] addressLineField;

//        private string cityField;

//        private string countyField;

//        private string stateOrProvinceField;

//        private string countryField;

//        private string regionField;

//        private string postalCodeField;

//        private Description[] descriptionField;

        
//        [System.Xml.Serialization.XmlElementAttribute("AddressId")]
//        public AddressId[] AddressId
//        {
//            get
//            {
//                return this.addressIdField;
//            }
//            set
//            {
//                this.addressIdField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
//        public string[] AddressLine
//        {
//            get
//            {
//                return this.addressLineField;
//            }
//            set
//            {
//                this.addressLineField = value;
//            }
//        }

        
//        public string City
//        {
//            get
//            {
//                return this.cityField;
//            }
//            set
//            {
//                this.cityField = value;
//            }
//        }

        
//        public string County
//        {
//            get
//            {
//                return this.countyField;
//            }
//            set
//            {
//                this.countyField = value;
//            }
//        }

        
//        public string StateOrProvince
//        {
//            get
//            {
//                return this.stateOrProvinceField;
//            }
//            set
//            {
//                this.stateOrProvinceField = value;
//            }
//        }

        
//        public string Country
//        {
//            get
//            {
//                return this.countryField;
//            }
//            set
//            {
//                this.countryField = value;
//            }
//        }

        
//        public string Region
//        {
//            get
//            {
//                return this.regionField;
//            }
//            set
//            {
//                this.regionField = value;
//            }
//        }

        
//        public string PostalCode
//        {
//            get
//            {
//                return this.postalCodeField;
//            }
//            set
//            {
//                this.postalCodeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class AddressId
//    {

//        private string qualifyingAgencyField;

//        private string valueField;

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string qualifyingAgency
//        {
//            get
//            {
//                return this.qualifyingAgencyField;
//            }
//            set
//            {
//                this.qualifyingAgencyField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class GPSCoordinates
//    {

//        private object gPSSystemTypeField;

//        private object latitudeField;

//        private object longitudeField;

//        private UserArea userAreaField;

        
//        public object GPSSystemType
//        {
//            get
//            {
//                return this.gPSSystemTypeField;
//            }
//            set
//            {
//                this.gPSSystemTypeField = value;
//            }
//        }

        
//        public object Latitude
//        {
//            get
//            {
//                return this.latitudeField;
//            }
//            set
//            {
//                this.latitudeField = value;
//            }
//        }

        
//        public object Longitude
//        {
//            get
//            {
//                return this.longitudeField;
//            }
//            set
//            {
//                this.longitudeField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class ProximalLocation
//    {

//        private string binderField;

//        private Location locationField;

//        private UserArea userAreaField;

        
//        public string Binder
//        {
//            get
//            {
//                return this.binderField;
//            }
//            set
//            {
//                this.binderField = value;
//            }
//        }

        
//        public Location Location
//        {
//            get
//            {
//                return this.locationField;
//            }
//            set
//            {
//                this.locationField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PaymentTerms
//    {

//        private Id termIdField;

//        private Description[] descriptionField;

//        private string lineNumberField;

//        private string[] itemsField;

//        private ItemsChoiceType2[] itemsElementNameField;

//        private object itemField;

//        private UserArea userAreaField;

        
//        public Id TermId
//        {
//            get
//            {
//                return this.termIdField;
//            }
//            set
//            {
//                this.termIdField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public string LineNumber
//        {
//            get
//            {
//                return this.lineNumberField;
//            }
//            set
//            {
//                this.lineNumberField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("DayOfMonth", typeof(string), DataType = "positiveInteger")]
//        [System.Xml.Serialization.XmlElementAttribute("DueDate", typeof(string))]
//        [System.Xml.Serialization.XmlElementAttribute("NumberOfDays", typeof(string), DataType = "positiveInteger")]
//        [System.Xml.Serialization.XmlElementAttribute("PaymentTermsDate", typeof(string))]
//        [System.Xml.Serialization.XmlElementAttribute("ProximoNumberMonth", typeof(string), DataType = "positiveInteger")]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public string[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemsChoiceType2[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("DiscountAmount", typeof(Amount))]
//        [System.Xml.Serialization.XmlElementAttribute("DiscountPercent", typeof(Quantity))]
//        public object Item
//        {
//            get
//            {
//                return this.itemField;
//            }
//            set
//            {
//                this.itemField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemsChoiceType2
//    {

        
//        DayOfMonth,

        
//        DueDate,

        
//        NumberOfDays,

        
//        PaymentTermsDate,

        
//        ProximoNumberMonth,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Charges
//    {

//        private Charge[] itemsField;

//        private ItemsChoiceType3[] itemsElementNameField;

//        private Charge totalChargeField;

        
//        [System.Xml.Serialization.XmlElementAttribute("AdditionalCharge", typeof(Charge))]
//        [System.Xml.Serialization.XmlElementAttribute("Charge", typeof(Charge))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public Charge[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemsChoiceType3[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }

        
//        public Charge TotalCharge
//        {
//            get
//            {
//                return this.totalChargeField;
//            }
//            set
//            {
//                this.totalChargeField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Charge
//    {

//        private string idField;

//        private Amount totalField;

//        private AmountPerQuantity costField;

//        private Description[] descriptionField;

//        private Distribution[] distributionField;

//        private UserArea userAreaField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public Amount Total
//        {
//            get
//            {
//                return this.totalField;
//            }
//            set
//            {
//                this.totalField = value;
//            }
//        }

        
//        public AmountPerQuantity Cost
//        {
//            get
//            {
//                return this.costField;
//            }
//            set
//            {
//                this.costField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Distribution")]
//        public Distribution[] Distribution
//        {
//            get
//            {
//                return this.distributionField;
//            }
//            set
//            {
//                this.distributionField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemsChoiceType3
//    {

        
//        AdditionalCharge,

        
//        Charge,
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderHeader))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderHeader))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class DocumentOrderHeader : DocumentHeader
//    {
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DocumentOrderHeader))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OrderHeader))]
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderHeader))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public abstract partial class DocumentHeader
//    {

//        private DocumentIdType[] documentIdsField;

//        private Status statusField;

//        private System.DateTime lastModificationDateTimeField;

//        private bool lastModificationDateTimeFieldSpecified;

//        private System.DateTime documentDateTimeField;

//        private bool documentDateTimeFieldSpecified;

//        private Description[] descriptionField;

//        private Note[] noteField;

//        private DocumentReference[] documentReferencesField;

//        private Attachment[] attachmentsField;

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentIdType[] DocumentIds
//        {
//            get
//            {
//                return this.documentIdsField;
//            }
//            set
//            {
//                this.documentIdsField = value;
//            }
//        }

        
//        public Status Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public System.DateTime LastModificationDateTime
//        {
//            get
//            {
//                return this.lastModificationDateTimeField;
//            }
//            set
//            {
//                this.lastModificationDateTimeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool LastModificationDateTimeSpecified
//        {
//            get
//            {
//                return this.lastModificationDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.lastModificationDateTimeFieldSpecified = value;
//            }
//        }

        
//        public System.DateTime DocumentDateTime
//        {
//            get
//            {
//                return this.documentDateTimeField;
//            }
//            set
//            {
//                this.documentDateTimeField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool DocumentDateTimeSpecified
//        {
//            get
//            {
//                return this.documentDateTimeFieldSpecified;
//            }
//            set
//            {
//                this.documentDateTimeFieldSpecified = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentReference[] DocumentReferences
//        {
//            get
//            {
//                return this.documentReferencesField;
//            }
//            set
//            {
//                this.documentReferencesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Attachment[] Attachments
//        {
//            get
//            {
//                return this.attachmentsField;
//            }
//            set
//            {
//                this.attachmentsField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartyDocumentId))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("DocumentId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class DocumentIdType
//    {

//        private string idField;

//        private Revision revisionField;

        
//        public string Id
//        {
//            get
//            {
//                return this.idField;
//            }
//            set
//            {
//                this.idField = value;
//            }
//        }

        
//        public Revision Revision
//        {
//            get
//            {
//                return this.revisionField;
//            }
//            set
//            {
//                this.revisionField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("Line", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PurchaseOrderLine : OrderLine
//    {

//        private PurchaseOrderSubLine[] subLineField;

//        private PurchaseOrderSchedule[] scheduleField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlElementAttribute("SubLine")]
//        public PurchaseOrderSubLine[] SubLine
//        {
//            get
//            {
//                return this.subLineField;
//            }
//            set
//            {
//                this.subLineField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Schedule")]
//        public PurchaseOrderSchedule[] Schedule
//        {
//            get
//            {
//                return this.scheduleField;
//            }
//            set
//            {
//                this.scheduleField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("SubLine", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PurchaseOrderSubLine : OrderLineBase
//    {
//        private UserArea userAreaField;
        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("Schedule", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PurchaseOrderSchedule : OrderSchedule
//    {
//        private UserArea userAreaField;

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderSchedule))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class OrderSchedule : OrderLineBase
//    {

//        private TimePeriodAny effectivePeriodField;

//        private Quantity requiredQuantityField;

//        private string fixedTimeDurationField;

//        private string overToleranceDurationField;

//        private string underToleranceDurationField;

//        private Quantity overShipToleranceField;

//        private Quantity underShipToleranceField;

        
//        public TimePeriodAny EffectivePeriod
//        {
//            get
//            {
//                return this.effectivePeriodField;
//            }
//            set
//            {
//                this.effectivePeriodField = value;
//            }
//        }

        
//        public Quantity RequiredQuantity
//        {
//            get
//            {
//                return this.requiredQuantityField;
//            }
//            set
//            {
//                this.requiredQuantityField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "duration")]
//        public string FixedTimeDuration
//        {
//            get
//            {
//                return this.fixedTimeDurationField;
//            }
//            set
//            {
//                this.fixedTimeDurationField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "duration")]
//        public string OverToleranceDuration
//        {
//            get
//            {
//                return this.overToleranceDurationField;
//            }
//            set
//            {
//                this.overToleranceDurationField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "duration")]
//        public string UnderToleranceDuration
//        {
//            get
//            {
//                return this.underToleranceDurationField;
//            }
//            set
//            {
//                this.underToleranceDurationField = value;
//            }
//        }

        
//        public Quantity OverShipTolerance
//        {
//            get
//            {
//                return this.overShipToleranceField;
//            }
//            set
//            {
//                this.overShipToleranceField = value;
//            }
//        }

        
//        public Quantity UnderShipTolerance
//        {
//            get
//            {
//                return this.underShipToleranceField;
//            }
//            set
//            {
//                this.underShipToleranceField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrderLine))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class OrderLine : OrderLineBase
//    {

//        private OrderStatus orderStatusField;

//        private PartyBase[] partiesField;

//        private Charges chargesField;

//        private Tax[] taxField;

//        private PaymentTerms[] paymentTermsField;

//        private DocumentReference[] documentReferencesField;

//        private Attachment[] attachmentsField;

        
//        public OrderStatus OrderStatus
//        {
//            get
//            {
//                return this.orderStatusField;
//            }
//            set
//            {
//                this.orderStatusField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute("PartyType", IsNullable = false)]
//        public PartyBase[] Parties
//        {
//            get
//            {
//                return this.partiesField;
//            }
//            set
//            {
//                this.partiesField = value;
//            }
//        }

        
//        public Charges Charges
//        {
//            get
//            {
//                return this.chargesField;
//            }
//            set
//            {
//                this.chargesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Tax")]
//        public Tax[] Tax
//        {
//            get
//            {
//                return this.taxField;
//            }
//            set
//            {
//                this.taxField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("PaymentTerms")]
//        public PaymentTerms[] PaymentTerms
//        {
//            get
//            {
//                return this.paymentTermsField;
//            }
//            set
//            {
//                this.paymentTermsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentReference[] DocumentReferences
//        {
//            get
//            {
//                return this.documentReferencesField;
//            }
//            set
//            {
//                this.documentReferencesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Attachment[] Attachments
//        {
//            get
//            {
//                return this.attachmentsField;
//            }
//            set
//            {
//                this.attachmentsField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Tax
//    {

//        private Amount taxAmountField;

//        private Amount taxBaseAmountField;

//        private Quantity percentQuantityField;

//        private Description[] descriptionField;

//        private TaxCode taxCodeField;

//        private TaxJurisdiction taxJurisdictionField;

//        private Charges chargesField;

//        private Tax[] tax1Field;

//        private UserArea userAreaField;

        
//        public Amount TaxAmount
//        {
//            get
//            {
//                return this.taxAmountField;
//            }
//            set
//            {
//                this.taxAmountField = value;
//            }
//        }

        
//        public Amount TaxBaseAmount
//        {
//            get
//            {
//                return this.taxBaseAmountField;
//            }
//            set
//            {
//                this.taxBaseAmountField = value;
//            }
//        }

        
//        public Quantity PercentQuantity
//        {
//            get
//            {
//                return this.percentQuantityField;
//            }
//            set
//            {
//                this.percentQuantityField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }


        
//        public TaxCode TaxCode
//        {
//            get
//            {
//                return this.taxCodeField;
//            }
//            set
//            {
//                this.taxCodeField = value;
//            }
//        }

        
//        public TaxJurisdiction TaxJurisdiction
//        {
//            get
//            {
//                return this.taxJurisdictionField;
//            }
//            set
//            {
//                this.taxJurisdictionField = value;
//            }
//        }

        
//        public Charges Charges
//        {
//            get
//            {
//                return this.chargesField;
//            }
//            set
//            {
//                this.chargesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Tax")]
//        public Tax[] Tax1
//        {
//            get
//            {
//                return this.tax1Field;
//            }
//            set
//            {
//                this.tax1Field = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class TaxJurisdiction
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PurchaseOrder))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Order : Document
//    {
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class PersonName
//    {

//        private Name[] salutationField;

//        private Name[] givenNameField;

//        private Name[] preferredGivenNameField;

//        private Name[] middleNameField;

//        private Name[] familyNameField;

//        private Name[] suffixField;

//        private Name[] formattedNameField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlElementAttribute("Salutation")]
//        public Name[] Salutation
//        {
//            get
//            {
//                return this.salutationField;
//            }
//            set
//            {
//                this.salutationField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("GivenName")]
//        public Name[] GivenName
//        {
//            get
//            {
//                return this.givenNameField;
//            }
//            set
//            {
//                this.givenNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("PreferredGivenName")]
//        public Name[] PreferredGivenName
//        {
//            get
//            {
//                return this.preferredGivenNameField;
//            }
//            set
//            {
//                this.preferredGivenNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("MiddleName")]
//        public Name[] MiddleName
//        {
//            get
//            {
//                return this.middleNameField;
//            }
//            set
//            {
//                this.middleNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("FamilyName")]
//        public Name[] FamilyName
//        {
//            get
//            {
//                return this.familyNameField;
//            }
//            set
//            {
//                this.familyNameField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Suffix")]
//        public Name[] Suffix
//        {
//            get
//            {
//                return this.suffixField;
//            }
//            set
//            {
//                this.suffixField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("FormattedName")]
//        public Name[] FormattedName
//        {
//            get
//            {
//                return this.formattedNameField;
//            }
//            set
//            {
//                this.formattedNameField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class PersonCode
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Usage
//    {

//        private string valueField;

        
//        [System.Xml.Serialization.XmlTextAttribute()]
//        public string Value
//        {
//            get
//            {
//                return this.valueField;
//            }
//            set
//            {
//                this.valueField = value;
//            }
//        }
//    }

    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ProcessPurchaseOrderDataArea))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class DataArea
//    {
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Signature
//    {

//        private System.Xml.XmlElement anyField;

//        private string qualifyingAgencyField;

        
//        [System.Xml.Serialization.XmlAnyElementAttribute()]
//        public System.Xml.XmlElement Any
//        {
//            get
//            {
//                return this.anyField;
//            }
//            set
//            {
//                this.anyField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string qualifyingAgency
//        {
//            get
//            {
//                return this.qualifyingAgencyField;
//            }
//            set
//            {
//                this.qualifyingAgencyField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class Sender
//    {

//        private string logicalIdField;

//        private string componentField;

//        private string taskField;

//        private string referenceIdField;

//        private Enums.Confirmation confirmationField;

//        private bool confirmationFieldSpecified;

//        private string authorizationIdField;

        
//        public string LogicalId
//        {
//            get
//            {
//                return this.logicalIdField;
//            }
//            set
//            {
//                this.logicalIdField = value;
//            }
//        }

        
//        public string Component
//        {
//            get
//            {
//                return this.componentField;
//            }
//            set
//            {
//                this.componentField = value;
//            }
//        }

        
//        public string Task
//        {
//            get
//            {
//                return this.taskField;
//            }
//            set
//            {
//                this.taskField = value;
//            }
//        }

        
//        public string ReferenceId
//        {
//            get
//            {
//                return this.referenceIdField;
//            }
//            set
//            {
//                this.referenceIdField = value;
//            }
//        }        
//        public Enums.Confirmation Confirmation
//        {
//            get
//            {
//                return this.confirmationField;
//            }
//            set
//            {
//                this.confirmationField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool ConfirmationSpecified
//        {
//            get
//            {
//                return this.confirmationFieldSpecified;
//            }
//            set
//            {
//                this.confirmationFieldSpecified = value;
//            }
//        }        
//        public string AuthorizationId
//        {
//            get
//            {
//                return this.authorizationIdField;
//            }
//            set
//            {
//                this.authorizationIdField = value;
//            }
//        }
//    }
    
//    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ProcessPurchaseOrderXML))]
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public partial class BusinessObjectDocument
//    {
//        private ApplicationArea applicationAreaField;
//        private string revisionField;
//        private Enums.BusinessObjectDocumentEnvironment environmentField;
//        private string langField;

//        public BusinessObjectDocument()
//        {
//            SSF_AutoParts.Globals glob = new SSF_AutoParts.Globals();
//            if (glob.GetIsTest())
//            {
//                this.environmentField = Enums.BusinessObjectDocumentEnvironment.Test;
//            }
//            else
//            {
//                this.environmentField = Enums.BusinessObjectDocumentEnvironment.Production;
//            }

//            this.langField = "en-US";
//        }

        
//        public ApplicationArea ApplicationArea
//        {
//            get
//            {
//                return this.applicationAreaField;
//            }
//            set
//            {
//                this.applicationAreaField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        public string revision
//        {
//            get
//            {
//                return this.revisionField;
//            }
//            set
//            {
//                this.revisionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute()]
//        [System.ComponentModel.DefaultValueAttribute(Enums.BusinessObjectDocumentEnvironment.Production)]
//        public Enums.BusinessObjectDocumentEnvironment environment
//        {
//            get
//            {
//                return this.environmentField;
//            }
//            set
//            {
//                this.environmentField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
//        [System.ComponentModel.DefaultValueAttribute("en-US")]
//        public string lang
//        {
//            get
//            {
//                return this.langField;
//            }
//            set
//            {
//                this.langField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class ApplicationArea
//    {

//        private Sender senderField;

//        private System.DateTime creationDateTimeField;

//        private Signature signatureField;

//        private string bODIdField;

//        private UserArea userAreaField;

        
//        public Sender Sender
//        {
//            get
//            {
//                return this.senderField;
//            }
//            set
//            {
//                this.senderField = value;
//            }
//        }

        
//        public System.DateTime CreationDateTime
//        {
//            get
//            {
//                return this.creationDateTimeField;
//            }
//            set
//            {
//                this.creationDateTimeField = value;
//            }
//        }

        
//        public Signature Signature
//        {
//            get
//            {
//                return this.signatureField;
//            }
//            set
//            {
//                this.signatureField = value;
//            }
//        }

        
//        public string BODId
//        {
//            get
//            {
//                return this.bODIdField;
//            }
//            set
//            {
//                this.bODIdField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }    
    

//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http:www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http:www.openapplications.org/oagis", IsNullable = false)]
//    public partial class RequestForQuote : Order
//    {
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PurchaseOrder : Order
//    {
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class DocumentReferences
//    {

//        private DocumentReference[] itemsField;

//        private ItemsChoiceType1[] itemsElementNameField;

        
//        [System.Xml.Serialization.XmlElementAttribute("CatalogDocumentReference", typeof(GenericDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("ContractDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("InvoiceDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("LedgerDocumentReference", typeof(GenericDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("MaintenanceOrderReference", typeof(GenericDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("ProjectReference", typeof(GenericDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("PurchaseOrderDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("QuoteDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("RFQDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("ReceiptDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("RequisitionDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("SalesOrderDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("UOMGroupReference", typeof(GenericDocumentReference))]
//        [System.Xml.Serialization.XmlElementAttribute("VoucherDocumentReference", typeof(OrderDocumentReference))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public DocumentReference[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemsChoiceType1[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("MaintenanceOrderReference", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class GenericDocumentReference : DocumentReference
//    {

//        private DocumentIdType[] documentIdsField;

//        private string documentDateField;

//        private Description[] descriptionField;

//        private Name[] nameField;

//        private Status statusField;

//        private Usage usageField;

//        private Note[] noteField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentIdType[] DocumentIds
//        {
//            get
//            {
//                return this.documentIdsField;
//            }
//            set
//            {
//                this.documentIdsField = value;
//            }
//        }

        
//        public string DocumentDate
//        {
//            get
//            {
//                return this.documentDateField;
//            }
//            set
//            {
//                this.documentDateField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Name")]
//        public Name[] Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        public Status Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public Usage Usage
//        {
//            get
//            {
//                return this.usageField;
//            }
//            set
//            {
//                this.usageField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("PurchaseOrderDocumentReference", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class OrderDocumentReference : DocumentReference
//    {
//        private DocumentIdType[] documentIdsField;
//        private string documentDateField;
//        private Description[] descriptionField;
//        private Name[] nameField;
//        private Status statusField;
//        private Usage usageField;
//        private Note[] noteField;
//        private string lineNumberField;
//        private string scheduleLineNumberField;
//        private string subLineNumberField;
//        private UserArea userAreaField;
      
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public DocumentIdType[] DocumentIds
//        {
//            get
//            {
//                return this.documentIdsField;
//            }
//            set
//            {
//                this.documentIdsField = value;
//            }
//        }
        
//        public string DocumentDate
//        {
//            get
//            {
//                return this.documentDateField;
//            }
//            set
//            {
//                this.documentDateField = value;
//            }
//        }
        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Name")]
//        public Name[] Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        public Status Status
//        {
//            get
//            {
//                return this.statusField;
//            }
//            set
//            {
//                this.statusField = value;
//            }
//        }

        
//        public Usage Usage
//        {
//            get
//            {
//                return this.usageField;
//            }
//            set
//            {
//                this.usageField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Note")]
//        public Note[] Note
//        {
//            get
//            {
//                return this.noteField;
//            }
//            set
//            {
//                this.noteField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
//        public string LineNumber
//        {
//            get
//            {
//                return this.lineNumberField;
//            }
//            set
//            {
//                this.lineNumberField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
//        public string ScheduleLineNumber
//        {
//            get
//            {
//                return this.scheduleLineNumberField;
//            }
//            set
//            {
//                this.scheduleLineNumberField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute(DataType = "positiveInteger")]
//        public string SubLineNumber
//        {
//            get
//            {
//                return this.subLineNumberField;
//            }
//            set
//            {
//                this.subLineNumberField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemsChoiceType1
//    {        
//        CatalogDocumentReference,
//        ContractDocumentReference,        
//        InvoiceDocumentReference,       
//        LedgerDocumentReference,        
//        MaintenanceOrderReference,        
//        ProjectReference,        
//        PurchaseOrderDocumentReference,        
//        QuoteDocumentReference,        
//        RFQDocumentReference,        
//        ReceiptDocumentReference,        
//        RequisitionDocumentReference,        
//        SalesOrderDocumentReference,        
//        UOMGroupReference,        
//        VoucherDocumentReference,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Attachments
//    {

//        private Attachment[] attachmentField;

        
//        [System.Xml.Serialization.XmlElementAttribute("Attachment")]
//        public Attachment[] Attachment
//        {
//            get
//            {
//                return this.attachmentField;
//            }
//            set
//            {
//                this.attachmentField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Parties
//    {

//        private PartyInstitutional[] itemsField;

//        private Enums.ItemsChoiceType6[] itemsElementNameField;

        
//        [System.Xml.Serialization.XmlElementAttribute("BillToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("BrokerParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("CarrierParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("CustomerParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("EmployeeParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ExporterParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("FreightBillToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("HoldAtParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ImporterParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("JointVentureParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ManufacturerParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("PayFromParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("PublisherParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("RemitToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ReturnToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ShipFromParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("ShipToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("SoldToParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlElementAttribute("SupplierParty", typeof(PartyInstitutional))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public PartyInstitutional[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public Enums.ItemsChoiceType6[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("SoldToParty", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyInstitutional : PartyBase
//    {

//        private Name[] nameField;

//        private OrganizationalUnit businessField;

//        private string currencyField;

//        private Description[] descriptionField;

//        private GLEntity gLEntityField;

//        private PaymentMethod paymentMethodField;

//        private bool paymentMethodFieldSpecified;

//        private Rating ratingField;

//        private bool ratingFieldSpecified;

//        private bool taxExemptIndField;

//        private bool taxExemptIndFieldSpecified;

//        private Id taxIdField;

//        private Id termIdField;

//        private Address[] addressesField;

//        private Contact[] contactsField;

//        private Attachment[] attachmentsField;

//        private string[] correspondenceLanguageField;

//        private NameValue[] qualificationField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlElementAttribute("Name")]
//        public Name[] Name
//        {
//            get
//            {
//                return this.nameField;
//            }
//            set
//            {
//                this.nameField = value;
//            }
//        }

        
//        public OrganizationalUnit Business
//        {
//            get
//            {
//                return this.businessField;
//            }
//            set
//            {
//                this.businessField = value;
//            }
//        }

        
//        public string Currency
//        {
//            get
//            {
//                return this.currencyField;
//            }
//            set
//            {
//                this.currencyField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        public GLEntity GLEntity
//        {
//            get
//            {
//                return this.gLEntityField;
//            }
//            set
//            {
//                this.gLEntityField = value;
//            }
//        }

        
//        public PaymentMethod PaymentMethod
//        {
//            get
//            {
//                return this.paymentMethodField;
//            }
//            set
//            {
//                this.paymentMethodField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool PaymentMethodSpecified
//        {
//            get
//            {
//                return this.paymentMethodFieldSpecified;
//            }
//            set
//            {
//                this.paymentMethodFieldSpecified = value;
//            }
//        }

        
//        public Rating Rating
//        {
//            get
//            {
//                return this.ratingField;
//            }
//            set
//            {
//                this.ratingField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool RatingSpecified
//        {
//            get
//            {
//                return this.ratingFieldSpecified;
//            }
//            set
//            {
//                this.ratingFieldSpecified = value;
//            }
//        }

        
//        public bool TaxExemptInd
//        {
//            get
//            {
//                return this.taxExemptIndField;
//            }
//            set
//            {
//                this.taxExemptIndField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public bool TaxExemptIndSpecified
//        {
//            get
//            {
//                return this.taxExemptIndFieldSpecified;
//            }
//            set
//            {
//                this.taxExemptIndFieldSpecified = value;
//            }
//        }

        
//        public Id TaxId
//        {
//            get
//            {
//                return this.taxIdField;
//            }
//            set
//            {
//                this.taxIdField = value;
//            }
//        }

        
//        public Id TermId
//        {
//            get
//            {
//                return this.termIdField;
//            }
//            set
//            {
//                this.termIdField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Address[] Addresses
//        {
//            get
//            {
//                return this.addressesField;
//            }
//            set
//            {
//                this.addressesField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute("ContactAbs", IsNullable = false)]
//        public Contact[] Contacts
//        {
//            get
//            {
//                return this.contactsField;
//            }
//            set
//            {
//                this.contactsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Attachment[] Attachments
//        {
//            get
//            {
//                return this.attachmentsField;
//            }
//            set
//            {
//                this.attachmentsField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("CorrespondenceLanguage", DataType = "language")]
//        public string[] CorrespondenceLanguage
//        {
//            get
//            {
//                return this.correspondenceLanguageField;
//            }
//            set
//            {
//                this.correspondenceLanguageField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Qualification")]
//        public NameValue[] Qualification
//        {
//            get
//            {
//                return this.qualificationField;
//            }
//            set
//            {
//                this.qualificationField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public enum PaymentMethod
//    {       
//        [System.Xml.Serialization.XmlEnumAttribute("Credit Card")]
//        CreditCard,
//        [System.Xml.Serialization.XmlEnumAttribute("Debit Card")]
//        DebitCard,
//        Cash,
//        Check,
//        Other,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    public enum Rating
//    {
//        Good,
//        Bad,
//        Ugly,
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Address : PostalAddressBase
//    {
//        private string[] telephoneField;

//        private string[] faxNumberField;

//        private string[] eMailAddressField;

//        private string[] uRIField;

//        private TaxJurisdiction taxJurisdictionField;

//        private UserArea userAreaField;

        
//        [System.Xml.Serialization.XmlElementAttribute("Telephone")]
//        public string[] Telephone
//        {
//            get
//            {
//                return this.telephoneField;
//            }
//            set
//            {
//                this.telephoneField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("FaxNumber")]
//        public string[] FaxNumber
//        {
//            get
//            {
//                return this.faxNumberField;
//            }
//            set
//            {
//                this.faxNumberField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("EMailAddress")]
//        public string[] EMailAddress
//        {
//            get
//            {
//                return this.eMailAddressField;
//            }
//            set
//            {
//                this.eMailAddressField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("URI", DataType = "anyURI")]
//        public string[] URI
//        {
//            get
//            {
//                return this.uRIField;
//            }
//            set
//            {
//                this.uRIField = value;
//            }
//        }

        
//        public TaxJurisdiction TaxJurisdiction
//        {
//            get
//            {
//                return this.taxJurisdictionField;
//            }
//            set
//            {
//                this.taxJurisdictionField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("ContactAbs", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Contact
//    {

//        private Person[] personField;
//        private string[] telephoneField;
//        private string[] eMailAddressField;
//        private string[] faxField;
//        private string[] uRIField;
//        private Description[] descriptionField;
//        private Address[] addressesField;
//        private UserArea userAreaField;
        
//        [System.Xml.Serialization.XmlElementAttribute("Person")]
//        public Person[] Person
//        {
//            get
//            {
//                return this.personField;
//            }
//            set
//            {
//                this.personField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Telephone")]
//        public string[] Telephone
//        {
//            get
//            {
//                return this.telephoneField;
//            }
//            set
//            {
//                this.telephoneField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("EMailAddress")]
//        public string[] EMailAddress
//        {
//            get
//            {
//                return this.eMailAddressField;
//            }
//            set
//            {
//                this.eMailAddressField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Fax")]
//        public string[] Fax
//        {
//            get
//            {
//                return this.faxField;
//            }
//            set
//            {
//                this.faxField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("URI", DataType = "anyURI")]
//        public string[] URI
//        {
//            get
//            {
//                return this.uRIField;
//            }
//            set
//            {
//                this.uRIField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlElementAttribute("Description")]
//        public Description[] Description
//        {
//            get
//            {
//                return this.descriptionField;
//            }
//            set
//            {
//                this.descriptionField = value;
//            }
//        }

        
//        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
//        public Address[] Addresses
//        {
//            get
//            {
//                return this.addressesField;
//            }
//            set
//            {
//                this.addressesField = value;
//            }
//        }

        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }

    
//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Person
//    {
//        private PersonCode personCodeField;
//        private PersonName personNameField;
//        private UserArea userAreaField;
        
//        public PersonCode PersonCode
//        {
//            get
//            {
//                return this.personCodeField;
//            }
//            set
//            {
//                this.personCodeField = value;
//            }
//        }
        
//        public PersonName PersonName
//        {
//            get
//            {
//                return this.personNameField;
//            }
//            set
//            {
//                this.personNameField = value;
//            }
//        }
        
//        public UserArea UserArea
//        {
//            get
//            {
//                return this.userAreaField;
//            }
//            set
//            {
//                this.userAreaField = value;
//            }
//        }
//    }       


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class DocumentIds
//    {
//        private DocumentIdType[] itemsField;
//        private ItemsChoiceType[] itemsElementNameField;

//        [System.Xml.Serialization.XmlElementAttribute("BrokerDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlElementAttribute("CarrierDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlElementAttribute("CustomerDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlElementAttribute("DocumentId", typeof(DocumentIdType))]
//        [System.Xml.Serialization.XmlElementAttribute("LogisticsProviderDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlElementAttribute("ShippersDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlElementAttribute("SupplierDocumentId", typeof(PartyDocumentId))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public DocumentIdType[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }


//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public ItemsChoiceType[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute("CustomerDocumentId", Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyDocumentId : DocumentIdType
//    {

//        private PartyIdType assigningPartyIdField;


//        public PartyIdType AssigningPartyId
//        {
//            get
//            {
//                return this.assigningPartyIdField;
//            }
//            set
//            {
//                this.assigningPartyIdField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis", IncludeInSchema = false)]
//    public enum ItemsChoiceType
//    {


//        BrokerDocumentId,


//        CarrierDocumentId,


//        CustomerDocumentId,


//        DocumentId,


//        LogisticsProviderDocumentId,


//        ShippersDocumentId,


//        SupplierDocumentId,
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class PartyReferences
//    {

//        private PartyReference[] partyReferenceField;


//        [System.Xml.Serialization.XmlElementAttribute("PartyReference")]
//        public PartyReference[] PartyReference
//        {
//            get
//            {
//                return this.partyReferenceField;
//            }
//            set
//            {
//                this.partyReferenceField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Addresses
//    {

//        private Address[] addressField;


//        [System.Xml.Serialization.XmlElementAttribute("Address")]
//        public Address[] Address
//        {
//            get
//            {
//                return this.addressField;
//            }
//            set
//            {
//                this.addressField = value;
//            }
//        }
//    }


//    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
//    [System.SerializableAttribute()]
//    [System.Diagnostics.DebuggerStepThroughAttribute()]
//    [System.ComponentModel.DesignerCategoryAttribute("code")]
//    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.openapplications.org/oagis")]
//    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
//    public partial class Contacts
//    {

//        private Contact[] itemsField;

//        private Enums.ItemsChoiceType5[] itemsElementNameField;


//        [System.Xml.Serialization.XmlElementAttribute("Contact", typeof(Contact))]
//        [System.Xml.Serialization.XmlElementAttribute("ContactAbs", typeof(Contact))]
//        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
//        public Contact[] Items
//        {
//            get
//            {
//                return this.itemsField;
//            }
//            set
//            {
//                this.itemsField = value;
//            }
//        }


//        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
//        [System.Xml.Serialization.XmlIgnoreAttribute()]
//        public Enums.ItemsChoiceType5[] ItemsElementName
//        {
//            get
//            {
//                return this.itemsElementNameField;
//            }
//            set
//            {
//                this.itemsElementNameField = value;
//            }
//        }
//    }   
//}