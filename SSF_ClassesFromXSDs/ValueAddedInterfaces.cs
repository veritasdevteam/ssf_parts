﻿
using System;
using System.ComponentModel;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Net.Http;
using System.Xml.Serialization;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Web;

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[WebServiceBindingAttribute(Name = "valueAddedSoap", Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public interface IValueAddedSoap
{
    [WebMethodAttribute()]
    [SoapDocumentMethodAttribute("https://www.ssfparts.com/ssfconnect-v1.5/valueAdded/GetFreightOptions", RequestNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", ResponseNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    getFreightOptions_Info[] GetFreightOptions(string as_ReferenceID, string as_whse_initials);


    [WebMethodAttribute()]
    [SoapDocumentMethodAttribute("https://www.ssfparts.com/ssfconnect-v1.5/valueAdded/GetPartImage", RequestNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", ResponseNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    Get_image_wsInfo GetPartImage(string anv_reference_id, int ai_searchId_sequence);


    [WebMethodAttribute()]
    [SoapDocumentMethodAttribute("https://www.ssfparts.com/ssfconnect-v1.5/valueAdded/check_partNumber", RequestNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", ResponseNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    return_info check_partNumber(string as_partNumber);


    [WebMethodAttribute()]
    [SoapDocumentMethodAttribute("https://www.ssfparts.com/ssfconnect-v1.5/valueAdded/CalculateFreight", RequestNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", ResponseNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    calculate_freight_otherFeesInfo[] CalculateFreight(string as_ReferenceID, string as_addr1, string as_addr2, string as_city, string as_state, string as_zip);


    [WebMethodAttribute()]
    [SoapDocumentMethodAttribute("https://www.ssfparts.com/ssfconnect-v1.5/valueAdded/cutoff_departure", RequestNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", ResponseNamespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
    route_cutoff_departure_WSInfo cutoff_departure(int ai_warehouse, string as_route, int ai_projectId);
}

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[SerializableAttribute()]
[DebuggerStepThroughAttribute()]
[DesignerCategoryAttribute("code")]
[XmlTypeAttribute(Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public partial class getFreightOptions_Info
{
    private string anv_reference_idField;
    private string ac_whse_initialsField;
    private string ac_send_recordsetField;
    private int ai_return_codeField;
    private string av_error_msjField;
    private int ai_order_idField;
    private string ac_svc1_codeField;
    private string av_svc1_descField;
    private decimal ade_svc1_chargeField;
    private decimal ade_svc1_grossField;
    private string ac_svc2_codeField;
    private string av_svc2_descField;
    private decimal ade_svc2_chargeField;
    private decimal ade_svc2_grossField;
    private string ac_svc3_codeField;
    private string av_svc3_descField;
    private decimal ade_svc3_chargeField;
    private decimal ade_svc3_grossField;
    private string ac_svc4_codeField;
    private string av_svc4_descField;
    private decimal ade_svc4_chargeField;
    private decimal ade_svc4_grossField;
    private string ac_svc5_codeField;
    private string av_svc5_descField;
    private decimal ade_svc5_chargeField;
    private decimal ade_svc5_grossField;
    private string ac_svc6_codeField;
    private string av_svc6_descField;
    private decimal ade_svc6_chargeField;
    private decimal ade_svc6_grossField;
    private string ac_svc7_codeField;
    private string av_svc7_descField;
    private decimal ade_svc7_chargeField;
    private decimal ade_svc7_grossField;
    private string ac_svc8_codeField;
    private string av_svc8_descField;
    private decimal ade_svc8_chargeField;
    private decimal ade_svc8_grossField;
    private string ac_svc9_codeField;
    private string av_svc9_descField;
    private decimal ade_svc9_chargeField;
    private decimal ade_svc9_grossField;
    private string ac_svc10_codeField;
    private string av_svc10_descField;
    private decimal ade_svc10_chargeField;
    private decimal ade_svc10_grossField;
    private string ac_svc11_codeField;
    private string av_svc11_descField;
    private decimal ade_svc11_chargeField;
    private decimal ade_svc11_grossField;
    private string ac_svc12_codeField;
    private string av_svc12_descField;
    private decimal ade_svc12_chargeField;
    private decimal ade_svc12_grossField;

    public string anv_reference_id
    {
        get
        {
            return this.anv_reference_idField;
        }
        set
        {
            this.anv_reference_idField = value;
        }
    }

    public string ac_whse_initials
    {
        get
        {
            return this.ac_whse_initialsField;
        }
        set
        {
            this.ac_whse_initialsField = value;
        }
    }

    public string ac_send_recordset
    {
        get
        {
            return this.ac_send_recordsetField;
        }
        set
        {
            this.ac_send_recordsetField = value;
        }
    }
    
    public int ai_return_code
    {
        get
        {
            return this.ai_return_codeField;
        }
        set
        {
            this.ai_return_codeField = value;
        }
    }
   
    public string av_error_msj
    {
        get
        {
            return this.av_error_msjField;
        }
        set
        {
            this.av_error_msjField = value;
        }
    }

    public int ai_order_id
    {
        get
        {
            return this.ai_order_idField;
        }
        set
        {
            this.ai_order_idField = value;
        }
    }

    public string ac_svc1_code
    {
        get
        {
            return this.ac_svc1_codeField;
        }
        set
        {
            this.ac_svc1_codeField = value;
        }
    }

    public string av_svc1_desc
    {
        get
        {
            return this.av_svc1_descField;
        }
        set
        {
            this.av_svc1_descField = value;
        }
    }

    public decimal ade_svc1_charge
    {
        get
        {
            return this.ade_svc1_chargeField;
        }
        set
        {
            this.ade_svc1_chargeField = value;
        }
    }

    public decimal ade_svc1_gross
    {
        get
        {
            return this.ade_svc1_grossField;
        }
        set
        {
            this.ade_svc1_grossField = value;
        }
    }

    public string ac_svc2_code
    {
        get
        {
            return this.ac_svc2_codeField;
        }
        set
        {
            this.ac_svc2_codeField = value;
        }
    }

    public string av_svc2_desc
    {
        get
        {
            return this.av_svc2_descField;
        }
        set
        {
            this.av_svc2_descField = value;
        }
    }

    public decimal ade_svc2_charge
    {
        get
        {
            return this.ade_svc2_chargeField;
        }
        set
        {
            this.ade_svc2_chargeField = value;
        }
    }

    public decimal ade_svc2_gross
    {
        get
        {
            return this.ade_svc2_grossField;
        }
        set
        {
            this.ade_svc2_grossField = value;
        }
    }

    public string ac_svc3_code
    {
        get
        {
            return this.ac_svc3_codeField;
        }
        set
        {
            this.ac_svc3_codeField = value;
        }
    }

    public string av_svc3_desc
    {
        get
        {
            return this.av_svc3_descField;
        }
        set
        {
            this.av_svc3_descField = value;
        }
    }

    public decimal ade_svc3_charge
    {
        get
        {
            return this.ade_svc3_chargeField;
        }
        set
        {
            this.ade_svc3_chargeField = value;
        }
    }


    public decimal ade_svc3_gross
    {
        get
        {
            return this.ade_svc3_grossField;
        }
        set
        {
            this.ade_svc3_grossField = value;
        }
    }


    public string ac_svc4_code
    {
        get
        {
            return this.ac_svc4_codeField;
        }
        set
        {
            this.ac_svc4_codeField = value;
        }
    }


    public string av_svc4_desc
    {
        get
        {
            return this.av_svc4_descField;
        }
        set
        {
            this.av_svc4_descField = value;
        }
    }


    public decimal ade_svc4_charge
    {
        get
        {
            return this.ade_svc4_chargeField;
        }
        set
        {
            this.ade_svc4_chargeField = value;
        }
    }


    public decimal ade_svc4_gross
    {
        get
        {
            return this.ade_svc4_grossField;
        }
        set
        {
            this.ade_svc4_grossField = value;
        }
    }


    public string ac_svc5_code
    {
        get
        {
            return this.ac_svc5_codeField;
        }
        set
        {
            this.ac_svc5_codeField = value;
        }
    }


    public string av_svc5_desc
    {
        get
        {
            return this.av_svc5_descField;
        }
        set
        {
            this.av_svc5_descField = value;
        }
    }


    public decimal ade_svc5_charge
    {
        get
        {
            return this.ade_svc5_chargeField;
        }
        set
        {
            this.ade_svc5_chargeField = value;
        }
    }


    public decimal ade_svc5_gross
    {
        get
        {
            return this.ade_svc5_grossField;
        }
        set
        {
            this.ade_svc5_grossField = value;
        }
    }


    public string ac_svc6_code
    {
        get
        {
            return this.ac_svc6_codeField;
        }
        set
        {
            this.ac_svc6_codeField = value;
        }
    }


    public string av_svc6_desc
    {
        get
        {
            return this.av_svc6_descField;
        }
        set
        {
            this.av_svc6_descField = value;
        }
    }


    public decimal ade_svc6_charge
    {
        get
        {
            return this.ade_svc6_chargeField;
        }
        set
        {
            this.ade_svc6_chargeField = value;
        }
    }


    public decimal ade_svc6_gross
    {
        get
        {
            return this.ade_svc6_grossField;
        }
        set
        {
            this.ade_svc6_grossField = value;
        }
    }


    public string ac_svc7_code
    {
        get
        {
            return this.ac_svc7_codeField;
        }
        set
        {
            this.ac_svc7_codeField = value;
        }
    }


    public string av_svc7_desc
    {
        get
        {
            return this.av_svc7_descField;
        }
        set
        {
            this.av_svc7_descField = value;
        }
    }


    public decimal ade_svc7_charge
    {
        get
        {
            return this.ade_svc7_chargeField;
        }
        set
        {
            this.ade_svc7_chargeField = value;
        }
    }


    public decimal ade_svc7_gross
    {
        get
        {
            return this.ade_svc7_grossField;
        }
        set
        {
            this.ade_svc7_grossField = value;
        }
    }


    public string ac_svc8_code
    {
        get
        {
            return this.ac_svc8_codeField;
        }
        set
        {
            this.ac_svc8_codeField = value;
        }
    }


    public string av_svc8_desc
    {
        get
        {
            return this.av_svc8_descField;
        }
        set
        {
            this.av_svc8_descField = value;
        }
    }


    public decimal ade_svc8_charge
    {
        get
        {
            return this.ade_svc8_chargeField;
        }
        set
        {
            this.ade_svc8_chargeField = value;
        }
    }


    public decimal ade_svc8_gross
    {
        get
        {
            return this.ade_svc8_grossField;
        }
        set
        {
            this.ade_svc8_grossField = value;
        }
    }


    public string ac_svc9_code
    {
        get
        {
            return this.ac_svc9_codeField;
        }
        set
        {
            this.ac_svc9_codeField = value;
        }
    }


    public string av_svc9_desc
    {
        get
        {
            return this.av_svc9_descField;
        }
        set
        {
            this.av_svc9_descField = value;
        }
    }


    public decimal ade_svc9_charge
    {
        get
        {
            return this.ade_svc9_chargeField;
        }
        set
        {
            this.ade_svc9_chargeField = value;
        }
    }


    public decimal ade_svc9_gross
    {
        get
        {
            return this.ade_svc9_grossField;
        }
        set
        {
            this.ade_svc9_grossField = value;
        }
    }


    public string ac_svc10_code
    {
        get
        {
            return this.ac_svc10_codeField;
        }
        set
        {
            this.ac_svc10_codeField = value;
        }
    }


    public string av_svc10_desc
    {
        get
        {
            return this.av_svc10_descField;
        }
        set
        {
            this.av_svc10_descField = value;
        }
    }

    public decimal ade_svc10_charge
    {
        get
        {
            return this.ade_svc10_chargeField;
        }
        set
        {
            this.ade_svc10_chargeField = value;
        }
    }

    public decimal ade_svc10_gross
    {
        get
        {
            return this.ade_svc10_grossField;
        }
        set
        {
            this.ade_svc10_grossField = value;
        }
    }

    public string ac_svc11_code
    {
        get
        {
            return this.ac_svc11_codeField;
        }
        set
        {
            this.ac_svc11_codeField = value;
        }
    }
    public string av_svc11_desc
    {
        get
        {
            return this.av_svc11_descField;
        }
        set
        {
            this.av_svc11_descField = value;
        }
    }

    public decimal ade_svc11_charge
    {
        get
        {
            return this.ade_svc11_chargeField;
        }
        set
        {
            this.ade_svc11_chargeField = value;
        }
    }
    public decimal ade_svc11_gross
    {
        get
        {
            return this.ade_svc11_grossField;
        }
        set
        {
            this.ade_svc11_grossField = value;
        }
    }
    public string ac_svc12_code
    {
        get
        {
            return this.ac_svc12_codeField;
        }
        set
        {
            this.ac_svc12_codeField = value;
        }
    }


    public string av_svc12_desc
    {
        get
        {
            return this.av_svc12_descField;
        }
        set
        {
            this.av_svc12_descField = value;
        }
    }

    public decimal ade_svc12_charge
    {
        get
        {
            return this.ade_svc12_chargeField;
        }
        set
        {
            this.ade_svc12_chargeField = value;
        }
    }

    public decimal ade_svc12_gross
    {
        get
        {
            return this.ade_svc12_grossField;
        }
        set
        {
            this.ade_svc12_grossField = value;
        }
    }
}

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[SerializableAttribute()]
[DebuggerStepThroughAttribute()]
[DesignerCategoryAttribute("code")]
[XmlTypeAttribute(Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public partial class route_cutoff_departure_WSInfo
{
    private int ai_whseField;

    private string ac_routeField;

    private System.DateTime ad_date_checkField;

    private int ai_return_codeField;

    private System.DateTime ad_cutOffTimeField;

    private System.DateTime ad_nextDepartureField;

    private string av_cutoffDatedisplayField;

    private string av_deliveryDatedisplayField;


    public int ai_whse
    {
        get
        {
            return this.ai_whseField;
        }
        set
        {
            this.ai_whseField = value;
        }
    }


    public string ac_route
    {
        get
        {
            return this.ac_routeField;
        }
        set
        {
            this.ac_routeField = value;
        }
    }


    public System.DateTime ad_date_check
    {
        get
        {
            return this.ad_date_checkField;
        }
        set
        {
            this.ad_date_checkField = value;
        }
    }


    public int ai_return_code
    {
        get
        {
            return this.ai_return_codeField;
        }
        set
        {
            this.ai_return_codeField = value;
        }
    }


    public System.DateTime ad_cutOffTime
    {
        get
        {
            return this.ad_cutOffTimeField;
        }
        set
        {
            this.ad_cutOffTimeField = value;
        }
    }


    public System.DateTime ad_nextDeparture
    {
        get
        {
            return this.ad_nextDepartureField;
        }
        set
        {
            this.ad_nextDepartureField = value;
        }
    }


    public string av_cutoffDatedisplay
    {
        get
        {
            return this.av_cutoffDatedisplayField;
        }
        set
        {
            this.av_cutoffDatedisplayField = value;
        }
    }


    public string av_deliveryDatedisplay
    {
        get
        {
            return this.av_deliveryDatedisplayField;
        }
        set
        {
            this.av_deliveryDatedisplayField = value;
        }
    }
}

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[SerializableAttribute()]
[DebuggerStepThroughAttribute()]
[DesignerCategoryAttribute("code")]
[XmlTypeAttribute(Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public partial class calculate_freight_otherFeesInfo
{

    private string anv_reference_idField;

    private string ac_whse_initialsField;

    private int ai_order_idField;

    private int ai_whseField;

    private string as_addr1Field;

    private string as_addr2Field;

    private string as_cityField;

    private string ac_stateField;

    private string ac_zipCodeField;

    private string ac_order_drop_shipField;

    private int ai_accountnumField;

    private int ai_return_codeField;

    private string as_error_descriptionField;

    private int ai_return_code_detailField;

    private string ac_svc0_codeField;

    private string av_svc0_descField;

    private decimal ade_svc0_chargeField;

    private string ac_svc1_codeField;

    private string av_svc1_descField;

    private decimal ade_svc1_chargeField;

    private string ac_svc2_codeField;

    private string av_svc2_descField;

    private decimal ade_svc2_chargeField;

    private string ac_svc3_codeField;

    private string av_svc3_descField;

    private decimal ade_svc3_chargeField;

    private string ac_svc4_codeField;

    private string av_svc4_descField;

    private decimal ade_svc4_chargeField;

    private string ac_svc5_codeField;

    private string av_svc5_descField;

    private decimal ade_svc5_chargeField;

    private string ac_svc6_codeField;

    private string av_svc6_descField;

    private decimal ade_svc6_chargeField;

    private string ac_svc7_codeField;

    private string av_svc7_descField;

    private decimal ade_svc7_chargeField;

    private string ac_svc8_codeField;

    private string av_svc8_descField;

    private decimal ade_svc8_chargeField;

    private string ac_svc9_codeField;

    private string av_svc9_descField;

    private decimal ade_svc9_chargeField;

    private string ac_svc10_codeField;

    private string av_svc10_descField;

    private decimal ade_svc10_chargeField;

    private string ac_svc11_codeField;

    private string av_svc11_descField;

    private decimal ade_svc11_chargeField;

    private string ac_svc12_codeField;

    private string av_svc12_descField;

    private decimal ade_svc12_chargeField;


    public string anv_reference_id
    {
        get
        {
            return this.anv_reference_idField;
        }
        set
        {
            this.anv_reference_idField = value;
        }
    }


    public string ac_whse_initials
    {
        get
        {
            return this.ac_whse_initialsField;
        }
        set
        {
            this.ac_whse_initialsField = value;
        }
    }


    public int ai_order_id
    {
        get
        {
            return this.ai_order_idField;
        }
        set
        {
            this.ai_order_idField = value;
        }
    }


    public int ai_whse
    {
        get
        {
            return this.ai_whseField;
        }
        set
        {
            this.ai_whseField = value;
        }
    }


    public string as_addr1
    {
        get
        {
            return this.as_addr1Field;
        }
        set
        {
            this.as_addr1Field = value;
        }
    }


    public string as_addr2
    {
        get
        {
            return this.as_addr2Field;
        }
        set
        {
            this.as_addr2Field = value;
        }
    }


    public string as_city
    {
        get
        {
            return this.as_cityField;
        }
        set
        {
            this.as_cityField = value;
        }
    }


    public string ac_state
    {
        get
        {
            return this.ac_stateField;
        }
        set
        {
            this.ac_stateField = value;
        }
    }


    public string ac_zipCode
    {
        get
        {
            return this.ac_zipCodeField;
        }
        set
        {
            this.ac_zipCodeField = value;
        }
    }


    public string ac_order_drop_ship
    {
        get
        {
            return this.ac_order_drop_shipField;
        }
        set
        {
            this.ac_order_drop_shipField = value;
        }
    }


    public int ai_accountnum
    {
        get
        {
            return this.ai_accountnumField;
        }
        set
        {
            this.ai_accountnumField = value;
        }
    }


    public int ai_return_code
    {
        get
        {
            return this.ai_return_codeField;
        }
        set
        {
            this.ai_return_codeField = value;
        }
    }


    public string as_error_description
    {
        get
        {
            return this.as_error_descriptionField;
        }
        set
        {
            this.as_error_descriptionField = value;
        }
    }


    public int ai_return_code_detail
    {
        get
        {
            return this.ai_return_code_detailField;
        }
        set
        {
            this.ai_return_code_detailField = value;
        }
    }


    public string ac_svc0_code
    {
        get
        {
            return this.ac_svc0_codeField;
        }
        set
        {
            this.ac_svc0_codeField = value;
        }
    }


    public string av_svc0_desc
    {
        get
        {
            return this.av_svc0_descField;
        }
        set
        {
            this.av_svc0_descField = value;
        }
    }


    public decimal ade_svc0_charge
    {
        get
        {
            return this.ade_svc0_chargeField;
        }
        set
        {
            this.ade_svc0_chargeField = value;
        }
    }


    public string ac_svc1_code
    {
        get
        {
            return this.ac_svc1_codeField;
        }
        set
        {
            this.ac_svc1_codeField = value;
        }
    }


    public string av_svc1_desc
    {
        get
        {
            return this.av_svc1_descField;
        }
        set
        {
            this.av_svc1_descField = value;
        }
    }


    public decimal ade_svc1_charge
    {
        get
        {
            return this.ade_svc1_chargeField;
        }
        set
        {
            this.ade_svc1_chargeField = value;
        }
    }


    public string ac_svc2_code
    {
        get
        {
            return this.ac_svc2_codeField;
        }
        set
        {
            this.ac_svc2_codeField = value;
        }
    }


    public string av_svc2_desc
    {
        get
        {
            return this.av_svc2_descField;
        }
        set
        {
            this.av_svc2_descField = value;
        }
    }


    public decimal ade_svc2_charge
    {
        get
        {
            return this.ade_svc2_chargeField;
        }
        set
        {
            this.ade_svc2_chargeField = value;
        }
    }


    public string ac_svc3_code
    {
        get
        {
            return this.ac_svc3_codeField;
        }
        set
        {
            this.ac_svc3_codeField = value;
        }
    }


    public string av_svc3_desc
    {
        get
        {
            return this.av_svc3_descField;
        }
        set
        {
            this.av_svc3_descField = value;
        }
    }


    public decimal ade_svc3_charge
    {
        get
        {
            return this.ade_svc3_chargeField;
        }
        set
        {
            this.ade_svc3_chargeField = value;
        }
    }


    public string ac_svc4_code
    {
        get
        {
            return this.ac_svc4_codeField;
        }
        set
        {
            this.ac_svc4_codeField = value;
        }
    }


    public string av_svc4_desc
    {
        get
        {
            return this.av_svc4_descField;
        }
        set
        {
            this.av_svc4_descField = value;
        }
    }


    public decimal ade_svc4_charge
    {
        get
        {
            return this.ade_svc4_chargeField;
        }
        set
        {
            this.ade_svc4_chargeField = value;
        }
    }


    public string ac_svc5_code
    {
        get
        {
            return this.ac_svc5_codeField;
        }
        set
        {
            this.ac_svc5_codeField = value;
        }
    }


    public string av_svc5_desc
    {
        get
        {
            return this.av_svc5_descField;
        }
        set
        {
            this.av_svc5_descField = value;
        }
    }


    public decimal ade_svc5_charge
    {
        get
        {
            return this.ade_svc5_chargeField;
        }
        set
        {
            this.ade_svc5_chargeField = value;
        }
    }


    public string ac_svc6_code
    {
        get
        {
            return this.ac_svc6_codeField;
        }
        set
        {
            this.ac_svc6_codeField = value;
        }
    }


    public string av_svc6_desc
    {
        get
        {
            return this.av_svc6_descField;
        }
        set
        {
            this.av_svc6_descField = value;
        }
    }


    public decimal ade_svc6_charge
    {
        get
        {
            return this.ade_svc6_chargeField;
        }
        set
        {
            this.ade_svc6_chargeField = value;
        }
    }


    public string ac_svc7_code
    {
        get
        {
            return this.ac_svc7_codeField;
        }
        set
        {
            this.ac_svc7_codeField = value;
        }
    }


    public string av_svc7_desc
    {
        get
        {
            return this.av_svc7_descField;
        }
        set
        {
            this.av_svc7_descField = value;
        }
    }


    public decimal ade_svc7_charge
    {
        get
        {
            return this.ade_svc7_chargeField;
        }
        set
        {
            this.ade_svc7_chargeField = value;
        }
    }


    public string ac_svc8_code
    {
        get
        {
            return this.ac_svc8_codeField;
        }
        set
        {
            this.ac_svc8_codeField = value;
        }
    }


    public string av_svc8_desc
    {
        get
        {
            return this.av_svc8_descField;
        }
        set
        {
            this.av_svc8_descField = value;
        }
    }


    public decimal ade_svc8_charge
    {
        get
        {
            return this.ade_svc8_chargeField;
        }
        set
        {
            this.ade_svc8_chargeField = value;
        }
    }


    public string ac_svc9_code
    {
        get
        {
            return this.ac_svc9_codeField;
        }
        set
        {
            this.ac_svc9_codeField = value;
        }
    }


    public string av_svc9_desc
    {
        get
        {
            return this.av_svc9_descField;
        }
        set
        {
            this.av_svc9_descField = value;
        }
    }


    public decimal ade_svc9_charge
    {
        get
        {
            return this.ade_svc9_chargeField;
        }
        set
        {
            this.ade_svc9_chargeField = value;
        }
    }


    public string ac_svc10_code
    {
        get
        {
            return this.ac_svc10_codeField;
        }
        set
        {
            this.ac_svc10_codeField = value;
        }
    }


    public string av_svc10_desc
    {
        get
        {
            return this.av_svc10_descField;
        }
        set
        {
            this.av_svc10_descField = value;
        }
    }


    public decimal ade_svc10_charge
    {
        get
        {
            return this.ade_svc10_chargeField;
        }
        set
        {
            this.ade_svc10_chargeField = value;
        }
    }


    public string ac_svc11_code
    {
        get
        {
            return this.ac_svc11_codeField;
        }
        set
        {
            this.ac_svc11_codeField = value;
        }
    }


    public string av_svc11_desc
    {
        get
        {
            return this.av_svc11_descField;
        }
        set
        {
            this.av_svc11_descField = value;
        }
    }


    public decimal ade_svc11_charge
    {
        get
        {
            return this.ade_svc11_chargeField;
        }
        set
        {
            this.ade_svc11_chargeField = value;
        }
    }


    public string ac_svc12_code
    {
        get
        {
            return this.ac_svc12_codeField;
        }
        set
        {
            this.ac_svc12_codeField = value;
        }
    }


    public string av_svc12_desc
    {
        get
        {
            return this.av_svc12_descField;
        }
        set
        {
            this.av_svc12_descField = value;
        }
    }


    public decimal ade_svc12_charge
    {
        get
        {
            return this.ade_svc12_chargeField;
        }
        set
        {
            this.ade_svc12_chargeField = value;
        }
    }
}

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[SerializableAttribute()]
[DebuggerStepThroughAttribute()]
[DesignerCategoryAttribute("code")]
[XmlTypeAttribute(Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public partial class return_info
{

    private string as_part_existField;

    private string as_inputparam_okField;


    public string as_part_exist
    {
        get
        {
            return this.as_part_existField;
        }
        set
        {
            this.as_part_existField = value;
        }
    }


    public string as_inputparam_ok
    {
        get
        {
            return this.as_inputparam_okField;
        }
        set
        {
            this.as_inputparam_okField = value;
        }
    }
}

[GeneratedCodeAttribute("wsdl", "4.8.3928.0")]
[SerializableAttribute()]
[DebuggerStepThroughAttribute()]
[DesignerCategoryAttribute("code")]
[XmlTypeAttribute(Namespace = "https://www.ssfparts.com/ssfconnect-v1.5/valueAdded")]
public partial class Get_image_wsInfo
{

    private string anv_reference_idField;

    private int ai_searchId_sequenceField;

    private int ai_return_idField;

    private string av_error_msjField;

    private byte[] pi_part_imageField;

    private string pi_part_image_encode_withField;


    public string anv_reference_id
    {
        get
        {
            return this.anv_reference_idField;
        }
        set
        {
            this.anv_reference_idField = value;
        }
    }


    public int ai_searchId_sequence
    {
        get
        {
            return this.ai_searchId_sequenceField;
        }
        set
        {
            this.ai_searchId_sequenceField = value;
        }
    }


    public int ai_return_id
    {
        get
        {
            return this.ai_return_idField;
        }
        set
        {
            this.ai_return_idField = value;
        }
    }

    public string av_error_msj
    {
        get
        {
            return this.av_error_msjField;
        }
        set
        {
            this.av_error_msjField = value;
        }
    }

    [XmlElementAttribute(DataType = "base64Binary")]
    public byte[] pi_part_image
    {
        get
        {
            return this.pi_part_imageField;
        }
        set
        {
            this.pi_part_imageField = value;
        }
    }

    public string pi_part_image_encode_with
    {
        get
        {
            return this.pi_part_image_encode_withField;
        }
        set
        {
            this.pi_part_image_encode_withField = value;
        }
    }
}
