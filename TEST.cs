﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Threading.Tasks;
using SSF_AutoParts.Functions;

namespace SSF_AutoParts
{
    static class TEST
    {
        public static XNamespace XN = "http://www.openapplications.org/oagis";
        public static string XN2 = "{http://www.aftermarket.org/oagis}";
        public static string XN3 = "{https://www.ssfparts.com/ssfconnect}";
        public static XElement Root1 = null;
        public static XElement Root2 = null;
        public static XmlDocument XmlDoc = new();
        public static XDocument XDoc = new();
        public static List<string> XmlList = new();

        public static void ParseXML(string tagName)
        {

            XmlDocument xmldoc = new XmlDocument();
            XmlNodeList xmlnode;
            int i = 0;

            string str;

            xmlnode = xmldoc.GetElementsByTagName(tagName);
            for (i = 0; i <= xmlnode.Count - 1; i++)
            {
                xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                str = xmlnode[i].ChildNodes.Item(0).InnerText.Trim() + "  " + xmlnode[i].ChildNodes.Item(1).InnerText.Trim() + "  " + xmlnode[i].ChildNodes.Item(2).InnerText.Trim();
                Console.WriteLine(str);
            }
            Console.ReadKey();
        }

        static string GetXmlValue(string elements, string attribute)
        {
            string newValue = string.Empty;

            IEnumerable<XElement> address =
               from el in Root1.Elements(XN + elements)
               where (string)el.Attribute(XN + "Type") == attribute
               select el;
            foreach (XElement el in address)
            {
                Console.WriteLine(el.Value);
                newValue = el.Value;
            }
            return newValue;
        }


        public static string IterateThroughNodesToGetValue(XmlNodeList NodesToSearchThrough, string valueToFind)
        {
            foreach (XmlNode testNode in NodesToSearchThrough)
            {
                XmlNodeList children = testNode.ChildNodes;
                foreach (XmlNode detailNode in children)
                {
                    Console.WriteLine(detailNode.InnerText);
                }
            }
            return "";
        }

        public static XmlDocument CreateXML(string elemeentToSelect)
        {
            XmlDocument xDoc = new();
            xDoc.Load(Globals.Path);
            XElement element = XElement.Load(Globals.Path);
            IEnumerable<XElement> elem = element.XPathSelectElements(elemeentToSelect);

            //element.XPathSelectElement();
            XmlReaderSettings settings = new XmlReaderSettings();
            IEnumerable<string> partNos = element.Descendants("Line").Select(x => (string)x.Attribute("SupplierItemId"));

            return xDoc;
        }

        public static void XmlNodeTypeDisplay()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            Globals.XmlReader = XmlReader.Create(Globals.Path, settings);
            Globals.XmlReader.MoveToContent();
            // Parse the file and display each of the nodes.
            string temp = string.Empty;
            while (Globals.XmlReader.Read())
            {
                switch (Globals.XmlReader.NodeType)
                {
                    case XmlNodeType.Element:                        
                        temp = string.Format("<{0}>", Globals.XmlReader.Name);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.Text:
                        temp = Globals.XmlReader.Value;
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.CDATA:
                        temp = string.Format("<![CDATA[{0}]]>", Globals.XmlReader.Value);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.ProcessingInstruction:
                        temp = string.Format("<?{0} {1}?>", Globals.XmlReader.Name, Globals.XmlReader.Value);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.Comment:
                        temp = string.Format("<!--{0}-->", Globals.XmlReader.Value);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.XmlDeclaration:
                        temp = string.Format("<?xml version='1.0'?>");
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.Document:                        
                        break;
                    case XmlNodeType.DocumentType:
                        temp = string.Format("<!DOCTYPE {0} [{1}]", Globals.XmlReader.Name, Globals.XmlReader.Value);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.EntityReference:
                        temp = Globals.XmlReader.Name;
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                    case XmlNodeType.EndElement:
                        temp = string.Format("</{0}>", Globals.XmlReader.Name);
                        Console.WriteLine(temp);
                        XmlList.Add(temp);
                        break;
                }               
            }

            if (Globals.IsTest)
            {
                Static_Functs.WriteFileFromList(XmlList);
            }
        }

        public static void FixAndLoadXmlDoc()
        {
            // File.Copy(path, )
            string newPath = Globals.Path.Replace(".xml", "_fixed.xml");

            using (FileStream rfs = new FileStream(Globals.Path, FileMode.Open, FileAccess.Read))
            {
                Globals.XMLDoc.Load(rfs);
            }
            using (FileStream wfs = new FileStream(newPath, FileMode.Create, FileAccess.Write))
            {
                Globals.XMLDoc.Save(wfs);
            }       
        }


        public static void WriteToConsole(string template, string value)
        {
            value = string.Format(template, value);
            Console.WriteLine(value);

        }
        public static string GetValueFromXML(string tagName)
        {
            string str = "";
            using (var reader = new StreamReader(Globals.Path))
            {
                var all = reader.ReadToEnd();
                StringReader stringReader = new StringReader(all);
                XmlReader xmlReader = XmlTextReader.Create(stringReader, new System.Xml.XmlReaderSettings() { IgnoreWhitespace = true, IgnoreComments = true });
                while (xmlReader.Read())
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == tagName)
                        str = xmlReader["value"];

                return str;
            }
        }

        public static void ReadXML()
        {
            XmlNodeList xmlnode;
            int i = 0;
            string str = null;
            FileStream fs = new FileStream(Globals.Path, FileMode.Open, FileAccess.Read);
            Globals.XMLDoc.Load(fs);
            xmlnode = Globals.XMLDoc.GetElementsByTagName("aaia:ManufacturerName");
            for (i = 0; i <= xmlnode.Count - 1; i++)
            {
                xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                str = xmlnode[i].ChildNodes.Item(0).InnerText.Trim() + "  " + xmlnode[i].ChildNodes.Item(1).InnerText.Trim() + "  " + xmlnode[i].ChildNodes.Item(2).InnerText.Trim();
                Console.WriteLine(str);
            }
        }
        public static void GetNode(string nodeXpath)
        {
            XmlNodeList xnList = XmlDoc.SelectNodes(nodeXpath);

            foreach (XmlNode xn in xnList)
            {
                string firstName = xn["Id"].InnerText;
                string lastName = xn["DocumentDateTime"].InnerText;
                Console.WriteLine(firstName + Environment.NewLine + lastName);
            }
        }

    }
}

