﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using SSF_AutoParts.Functions;

namespace SSF_AutoParts.XML_Classes
{
    public static class AddQuote_Extractor
    {
        public static void AddQuote()
        {
            XmlSerializer ser = new XmlSerializer(typeof(AddQuote_XSD));

            XmlDocument xml = new();

            string filename = "AddQuote_Response.xml";
            string newPath = Globals.TestPath + filename;
            

            if (File.Exists(newPath))
            {
                File.Delete(newPath);               
            }
            File.Copy(Globals.Path, newPath);

            //fixFile();
            bool exists = File.Exists(newPath);
            if (exists)
            {
                try
                {
                    Globals.XMLDoc.Load(newPath);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static string getBODId()
        {
            string bodId = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("BODId");
            if (list.Count > 0)
            {
                bodId = list[0].InnerText;
            }
            return bodId;
        }

        public static string GetReferenceID()
        {
            string referenceId = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("Sender");
            foreach (XmlNode listItem in list)
            {
                foreach (XmlNode item in listItem.ChildNodes)
                {
                    Console.WriteLine(item.Name + "  - " + item.InnerText);
                    if (item.Name.ToUpper() == "REFERENCEID")
                    {
                        referenceId = item.InnerText;
                        break;
                    }
                }
            }
            return referenceId;
        }

        public static string GetSupplierID()
        {
            string supplierId = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:SupplierItemId");
            if (list.Count > 0)
            {
                supplierId = list[0].InnerText.Trim();
            }

            return supplierId;
        }

        public static string GetFormattedSupplierID()
        {
            string formattedSupplierId = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("ssf:SupplierItemIdFormatted");
            if (list.Count > 0)
            {
                formattedSupplierId = list[0].InnerText.Trim();
            }

            return formattedSupplierId;
        }

        public static string GetDescription()
        {
            string description = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("Description");
            if (list.Count > 0)
            {
                description = list[0].InnerText.Trim();
            }

            return description;
        }

        public static Dictionary<string, int> GetAvailability()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("ssf:Availability");
            foreach (XmlNode node in list)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    Console.WriteLine(child.Name + "  - " + child.InnerText);
                    string key = child.Name.Replace("ssf:", "");
                    int value = int.Parse(child.InnerText);
                    if (value > 0)
                    {
                        if (!dict.ContainsKey(key))
                        {
                            dict.Add(key, value);
                        }
                    }
                }
            }
            return dict;
        }

        public static Public_Classes.PriceAndQty GetListPriceAndQty()
        {
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:ListPrice");
            Public_Classes.PriceAndQty upq = new();

            foreach (XmlNode parent in list)
            {
                foreach (XmlNode child in parent.ChildNodes)
                {
                    if (child.Name.ToUpper().Trim() == "AMOUNT")
                    {
                        upq.Amount = decimal.Parse(child.InnerText);
                    }
                    else if (child.Name.ToUpper().Trim() == "PERQUANTITY")
                    {
                        upq.PerQuantity = float.Parse(child.InnerText);
                    }
                }
            }

            return upq;
        }

        public static float GetOrderQtyForLineItem()
        {
            float orderQty = 0;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:Line");

            foreach (XmlNode node in list)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name.ToUpper().Trim() == "ORDERQUANTITY")
                    {
                        Console.WriteLine(child.Name + "  -  " + child.Value + "  =   " + child.InnerText);
                        orderQty = float.Parse(child.InnerText);
                        break;
                    }
                }
            }

            return orderQty;
        }

        public static Public_Classes.PriceAndQty GetUnitPriceAndQtyForLineItem()
        {
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:Line");
            Public_Classes.PriceAndQty upq = new();

            foreach (XmlNode parent in list)
            {
                foreach (XmlNode child in parent.ChildNodes)
                {
                    string nodeName = child.Name.ToUpper();
                    if (nodeName == "UNITPRICE")
                    {
                        foreach (XmlNode node in child.ChildNodes)
                        {
                            string childNodeName = node.Name.ToUpper();
                            if (childNodeName == "AMOUNT")
                            {
                                upq.Amount = decimal.Parse(node.InnerText);
                            }
                            else if (childNodeName == "PERQUANTITY")
                            {
                                upq.PerQuantity = float.Parse(node.InnerText);
                            }
                        }
                    }
                }
            }

            return upq;
        }

        public static void SetExtendedPriceForLineItem()
        {
            Decimal extendedPrice = 0;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:Line");

            foreach (XmlNode parent in list)
            {
                foreach (XmlNode child in parent.ChildNodes)
                {
                    string nodeName = child.Name.ToUpper();
                    if (nodeName == "EXTENDEDPRICE")
                    {
                        Globals.ExtendedPrice = decimal.Parse(child.InnerText);
                        break;
                    }
                }
            }

           
        }

        public static Decimal GetTotalAmountForLineItem()
        {
            Decimal totalPrice = 0;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("aaia:Line");

            foreach (XmlNode parent in list)
            {
                foreach (XmlNode child in parent.ChildNodes)
                {
                    string nodeName = child.Name.ToUpper();
                    if (nodeName == "TOTALAMOUNT")
                    {
                        totalPrice = decimal.Parse(child.InnerText);
                    }
                }
            }
            return totalPrice;
        }

        public static void GetFeatureValues()
        {
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("FeatureValue");

            foreach (XmlNode child in list)
            {
                if (child.NodeType == XmlNodeType.Element)
                {
                    if (child.HasChildNodes)
                    {
                        foreach (XmlNode grandchild in child.ChildNodes)
                        {
                            string key = grandchild.Attributes["name"].Value.Trim();
                            key = CreateUniqueKey(key);
                            string value = grandchild.InnerText.Trim();
                            Console.WriteLine(key + " --- " + value);
                            Globals.FeatureValueDict.Add(key, value);
                        }
                    }
                }
            }

            Static_Functs.GetFeatureValueDictionaryAsList();
        }             
        

        public static string CreateUniqueKey(string key)
        {
            int counter = 1;
            while (Globals.FeatureValueDict.ContainsKey(key))
            {
                key = key + counter;
                counter++;
            }

            return key;
        }

        public static string GetPrimaryShipFrom()
        {
            string primaryShipfrom = string.Empty;
            XmlNodeList list = Globals.XMLDoc.GetElementsByTagName("ssf:PrimaryShipFrom");
            if (list.Count == 1)
            {
                primaryShipfrom = list[0].InnerText;
            }

            return primaryShipfrom;
        }       
    }
}
