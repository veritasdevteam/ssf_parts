﻿

public partial class AddQuote_XSD
{

    private ApplicationArea applicationAreaField;
    private string revisionField;
    private string langField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public ApplicationArea ApplicationArea
    {
        get
        {
            return this.applicationAreaField;
        }
        set
        {
            this.applicationAreaField = value;
        }
    }
   

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string revision
    {
        get
        {
            return this.revisionField;
        }
        set
        {
            this.revisionField = value;
        }
    }


    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string lang
    {
        get
        {
            return this.langField;
        }
        set
        {
            this.langField = value;
        }
    }
}

/// <remarks/>

public partial class ApplicationArea
{ 
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ApplicationAreaSender
    {
        private string referenceIdField;
        private byte confirmationField;

        /// <remarks/>
        public string ReferenceId
        {
            get
            {
                return this.referenceIdField;
            }
            set
            {
                this.referenceIdField = value;
            }
        }

        /// <remarks/>
        public byte Confirmation
        {
            get
            {
                return this.confirmationField;
            }
            set
            {
                this.confirmationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class DataArea
    {

        private DataAreaAdd addField;

        private DataAreaQuote quoteField;

        /// <remarks/>
        public DataAreaAdd Add
        {
            get
            {
                return this.addField;
            }
            set
            {
                this.addField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuote Quote
        {
            get
            {
                return this.quoteField;
            }
            set
            {
                this.quoteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaAdd
    {

        private string confirmField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string confirm
        {
            get
            {
                return this.confirmField;
            }
            set
            {
                this.confirmField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuote
    {

        private DataAreaQuoteHeader headerField;

        private Line lineField;

        /// <remarks/>
        public DataAreaQuoteHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.aftermarket.org/oagis")]
        public Line Line
        {
            get
            {
                return this.lineField;
            }
            set
            {
                this.lineField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeader
    {

        private DataAreaQuoteHeaderDocumentIds documentIdsField;

        private System.DateTime lastModificationDateTimeField;

        private System.DateTime documentDateTimeField;

        private DataAreaQuoteHeaderDocumentReferences documentReferencesField;

        private System.DateTime promisedShipDateField;

        private DataAreaQuoteHeaderTransportationTerm transportationTermField;

        private DataAreaQuoteHeaderExtendedPrice extendedPriceField;

        private DataAreaQuoteHeaderCharges chargesField;

        private DataAreaQuoteHeaderTotalAmount totalAmountField;

        private PaymentTerms paymentTermsField;

        private DataAreaQuoteHeaderNote noteField;

        private byte dropShipIndField;

        private DataAreaQuoteHeaderParties partiesField;

        /// <remarks/>
        public DataAreaQuoteHeaderDocumentIds DocumentIds
        {
            get
            {
                return this.documentIdsField;
            }
            set
            {
                this.documentIdsField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LastModificationDateTime
        {
            get
            {
                return this.lastModificationDateTimeField;
            }
            set
            {
                this.lastModificationDateTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DocumentDateTime
        {
            get
            {
                return this.documentDateTimeField;
            }
            set
            {
                this.documentDateTimeField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderDocumentReferences DocumentReferences
        {
            get
            {
                return this.documentReferencesField;
            }
            set
            {
                this.documentReferencesField = value;
            }
        }

        /// <remarks/>
        public System.DateTime PromisedShipDate
        {
            get
            {
                return this.promisedShipDateField;
            }
            set
            {
                this.promisedShipDateField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderTransportationTerm TransportationTerm
        {
            get
            {
                return this.transportationTermField;
            }
            set
            {
                this.transportationTermField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderExtendedPrice ExtendedPrice
        {
            get
            {
                return this.extendedPriceField;
            }
            set
            {
                this.extendedPriceField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderCharges Charges
        {
            get
            {
                return this.chargesField;
            }
            set
            {
                this.chargesField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderTotalAmount TotalAmount
        {
            get
            {
                return this.totalAmountField;
            }
            set
            {
                this.totalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.aftermarket.org/oagis")]
        public PaymentTerms PaymentTerms
        {
            get
            {
                return this.paymentTermsField;
            }
            set
            {
                this.paymentTermsField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderNote Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public byte DropShipInd
        {
            get
            {
                return this.dropShipIndField;
            }
            set
            {
                this.dropShipIndField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderParties Parties
        {
            get
            {
                return this.partiesField;
            }
            set
            {
                this.partiesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentIds
    {
        private DataAreaQuoteHeaderDocumentIdsCustomerDocumentId customerDocumentIdField;
        private DataAreaQuoteHeaderDocumentIdsSupplierDocumentId supplierDocumentIdField;

        /// <remarks/>
        public DataAreaQuoteHeaderDocumentIdsCustomerDocumentId CustomerDocumentId
        {
            get
            {
                return this.customerDocumentIdField;
            }
            set
            {
                this.customerDocumentIdField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderDocumentIdsSupplierDocumentId SupplierDocumentId
        {
            get
            {
                return this.supplierDocumentIdField;
            }
            set
            {
                this.supplierDocumentIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentIdsCustomerDocumentId
    {
        private object idField;
        private byte revisionField;

        /// <remarks/>
        public object Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public byte Revision
        {
            get
            {
                return this.revisionField;
            }
            set
            {
                this.revisionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentIdsSupplierDocumentId
    {
        private string idField;
        private byte revisionField;
        /// <remarks/>
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public byte Revision
        {
            get
            {
                return this.revisionField;
            }
            set
            {
                this.revisionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentReferences
    {
        private DataAreaQuoteHeaderDocumentReferencesRFQDocumentReference rFQDocumentReferenceField;
        /// <remarks/>
        public DataAreaQuoteHeaderDocumentReferencesRFQDocumentReference RFQDocumentReference
        {
            get
            {
                return this.rFQDocumentReferenceField;
            }
            set
            {
                this.rFQDocumentReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentReferencesRFQDocumentReference
    {
        private DataAreaQuoteHeaderDocumentReferencesRFQDocumentReferenceDocumentIds documentIdsField;
        /// <remarks/>
        public DataAreaQuoteHeaderDocumentReferencesRFQDocumentReferenceDocumentIds DocumentIds
        {
            get
            {
                return this.documentIdsField;
            }
            set
            {
                this.documentIdsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderDocumentReferencesRFQDocumentReferenceDocumentIds
    {
        private string[] customerDocumentIdField;
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Id", IsNullable = false)]
        public string[] CustomerDocumentId
        {
            get
            {
                return this.customerDocumentIdField;
            }
            set
            {
                this.customerDocumentIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderTransportationTerm
    {
        private string freightTermsField;
        /// <remarks/>
        public string FreightTerms
        {
            get
            {
                return this.freightTermsField;
            }
            set
            {
                this.freightTermsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderExtendedPrice
    {
        private string currencyField;
        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderCharges
    {

        private DataAreaQuoteHeaderChargesAdditionalCharge additionalChargeField;

        /// <remarks/>
        public DataAreaQuoteHeaderChargesAdditionalCharge AdditionalCharge
        {
            get
            {
                return this.additionalChargeField;
            }
            set
            {
                this.additionalChargeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderChargesAdditionalCharge
    {

        private DataAreaQuoteHeaderChargesAdditionalChargeTotal totalField;

        private DataAreaQuoteHeaderChargesAdditionalChargeDescription descriptionField;

        /// <remarks/>
        public DataAreaQuoteHeaderChargesAdditionalChargeTotal Total
        {
            get
            {
                return this.totalField;
            }
            set
            {
                this.totalField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderChargesAdditionalChargeDescription Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderChargesAdditionalChargeTotal
    {

        private string currencyField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderChargesAdditionalChargeDescription
    {

        private string langField;

        private string ownerField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderTotalAmount
    {

        private string currencyField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
    public partial class PaymentTerms
    {

        private string termIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public string TermId
        {
            get
            {
                return this.termIdField;
            }
            set
            {
                this.termIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderNote
    {

        private string langField;

        private string authorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderParties
    {

        private DataAreaQuoteHeaderPartiesBillToParty billToPartyField;

        private DataAreaQuoteHeaderPartiesShipToParty shipToPartyField;

        private DataAreaQuoteHeaderPartiesShipFromParty shipFromPartyField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesBillToParty BillToParty
        {
            get
            {
                return this.billToPartyField;
            }
            set
            {
                this.billToPartyField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipToParty ShipToParty
        {
            get
            {
                return this.shipToPartyField;
            }
            set
            {
                this.shipToPartyField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipFromParty ShipFromParty
        {
            get
            {
                return this.shipFromPartyField;
            }
            set
            {
                this.shipFromPartyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesBillToParty
    {

        private DataAreaQuoteHeaderPartiesBillToPartyPartyId partyIdField;

        private DataAreaQuoteHeaderPartiesBillToPartyName nameField;

        private DataAreaQuoteHeaderPartiesBillToPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesBillToPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesBillToPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesBillToPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesBillToPartyPartyId
    {

        private uint idField;

        /// <remarks/>
        public uint Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesBillToPartyName
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesBillToPartyAddresses
    {

        private DataAreaQuoteHeaderPartiesBillToPartyAddressesAddress addressField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesBillToPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesBillToPartyAddressesAddress
    {

        private string[] addressLineField;

        private string cityField;

        private string stateOrProvinceField;

        private string countryField;

        private string postalCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipToParty
    {

        private DataAreaQuoteHeaderPartiesShipToPartyPartyId partyIdField;

        private DataAreaQuoteHeaderPartiesShipToPartyName nameField;

        private DataAreaQuoteHeaderPartiesShipToPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipToPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipToPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipToPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipToPartyPartyId
    {

        private uint idField;

        /// <remarks/>
        public uint Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipToPartyName
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipToPartyAddresses
    {

        private DataAreaQuoteHeaderPartiesShipToPartyAddressesAddress addressField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipToPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipToPartyAddressesAddress
    {

        private string[] addressLineField;

        private string cityField;

        private string stateOrProvinceField;

        private string countryField;

        private string postalCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipFromParty
    {

        private DataAreaQuoteHeaderPartiesShipFromPartyPartyId partyIdField;

        private DataAreaQuoteHeaderPartiesShipFromPartyName nameField;

        private DataAreaQuoteHeaderPartiesShipFromPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipFromPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipFromPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipFromPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipFromPartyPartyId
    {

        private string idField;

        /// <remarks/>
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipFromPartyName
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipFromPartyAddresses
    {

        private DataAreaQuoteHeaderPartiesShipFromPartyAddressesAddress addressField;

        /// <remarks/>
        public DataAreaQuoteHeaderPartiesShipFromPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class DataAreaQuoteHeaderPartiesShipFromPartyAddressesAddress
    {

        private string[] addressLineField;

        private string cityField;

        private string stateOrProvinceField;

        private string countryField;

        private uint postalCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public uint PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
    public partial class Line
    {
        private byte lineNumberField;
        private LineOrderItem orderItemField;
        private byte orderQuantityField;
        private LineListPrice listPriceField;
        private UnitPrice unitPriceField;
        private ExtendedPrice extendedPriceField;
        private TotalAmount totalAmountField;
        private LineSubLine subLineField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte LineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }

        /// <remarks/>
        public LineOrderItem OrderItem
        {
            get
            {
                return this.orderItemField;
            }
            set
            {
                this.orderItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte OrderQuantity
        {
            get
            {
                return this.orderQuantityField;
            }
            set
            {
                this.orderQuantityField = value;
            }
        }

        /// <remarks/>
        public LineListPrice ListPrice
        {
            get
            {
                return this.listPriceField;
            }
            set
            {
                this.listPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public UnitPrice UnitPrice
        {
            get
            {
                return this.unitPriceField;
            }
            set
            {
                this.unitPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ExtendedPrice ExtendedPrice
        {
            get
            {
                return this.extendedPriceField;
            }
            set
            {
                this.extendedPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public TotalAmount TotalAmount
        {
            get
            {
                return this.totalAmountField;
            }
            set
            {
                this.totalAmountField = value;
            }
        }

        /// <remarks/>
        public LineSubLine SubLine
        {
            get
            {
                return this.subLineField;
            }
            set
            {
                this.subLineField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItem
    {

        private LineOrderItemItemIds itemIdsField;

        private Description[] descriptionField;

        private ItemStatus itemStatusField;

        private FeatureValueNameValue[] featureValueField;

        /// <remarks/>
        public LineOrderItemItemIds ItemIds
        {
            get
            {
                return this.itemIdsField;
            }
            set
            {
                this.itemIdsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Description", Namespace = "http://www.openapplications.org/oagis")]
        public Description[] Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ItemStatus ItemStatus
        {
            get
            {
                return this.itemStatusField;
            }
            set
            {
                this.itemStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace = "http://www.openapplications.org/oagis")]
        [System.Xml.Serialization.XmlArrayItemAttribute("NameValue", IsNullable = false)]
        public FeatureValueNameValue[] FeatureValue
        {
            get
            {
                return this.featureValueField;
            }
            set
            {
                this.featureValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItemItemIds
    {

        private object catalogSearchField;

        private SearchId searchIdField;

        private CustomerItemId customerItemIdField;

        private LineOrderItemItemIdsSupplierItemId supplierItemIdField;

        private SupplierItemIdFormatted supplierItemIdFormattedField;

        private byte manufacturerCodeField;

        private string manufacturerNameField;

        private HazardousMaterialInt hazardousMaterialIntField;

        private PrimaryShipFrom primaryShipFromField;

        private Availability availabilityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public object CatalogSearch
        {
            get
            {
                return this.catalogSearchField;
            }
            set
            {
                this.catalogSearchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public SearchId SearchId
        {
            get
            {
                return this.searchIdField;
            }
            set
            {
                this.searchIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public CustomerItemId CustomerItemId
        {
            get
            {
                return this.customerItemIdField;
            }
            set
            {
                this.customerItemIdField = value;
            }
        }

        /// <remarks/>
        public LineOrderItemItemIdsSupplierItemId SupplierItemId
        {
            get
            {
                return this.supplierItemIdField;
            }
            set
            {
                this.supplierItemIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public SupplierItemIdFormatted SupplierItemIdFormatted
        {
            get
            {
                return this.supplierItemIdFormattedField;
            }
            set
            {
                this.supplierItemIdFormattedField = value;
            }
        }

        /// <remarks/>
        public byte ManufacturerCode
        {
            get
            {
                return this.manufacturerCodeField;
            }
            set
            {
                this.manufacturerCodeField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerName
        {
            get
            {
                return this.manufacturerNameField;
            }
            set
            {
                this.manufacturerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public HazardousMaterialInt HazardousMaterialInt
        {
            get
            {
                return this.hazardousMaterialIntField;
            }
            set
            {
                this.hazardousMaterialIntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public PrimaryShipFrom PrimaryShipFrom
        {
            get
            {
                return this.primaryShipFromField;
            }
            set
            {
                this.primaryShipFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public Availability Availability
        {
            get
            {
                return this.availabilityField;
            }
            set
            {
                this.availabilityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class SearchId
    {

        private uint sequenceField;

        /// <remarks/>
        public uint Sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class CustomerItemId
    {

        private string idField;

        /// <remarks/>
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItemItemIdsSupplierItemId
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class SupplierItemIdFormatted
    {

        private string supplierItemIdFormatted1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SupplierItemIdFormatted")]
        public string SupplierItemIdFormatted1
        {
            get
            {
                return this.supplierItemIdFormatted1Field;
            }
            set
            {
                this.supplierItemIdFormatted1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class HazardousMaterialInt
    {

        private object codeField;

        /// <remarks/>
        public object Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class PrimaryShipFrom
    {

        private string locationField;

        /// <remarks/>
        public string Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class Availability
    {

        private byte sfField;

        private byte lbField;

        private byte sdField;

        private byte phField;

        private byte ocField;

        /// <remarks/>
        public byte SF
        {
            get
            {
                return this.sfField;
            }
            set
            {
                this.sfField = value;
            }
        }

        /// <remarks/>
        public byte LB
        {
            get
            {
                return this.lbField;
            }
            set
            {
                this.lbField = value;
            }
        }

        /// <remarks/>
        public byte SD
        {
            get
            {
                return this.sdField;
            }
            set
            {
                this.sdField = value;
            }
        }

        /// <remarks/>
        public byte PH
        {
            get
            {
                return this.phField;
            }
            set
            {
                this.phField = value;
            }
        }

        /// <remarks/>
        public byte OC
        {
            get
            {
                return this.ocField;
            }
            set
            {
                this.ocField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class Description
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class ItemStatus
    {

        private ItemStatusChange changeField;

        /// <remarks/>
        public ItemStatusChange Change
        {
            get
            {
                return this.changeField;
            }
            set
            {
                this.changeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ItemStatusChange
    {

        private object[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Description", typeof(ItemStatusChangeDescription))]
        [System.Xml.Serialization.XmlElementAttribute("To", typeof(string))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ItemStatusChangeDescription
    {

        private string langField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class FeatureValueNameValue
    {

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineListPrice
    {

        private Amount amountField;

        private byte perQuantityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public Amount Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte PerQuantity
        {
            get
            {
                return this.perQuantityField;
            }
            set
            {
                this.perQuantityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class Amount
    {

        private string currencyField;

        private byte valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class UnitPrice
    {

        private UnitPriceAmount amountField;

        private byte perQuantityField;

        /// <remarks/>
        public UnitPriceAmount Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public byte PerQuantity
        {
            get
            {
                return this.perQuantityField;
            }
            set
            {
                this.perQuantityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class UnitPriceAmount
    {

        private string currencyField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class ExtendedPrice
    {

        private string currencyField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class TotalAmount
    {

        private string currencyField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineSubLine
    {

        private byte lineNumberField;

        private LineSubLineOrderItem orderItemField;

        private byte orderQuantityField;

        private LineSubLineListPrice listPriceField;

        private UnitPrice unitPriceField;

        private ExtendedPrice extendedPriceField;

        private TotalAmount totalAmountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte LineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }

        /// <remarks/>
        public LineSubLineOrderItem OrderItem
        {
            get
            {
                return this.orderItemField;
            }
            set
            {
                this.orderItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte OrderQuantity
        {
            get
            {
                return this.orderQuantityField;
            }
            set
            {
                this.orderQuantityField = value;
            }
        }

        /// <remarks/>
        public LineSubLineListPrice ListPrice
        {
            get
            {
                return this.listPriceField;
            }
            set
            {
                this.listPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public UnitPrice UnitPrice
        {
            get
            {
                return this.unitPriceField;
            }
            set
            {
                this.unitPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ExtendedPrice ExtendedPrice
        {
            get
            {
                return this.extendedPriceField;
            }
            set
            {
                this.extendedPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public TotalAmount TotalAmount
        {
            get
            {
                return this.totalAmountField;
            }
            set
            {
                this.totalAmountField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineSubLineOrderItem
    {

        private LineSubLineOrderItemItemIds itemIdsField;

        private Description[] descriptionField;

        private ItemStatus itemStatusField;

        private FeatureValueNameValue[] featureValueField;

        /// <remarks/>
        public LineSubLineOrderItemItemIds ItemIds
        {
            get
            {
                return this.itemIdsField;
            }
            set
            {
                this.itemIdsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Description", Namespace = "http://www.openapplications.org/oagis")]
        public Description[] Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ItemStatus ItemStatus
        {
            get
            {
                return this.itemStatusField;
            }
            set
            {
                this.itemStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace = "http://www.openapplications.org/oagis")]
        [System.Xml.Serialization.XmlArrayItemAttribute("NameValue", IsNullable = false)]
        public FeatureValueNameValue[] FeatureValue
        {
            get
            {
                return this.featureValueField;
            }
            set
            {
                this.featureValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineSubLineOrderItemItemIds
    {

        private object catalogSearchField;

        private SearchId searchIdField;

        private CustomerItemId customerItemIdField;

        private LineSubLineOrderItemItemIdsSupplierItemId supplierItemIdField;

        private SupplierItemIdFormatted supplierItemIdFormattedField;

        private byte manufacturerCodeField;

        private string manufacturerNameField;

        private HazardousMaterialInt hazardousMaterialIntField;

        private PrimaryShipFrom primaryShipFromField;

        private Availability availabilityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public object CatalogSearch
        {
            get
            {
                return this.catalogSearchField;
            }
            set
            {
                this.catalogSearchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public SearchId SearchId
        {
            get
            {
                return this.searchIdField;
            }
            set
            {
                this.searchIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public CustomerItemId CustomerItemId
        {
            get
            {
                return this.customerItemIdField;
            }
            set
            {
                this.customerItemIdField = value;
            }
        }

        /// <remarks/>
        public LineSubLineOrderItemItemIdsSupplierItemId SupplierItemId
        {
            get
            {
                return this.supplierItemIdField;
            }
            set
            {
                this.supplierItemIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public SupplierItemIdFormatted SupplierItemIdFormatted
        {
            get
            {
                return this.supplierItemIdFormattedField;
            }
            set
            {
                this.supplierItemIdFormattedField = value;
            }
        }

        /// <remarks/>
        public byte ManufacturerCode
        {
            get
            {
                return this.manufacturerCodeField;
            }
            set
            {
                this.manufacturerCodeField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerName
        {
            get
            {
                return this.manufacturerNameField;
            }
            set
            {
                this.manufacturerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public HazardousMaterialInt HazardousMaterialInt
        {
            get
            {
                return this.hazardousMaterialIntField;
            }
            set
            {
                this.hazardousMaterialIntField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public PrimaryShipFrom PrimaryShipFrom
        {
            get
            {
                return this.primaryShipFromField;
            }
            set
            {
                this.primaryShipFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public Availability Availability
        {
            get
            {
                return this.availabilityField;
            }
            set
            {
                this.availabilityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineSubLineOrderItemItemIdsSupplierItemId
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineSubLineListPrice
    {

        private Amount amountField;

        private byte perQuantityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public Amount Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte PerQuantity
        {
            get
            {
                return this.perQuantityField;
            }
            set
            {
                this.perQuantityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class FeatureValue
    {

        private FeatureValueNameValue[] nameValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NameValue")]
        public FeatureValueNameValue[] NameValue
        {
            get
            {
                return this.nameValueField;
            }
            set
            {
                this.nameValueField = value;
            }
        }
    }
}

