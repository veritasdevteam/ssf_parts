﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SSF_AutoParts.SQL;
using SSF_AutoParts.XML_Classes;
using SSF_AutoParts.Functions;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Xml.Serialization;
using ServiceReference1;

namespace SSF_AutoParts
{
    public static class AddRequestForQuote_Builder
    {
        public static AddRequestForQuote AddRequestForQuote()
        {
            AddRequestForQuote temp = new();        
            temp.ApplicationArea = FillApplicationArea();
            temp.DataArea = FillDataArea();
            // Attributes
            temp.environment = "Test";
            temp.lang = "en";
            temp.revision = "1.2.1";
            
            if (Globals.IsTest)
            {
                writeClassToXMLFile(temp);
            }
            return temp;
        }

        private static void writeClassToXMLFile(AddRequestForQuote xml)
        {
            string fullPath = Globals.TestPath + "Serialized_AddRequestForQuote.xml";

            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            string convertedXML = Static_Functs.SerializeObject(xml);
            convertedXML = new Regex("\\<\\?xml.*\\?>").Replace(convertedXML, "");
            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (convertedXML.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
            {
                convertedXML = convertedXML.Remove(0, _byteOrderMarkUtf8.Length);
            }            
            Globals.xDoc = new();
            Globals.XMLDoc.LoadXml(convertedXML);
            Globals.xDoc = XDocument.Parse(convertedXML);
            Globals.XMLDoc.Save(fullPath);
        }

        public static ApplicationArea FillApplicationArea()
        {
            ApplicationArea temp = new ApplicationArea();
            temp.BODId = Globals.BODId; // Need to get this!
            temp.CreationDateTime = System.DateTime.Now;
            temp.Sender = getSender();

            return temp;
        }

        public static DataArea FillDataArea()
        {
            DataArea da = new DataArea();
            da.Add = new();
            da.Add.confirm = "Always";          
            da.RequestForQuote = new();
            da.RequestForQuote.Header = FillApplicationAreaHeader();
            da.RequestForQuote.Line = FillLineInfo();
            da.Process = getProcess();          

            return da;
        }

        public static Header FillApplicationAreaHeader()
        {
            Header head = new();
            head.DocumentDateTime = System.DateTime.Now;
            head.LastModificationDateTime = System.DateTime.Now;
            head.Charges = FillCharges();
            head.DocumentIds = FillHeaderDocumentIDs();
            head.PaymentTerms = new();
            head.PaymentTerms.TermId = null;
            head.PromisedShipDate = System.DateTime.Now; // Override with a later date?
            head.DocumentReferences = getDocumentReferences();
            head.DropShipInd = 0;
            head.ExtendedPrice = null;
            
            Header.Note temp = new();           
            temp.author = "Delivery Notes";
            temp.lang = "en";
            head.Notes = temp;
            
            head.Parties = fillParties();
            head.Notes = getNotes();
            return head;
        }

        private static DataAreaProcess getProcess()
        {
            DataAreaProcess data = new();
            data.confirm = "Always";
            return data;
        }

        private static Header.Note getNotes()
        {
            Header.Note temp = new();
            temp.author = "Delivery Notes";
            temp.lang = "en";
            return temp;
        }
        private static DocumentReferences getDocumentReferences()
        {
            DocumentReferences doc = new();
            doc.RFQDocumentReference = new();
            doc.RFQDocumentReference.DocumentIds = new();
            doc.RFQDocumentReference.DocumentIds.CustomerDocumentId = null;
            return doc;
        }

        private static DocumentIds FillHeaderDocumentIDs()
        {
            DocumentIds ids = new();
            ids.CustomerDocumentId = new();
            ids.CustomerDocumentId.Revision = 1;
            ids.CustomerDocumentId.Id = null;
            //
            ids.SupplierDocumentId = new();
            ids.SupplierDocumentId.Revision = 1;
            ids.SupplierDocumentId.Id = null;       

            return ids;
        }       

        private static DocumentReferences fillDocumentRefs()
        {
            DocumentReferences doc = new DocumentReferences();
            doc.RFQDocumentReference.DocumentIds.CustomerDocumentId = null;
            return doc;
        }

        private static HeaderPaymentTerms fillPaymentTerms()
        {
            HeaderPaymentTerms term = new();
            term.TermId = null;
            return term;
        }

        public static Line FillLineInfo()
        {
            Line line = new();
            line.LineNumber = Convert.ToByte(Globals.GetCurrentLineNumber());

            line.OrderQuantity = Globals.ReqQty;
            line.OrderItem = getOrderItem();

            return line;
        }

        public static Charges FillCharges()
        {
            Charges ch = new();
            ch.AdditionalCharge = new();
            ch.AdditionalCharge.Description = null;
            ch.AdditionalCharge.Total = null;

            return ch;
        }

        public static DocumentIds FillDocumentIDs1()
        {
            DocumentIds doc = new();
            doc.CustomerDocumentId.Revision = 1;
            doc.CustomerDocumentId.Id = null;
            doc.SupplierDocumentId.Revision = 1;
            doc.SupplierDocumentId.Id = null;
            return doc;
        }

        private static DocumentIds fillDocumentTypeIDs()
        {
            DocumentIds doc = new();
            doc.CustomerDocumentId.Id = null; // TJF
            doc.CustomerDocumentId.Revision = 001;
            doc.SupplierDocumentId.Id = null;
            doc.SupplierDocumentId.Revision = 001;

            return doc;
        }       

        private static XML_Classes.Parties fillParties()
        {
            Parties p = new();
            p.BillToParty = fillBillTo();
            p.ShipFromParty = FillShipFrom();
            p.ShipToParty = FillShipTo();

            return p;
        }

        private static XML_Classes.PartiesBillToParty fillBillTo()
        {
            XML_Classes.PartiesBillToParty bill = new();
            bill.active = 1;
            bill.Addresses = getBillToAddresses();
            bill.PartyId = getBillToPartyID();
            bill.Name = getBillToName();
            bill.Name.lang = "en";

            return bill;
        }   

        private static PartiesBillToPartyName getBillToName()
        {
            PartiesBillToPartyName name = new();            
            name.lang = "en";
            name.Value = ConfigurationManager.AppSettings.Get("BillToName");

            return name;
        }

        private static XML_Classes.PartiesBillToPartyAddresses getBillToAddresses()
        {
            PartiesBillToPartyAddressesAddress address = new();
            address.AddressLine = getBillToAddressLine();
            address.City = ConfigurationManager.AppSettings.Get("BillToCity");
            address.StateOrProvince = ConfigurationManager.AppSettings.Get("BillToState");
            address.PostalCode = ConfigurationManager.AppSettings.Get("BillToZIP");
            address.Country = ConfigurationManager.AppSettings.Get("BillToCountry");
            address.Telephone = ConfigurationManager.AppSettings.Get("BillToPhone");
            PartiesBillToPartyAddresses add2 = new();
            add2.Address = address;

            return add2;
        }

        private static LineOrderItem getOrderItem()
        {
            LineOrderItem item = new();            
            LineOrderItemItemIds items = new();
            items.CatalogSearch = getCatalogSearch();            
            items.ManufacturerCode = getManufacturerCode();
            items.PrimaryShipFrom = getPrimaryShipFrom();
            items.CustomerItemId = getCustomerItemID();
            items.SupplierItemId = getSupplierItemId();
            item.ItemIds = items;
            return item;
        }
        private static LineOrderItemItemIdsSupplierItemId getSupplierItemId()
        {
            LineOrderItemItemIdsSupplierItemId id = new();
            id.Id = Globals.PartNo;
            return id;
        }
        private static CustomerItemId GetCustomerItemId()
        {
            CustomerItemId id = new();
            id.Id = null;
            return id;
        }               

        public static XML_Classes.PartiesShipToParty FillShipTo()
        {
            SQL_Select sel = new();

            sel.FillServiceCenterData();

            PartiesShipToParty shipTo = new();
            shipTo.active = 1;
            shipTo.oneTime = 0;

            shipTo.Name = GetShipToName();
            shipTo.Addresses = new();
            shipTo.Addresses.Address = new();
            shipTo.Addresses.Address.AddressLine = getShipToAddressLine();
            shipTo.Addresses.Address.City = SQL_Select.Ship.ShipToCity;
            shipTo.Addresses.Address.StateOrProvince = SQL_Select.Ship.ShipToState;
            shipTo.Addresses.Address.PostalCode = SQL_Select.Ship.ShipToPostalCode;
            shipTo.Addresses.Address.Country = SQL_Select.Ship.ShipToCountry;
            shipTo.Addresses.Address.Telephone = SQL_Select.Ship.ShipToPhone;

            return shipTo;
        }

        public static PartiesShipFromParty FillShipFrom()
        {
            PartiesShipFromParty shipFrom = new();

            // Attributes
            shipFrom.active = 1;
            shipFrom.oneTime = 0;

            // Actual tags
            shipFrom.PartyId = getShipFromPartyID();
            shipFrom.Name = getShipFromPartyName();
            shipFrom.Addresses = fillShipFromAddresses();

            return shipFrom;
        }

        private static object getCatalogSearch()
        {
            object catSearch = null;
            return catSearch;
        }     

        private static ApplicationArea.ApplicationAreaSender getSender()
        {
            ApplicationArea.ApplicationAreaSender send = new();
            send.Confirmation = 1;
            send.ReferenceId = "";
            return send;
        }
       
        
        private static PartiesShipFromPartyName getShipFromPartyName()
        {            
            PartiesShipFromParty party = new();
            PartiesShipFromPartyName name = new();
            string shipFromName = ConfigurationManager.AppSettings.Get("ShipFromName");
            name.Value = shipFromName;
            name.lang = "en";            
            party.Name = name;           
            return name;        
        }

        private static PartiesShipFromPartyAddresses fillShipFromAddresses()
        {
            PartiesShipFromPartyAddresses add = new();
            add.Address = new();
            add.Address.AddressLine = GetShipFromAddressLine();
            add.Address.City = ConfigurationManager.AppSettings.Get("ShipFromCity");
            add.Address.StateOrProvince = ConfigurationManager.AppSettings.Get("ShipFromState");
            add.Address.PostalCode = convertPostalCodeToUInt();
            add.Address.Country = ConfigurationManager.AppSettings.Get("ShipFromCountry");
            add.Address.Telephone = ConfigurationManager.AppSettings.Get("ShipFromTelephone");

            return add;
        }

        private static void SetOrderQty()
        {
            SQL_Select sel = new();
            sel.SetReqQtyFromClaimDetail();
        }

        private static uint convertPostalCodeToUInt()
        {
            string zip = ConfigurationManager.AppSettings.Get("ShipFromZIP");
            uint temp = Convert.ToUInt32(zip, 16);
            return temp;
        }

        private static string[] getShipToAddressLine()
        {
            SQL_Select sel = new();
            string[] add1 = new string[1];
            add1[0] = SQL_Select.Ship.ShipToAdd1;
            return add1;
        }

       private static  string[] getBillToAddressLine()
        {
            string[] add = new string[1];
            string address1 = ConfigurationManager.AppSettings.Get("BillToAddr1");
            add[0] = address1;
            return add;
        }

        private static string[] GetShipFromAddressLine()
        {
            string[] add = new string[1];
            add[0] = ConfigurationManager.AppSettings.Get("ShipFromAddr1");
            return add;
        }

        private static CustomerItemId getCustomerID()
        {
            CustomerItemId cust = new();
            cust.Id = ""; // ConfigurationManager
            return cust;
        }

        private static byte? getManufacturerCode()
        {
            return null;
        }       

        private static CustomerItemId getCustomerItemID()
        {
            CustomerItemId id = new();
            id.Id = Globals.PartNo.ToString();
            return id;
        }

        private static PrimaryShipFrom getPrimaryShipFrom()
        {
            PrimaryShipFrom ship = new();
            Static_Functs.SetClosestWarehouse();
            ship.Location = Globals.ClosestWarehouse;

            return ship;
        }
      

        private static PartiesBillToPartyPartyId getBillToPartyID()
        {
            PartiesBillToPartyPartyId part = new();
            string partyID = ConfigurationManager.AppSettings.Get("BillToPartyID");
            part.Id = ushort.Parse(partyID);  
            
            return part;
        }

        private static PartiesShipFromPartyPartyId getShipFromPartyID()
        {
            PartiesShipFromPartyPartyId part = new();
            string partyID = ConfigurationManager.AppSettings.Get("ShipFromPartyID");
            part.Id = partyID;

            return part;
        }

        public static PartiesShipToPartyName GetShipToName()
        {
            SQL_Select sel = new();
            sel.FillServiceCenterData();
            string name = SQL_Select.Ship.ShipToName;
            PartiesShipToPartyName pn = new();            
            pn.lang = "en";
            pn.Value = name;

            return pn;
        }
    }
}



