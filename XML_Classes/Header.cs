﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSF_AutoParts.XML_Classes
{
    public partial class Header
    {
        private DocumentIds documentIdsField;

        private System.DateTime lastModificationDateTimeField;

        private System.DateTime documentDateTimeField;

        private DocumentReferences documentReferencesField;

        private System.DateTime promisedShipDateField;

        private ExtendedPrice extendedPriceField;

        private TotalAmount totalAmountField;

        private HeaderPaymentTerms paymentTermsField;

        private Charges chargesField;        

        private byte dropShipIndField;

        private Parties partiesField;

        private ChargesAdditionalCharge additionalChargeField;

        private Note noteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public DocumentIds DocumentIds
        {
            get
            {
                return this.documentIdsField;
            }
            set
            {
                this.documentIdsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public System.DateTime LastModificationDateTime
        {
            get
            {
                return this.lastModificationDateTimeField;
            }
            set
            {
                this.lastModificationDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public System.DateTime DocumentDateTime
        {
            get
            {
                return this.documentDateTimeField;
            }
            set
            {
                this.documentDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public DocumentReferences DocumentReferences
        {
            get
            {
                return this.documentReferencesField;
            }
            set
            {
                this.documentReferencesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis", DataType = "date")]
        public System.DateTime PromisedShipDate
        {
            get
            {
                return this.promisedShipDateField;
            }
            set
            {
                this.promisedShipDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ExtendedPrice ExtendedPrice
        {
            get
            {
                return this.extendedPriceField;
            }
            set
            {
                this.extendedPriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public TotalAmount TotalAmount
        {
            get
            {
                return this.totalAmountField;
            }
            set
            {
                this.totalAmountField = value;
            }
        }

        /// <remarks/>
        public HeaderPaymentTerms PaymentTerms
        {
            get
            {
                return this.paymentTermsField;
            }
            set
            {
                this.paymentTermsField = value;
            }
        } 

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte DropShipInd
        {
            get
            {
                return this.dropShipIndField;
            }
            set
            {
                this.dropShipIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public Parties Parties
        {
            get
            {
                return this.partiesField;
            }
            set
            {
                this.partiesField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public Charges Charges
        {
            get
            {
                return this.chargesField;
            }
            set
            {
                this.chargesField = value;
            }
        }

        /// <remarks/>
        public ChargesAdditionalCharge AdditionalCharge
        {
            get
            {
                return this.additionalChargeField;
            }
            set
            {
                this.additionalChargeField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public Note Notes
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
        public partial class Note
        {
            private string langField;
            private string authorField;
           

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string lang
            {
                get
                {
                    return this.langField;
                }
                set
                {
                    this.langField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string author
            {
                get
                {
                    return this.authorField;
                }
                set
                {
                    this.authorField = value;
                }
            }

           
        }

        internal class Line
        {
        }
    }

    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    //public partial class LineOrderItemItemIdsSupplierItemId
    //{

    //    private ushort idField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    //    public ushort Id
    //    {
    //        get
    //        {
    //            return this.idField;
    //        }
    //        set
    //        {
    //            this.idField = value;
    //        }
    //    }
    //}
}

