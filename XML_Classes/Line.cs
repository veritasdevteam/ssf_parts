﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSF_AutoParts.XML_Classes
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
    public partial class Line
    {

        private byte lineNumberField;

        private LineOrderItem orderItemField;

        private byte orderQuantityField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte LineNumber
        {
            get
            {
                return this.lineNumberField;
            }
            set
            {
                this.lineNumberField = value;
            }
        }

        /// <remarks/>
        public LineOrderItem OrderItem
        {
            get
            {
                return this.orderItemField;
            }
            set
            {
                this.orderItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public byte OrderQuantity
        {
            get
            {
                return this.orderQuantityField;
            }
            set
            {
                this.orderQuantityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItem
    {

        private LineOrderItemItemIds itemIdsField;

        /// <remarks/>
        public LineOrderItemItemIds ItemIds
        {
            get
            {
                return this.itemIdsField;
            }
            set
            {
                this.itemIdsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItemItemIds
    {
        private CustomerItemId customerItemIdField;
        private LineOrderItemItemIdsSupplierItemId supplierItemIdField;
        private object manufacturerCodeField;
        private PrimaryShipFrom primaryShipFromField;
        private object catalogSearchField;
        private SearchId searchIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public CustomerItemId CustomerItemId
        {
            get
            {
                return this.customerItemIdField;
            }
            set
            {
                this.customerItemIdField = value;
            }
        }

        /// <remarks/>
        public LineOrderItemItemIdsSupplierItemId SupplierItemId
        {
            get
            {
                return this.supplierItemIdField;
            }
            set
            {
                this.supplierItemIdField = value;
            }
        }

        /// <remarks/>
        public object ManufacturerCode
        {
            get
            {
                return this.manufacturerCodeField;
            }
            set
            {
                this.manufacturerCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public PrimaryShipFrom PrimaryShipFrom
        {
            get
            {
                return this.primaryShipFromField;
            }
            set
            {
                this.primaryShipFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public object CatalogSearch
        {
            get
            {
                return this.catalogSearchField;
            }
            set
            {
                this.catalogSearchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        public SearchId SearchId
        {
            get
            {
                return this.searchIdField;
            }
            set
            {
                this.searchIdField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
    public partial class LineOrderItemItemIdsSupplierItemId
    {
        private ushort? idField;
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        public ushort? Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class PrimaryShipFrom
    {
        private object locationField;

        /// <remarks/>
        public object Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }
    }

    public partial class CustomerItemId
    {
        private string _id;
        public string Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
    public partial class SearchId
    {

        private byte sequenceField;

        /// <remarks/>
        public byte Sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }
    }
  
    public partial class PrimaryShipFrom
    {
       
    }


}
