﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSF_AutoParts.XML_Classes
{

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class Parties
    {

        private PartiesBillToParty billToPartyField;

        private PartiesShipToParty shipToPartyField;

        private PartiesShipFromParty shipFromPartyField;

        /// <remarks/>
        public PartiesBillToParty BillToParty
        {
            get
            {
                return this.billToPartyField;
            }
            set
            {
                this.billToPartyField = value;
            }
        }

        /// <remarks/>
        public PartiesShipToParty ShipToParty
        {
            get
            {
                return this.shipToPartyField;
            }
            set
            {
                this.shipToPartyField = value;
            }
        }

        /// <remarks/>
        public PartiesShipFromParty ShipFromParty
        {
            get
            {
                return this.shipFromPartyField;
            }
            set
            {
                this.shipFromPartyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesBillToParty
    {

        private PartiesBillToPartyPartyId partyIdField;

        private PartiesBillToPartyName nameField;

        private PartiesBillToPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public PartiesBillToPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public PartiesBillToPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public PartiesBillToPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesBillToPartyPartyId
    {

        private uint? idField;

        /// <remarks/>
        public uint? Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesBillToPartyName
    {

        private string langField;
        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesBillToPartyAddresses
    {

        private PartiesBillToPartyAddressesAddress addressField;

        /// <remarks/>
        public PartiesBillToPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesBillToPartyAddressesAddress
    {

        private string[] addressLineField;

        private string cityField;

        private string stateOrProvinceField;

        private string countryField;

        private string postalCodeField;

        private object telephoneField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public object Telephone
        {
            get
            {
                return this.telephoneField;
            }
            set
            {
                this.telephoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipToParty
    {

        private PartiesShipToPartyPartyId partyIdField;

        private PartiesShipToPartyName nameField;

        private PartiesShipToPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public PartiesShipToPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public PartiesShipToPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public PartiesShipToPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipToPartyPartyId
    {

        private uint idField;

        /// <remarks/>
        public uint Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipToPartyName
    {
        private string valueField;
        private string langField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipToPartyAddresses
    {

        private PartiesShipToPartyAddressesAddress addressField;

        /// <remarks/>
        public PartiesShipToPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipToPartyAddressesAddress
    {

        private string[] addressLineField;

        private string cityField;

        private string stateOrProvinceField;

        private string countryField;

        private string postalCodeField;

        private object telephoneField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public object Telephone
        {
            get
            {
                return this.telephoneField;
            }
            set
            {
                this.telephoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipFromParty
    {
        private PartiesShipFromPartyPartyId partyIdField;

        private PartiesShipFromPartyName nameField;

        private PartiesShipFromPartyAddresses addressesField;

        private byte activeField;

        private byte oneTimeField;

        /// <remarks/>
        public PartiesShipFromPartyPartyId PartyId
        {
            get
            {
                return this.partyIdField;
            }
            set
            {
                this.partyIdField = value;
            }
        }

        /// <remarks/>
        public PartiesShipFromPartyName Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public PartiesShipFromPartyAddresses Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte oneTime
        {
            get
            {
                return this.oneTimeField;
            }
            set
            {
                this.oneTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipFromPartyPartyId
    {

        private string idField;

        /// <remarks/>
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipFromPartyName
    {
        private string langField;
        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipFromPartyAddresses
    {

        private PartiesShipFromPartyAddressesAddress addressField;

        /// <remarks/>
        public PartiesShipFromPartyAddressesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class PartiesShipFromPartyAddressesAddress
    {
        private string[] addressLineField;
        private string cityField;
        private string stateOrProvinceField;
        private string countryField;
        private uint postalCodeField;
        private object telephoneField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string StateOrProvince
        {
            get
            {
                return this.stateOrProvinceField;
            }
            set
            {
                this.stateOrProvinceField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public uint PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public object Telephone
        {
            get
            {
                return this.telephoneField;
            }
            set
            {
                this.telephoneField = value;
            }
        }
    }
}






