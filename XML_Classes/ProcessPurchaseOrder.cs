﻿using System;
using SSF_AutoParts.XML_Classes;


/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
public partial class ProcessPurchaseOrder
{
    private ApplicationArea applicationAreaField;
    private DataArea dataAreaField;
    private string revisionField;
    private string environmentField;
    private string langField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public ApplicationArea ApplicationArea
    {
        get
        {
            return this.applicationAreaField;
        }
        set
        {
            this.applicationAreaField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public DataArea DataArea
    {
        get
        {
            return this.dataAreaField;
        }
        set
        {
            this.dataAreaField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string revision
    {
        get
        {
            return this.revisionField;
        }
        set
        {
            this.revisionField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string environment
    {
        get
        {
            return this.environmentField;
        }
        set
        {
            this.environmentField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string lang
    {
        get
        {
            return this.langField;
        }
        set
        {
            this.langField = value;
        }
    }
}

public partial class ApplicationArea
{   
}




public partial class DataArea
{
    private DataAreaProcess processField;
    private DataAreaPurchaseOrder purchaseOrderField;

    /// <remarks/>
    public DataAreaProcess Process
    {
        get
        {
            return this.processField;
        }
        set
        {
            this.processField = value;
        }
    }

    /// <remarks/>
    public DataAreaPurchaseOrder PurchaseOrder
    {
        get
        {
            return this.purchaseOrderField;
        }
        set
        {
            this.purchaseOrderField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DataAreaProcess
{

    private string confirmField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string confirm
    {
        get
        {
            return this.confirmField;
        }
        set
        {
            this.confirmField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DataAreaPurchaseOrder
{
    private Header headerField;     

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.aftermarket.org/oagis")]
    public Header Header
    {
        get
        {
            return this.headerField;
        }
        set
        {
            this.headerField = value;
        }
    } 
}  

   

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ChargesAdditionalCharge
    {      
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ChargesAdditionalChargeTotal
    {
        private string currencyField;
        private byte valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ChargesAdditionalChargeDescription
    {

        private string langField;

        private string ownerField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

   


