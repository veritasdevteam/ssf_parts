﻿using System;
using System.Text;
using System.IO;
using SSF_AutoParts.SQL;
using SSF_AutoParts.XML_Classes;
using SSF_AutoParts.Functions;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Configuration;

namespace SSF_AutoParts.XML_Classes
{
    public static class ProcessPurchaseOrder_Builder
    {
        public static ProcessPurchaseOrder ProcessPurchaseOrder()
        {
            ProcessPurchaseOrder ppo = new();
            ppo.ApplicationArea = fillApplicationArea();
            ppo.DataArea = fillDataArea();
            ppo.lang = "en";
            ppo.revision = "1";

            if (Globals.IsTest)
            {
                writeClassToFile(ppo);
            }
            return ppo;
        }

        private static ApplicationArea fillApplicationArea()
        {
            ApplicationArea aa = new();
            aa.BODId = Globals.BODId;
            aa.CreationDateTime = System.DateTime.Now;
            aa.Sender = getSender();
            
            return aa;
        }

        private static DataArea fillDataArea()
        {
            DataArea da = new();
            da.Add = new();
            da.Add.confirm = "Always";
            da.PurchaseOrder = fillPurchaseOrder();
            da.RequestForQuote = fillRequestForQuote();
            da.Process = new();
            da.Process.confirm = "Always";
          
            return da;
        }

        private static ApplicationArea.ApplicationAreaSender getSender()
        {
            ApplicationArea.ApplicationAreaSender aas = new();
            aas.Confirmation = 1;
            aas.ReferenceId = null; // TJF??
            return aas;
        }

        private static DataAreaPurchaseOrder fillPurchaseOrder()
        {
            DataAreaPurchaseOrder po = new();
            po.Header = fillHeader();
            
            return po;
        }
        private static Header fillHeader()
        {
            Header head = new();
            head.AdditionalCharge = fillAdditionalCharge();
            head.Charges = fillCharges();
            head.DocumentIds = fillDocumentIds();
            head.DocumentReferences = fillDocumentReferences();
            head.DropShipInd = 0; // ?
            head.ExtendedPrice = fillExtendedPrice();           
            head.Notes = fillNote();
            head.Parties = fillParties();
            head.PaymentTerms = fillPaymentTerms();          
            head.TotalAmount = fillTotalAmount();
           
            //  Need to know what value for these dates:
            head.DocumentDateTime = System.DateTime.Now;
            head.LastModificationDateTime = System.DateTime.Now;
            head.PromisedShipDate = System.DateTime.Now;
            return head;
        }

        private static ChargesAdditionalCharge fillAdditionalCharge()
        {
            ChargesAdditionalCharge ch = new();

            ChargesAdditionalChargeDescription desc = new();
            desc.Value = "Shipping";
            desc.lang = "en";
            desc.owner = "";
            ch.Description = desc;

            ChargesAdditionalChargeTotal tot = new();
            tot.Value = 0;
            tot.currency = "USD";
            ch.Total = tot;

            return ch;
        }

        private static DocumentIds fillDocumentIds()
        {
            DocumentIds ids = new();
            ids.CustomerDocumentId = new();
            ids.CustomerDocumentId.Revision = 1;
            ids.CustomerDocumentId.Id = null;
            //
            ids.SupplierDocumentId = new();
            ids.SupplierDocumentId.Revision = 1;
            ids.SupplierDocumentId.Id = null;

            return ids;
        }

        private static DocumentReferences fillDocumentReferences()
        {
            DocumentReferences doc = new();
            doc.RFQDocumentReference = new();
            doc.RFQDocumentReference.DocumentIds = new();
            doc.RFQDocumentReference.DocumentIds.CustomerDocumentId = null;
            return doc;
        }

        private static ExtendedPrice fillExtendedPrice()
        { 
            ExtendedPrice ep = new();
            ep.currency = "USD";
            return ep;
        }

        private static TotalAmount fillTotalAmount()
        {
            TotalAmount ta = new();
            ta.currency = "USD";
            return ta;
        }

        private static Header.Note fillNote()
        {
            Header.Note note = new();
            note.author = "Delivery Notes";
            note.lang = "en";
            return note;
        }

        private static HeaderPaymentTerms fillPaymentTerms()
        {
            HeaderPaymentTerms pt = new();
            pt.TermId = "C";
            return pt;
        }

        private static DataAreaRequestForQuote fillRequestForQuote()
        {
            DataAreaRequestForQuote rq = new();
            rq.Header = new();
            rq.Header.PaymentTerms = fillPaymentTerms();
            rq.Header.Charges = fillCharges();
            rq.Header.DocumentDateTime = System.DateTime.Now;
            rq.Header.LastModificationDateTime = System.DateTime.Now;
            rq.Header.DropShipInd = 0;
           
            return rq;
        }

        private static Parties fillParties()
        {
            Parties p = new();            
            p.BillToParty = fillBillTo();
            p.ShipToParty = fillShipTo();
            p.ShipFromParty = fillShipFrom();

            return p;
        }

        private static XML_Classes.PartiesBillToParty fillBillTo()
        {
            XML_Classes.PartiesBillToParty bill = new();
            bill.active = 1;
            bill.Addresses = getBillToAddresses();
            bill.PartyId = getBillToPartyID();
            bill.Name = getBillToName();
            return bill;
        }


        private static PartiesBillToPartyPartyId getBillToPartyID()
        {
            PartiesBillToPartyPartyId part = new();
            string partyID = ConfigurationManager.AppSettings.Get("BillToPartyID");
            part.Id = ushort.Parse(partyID);
            return part;
        }

        private static PartiesBillToPartyName getBillToName()
        {
            PartiesBillToPartyName name = new();
            name.lang = "en";
            name.Value = ConfigurationManager.AppSettings.Get("BillToName");
            return name;
        }

        private static XML_Classes.PartiesBillToPartyAddresses getBillToAddresses()
        {
            PartiesBillToPartyAddressesAddress address = new();
            address.AddressLine = getBillToAddressLine();
            address.City = ConfigurationManager.AppSettings.Get("BillToCity");
            address.StateOrProvince = ConfigurationManager.AppSettings.Get("BillToState");
            address.PostalCode = ConfigurationManager.AppSettings.Get("BillToZIP");
            address.Country = ConfigurationManager.AppSettings.Get("BillToCountry");
            address.Telephone = ConfigurationManager.AppSettings.Get("BillToPhone");
            PartiesBillToPartyAddresses add2 = new();
            add2.Address = address;
            return add2;
        }
        private static string[] getShipToAddressLine()
        {
            SQL_Select sel = new();
            string[] add1 = new string[1];
            add1[0] = SQL_Select.Ship.ShipToAdd1;
            return add1;
        }
        private static string[] getBillToAddressLine()
        {
            string[] add = new string[1];
            string address1 = ConfigurationManager.AppSettings.Get("BillToAddr1");
            add[0] = address1;
            return add;
        }
        public static XML_Classes.PartiesShipToParty fillShipTo()
        {
            SQL_Select sel = new();

            sel.FillServiceCenterData();

            PartiesShipToParty shipTo = new();
            shipTo.active = 1;
            shipTo.oneTime = 0;

            shipTo.Name = getShipToName();
            shipTo.Addresses = new();
            shipTo.Addresses.Address = new();
            shipTo.Addresses.Address.AddressLine = getShipToAddressLine();
            shipTo.Addresses.Address.City = SQL_Select.Ship.ShipToCity;
            shipTo.Addresses.Address.StateOrProvince = SQL_Select.Ship.ShipToState;
            shipTo.Addresses.Address.PostalCode = SQL_Select.Ship.ShipToPostalCode;
            shipTo.Addresses.Address.Country = SQL_Select.Ship.ShipToCountry;
            shipTo.Addresses.Address.Telephone = SQL_Select.Ship.ShipToPhone;

            return shipTo;
        }

        public static PartiesShipToPartyName getShipToName()
        {
            SQL_Select sel = new();
            sel.FillServiceCenterData();
            string name = SQL_Select.Ship.ShipToName;
            PartiesShipToPartyName pn = new();
            pn.lang = "en";
            pn.Value = name;

            return pn;
        }

        public static PartiesShipFromParty fillShipFrom()
        {
            PartiesShipFromParty shipFrom = new();

            // Attributes
            shipFrom.active = 1;
            shipFrom.oneTime = 0;

            // Actual tags
            shipFrom.PartyId = getShipFromPartyID();
            shipFrom.Name = getShipFromPartyName();
            shipFrom.Addresses = fillShipFromAddresses();

            return shipFrom;
        }

        private static PartiesShipFromPartyPartyId getShipFromPartyID()
        {
            PartiesShipFromPartyPartyId part = new();
            string partyID = ConfigurationManager.AppSettings.Get("ShipFromPartyID");
            part.Id = partyID;
            return part;
        }

        public static XML_Classes.PartiesShipToParty FillShipTo()
        {
            SQL_Select sel = new();

            sel.FillServiceCenterData();

            PartiesShipToParty shipTo = new();
            shipTo.active = 1;
            shipTo.oneTime = 0;

            shipTo.Name = getShipToName();
            shipTo.Addresses = new();
            shipTo.Addresses.Address = new();
            shipTo.Addresses.Address.AddressLine = getShipToAddressLine();
            shipTo.Addresses.Address.City = SQL_Select.Ship.ShipToCity;
            shipTo.Addresses.Address.StateOrProvince = SQL_Select.Ship.ShipToState;
            shipTo.Addresses.Address.PostalCode = SQL_Select.Ship.ShipToPostalCode;
            shipTo.Addresses.Address.Country = SQL_Select.Ship.ShipToCountry;
            shipTo.Addresses.Address.Telephone = SQL_Select.Ship.ShipToPhone;

            return shipTo;
        }    

        private static PartiesShipFromPartyAddresses fillShipFromAddresses()
        {
            PartiesShipFromPartyAddresses add = new();
            add.Address = new();
            add.Address.AddressLine = getShipFromAddressLine();
            add.Address.City = ConfigurationManager.AppSettings.Get("ShipFromCity");
            add.Address.StateOrProvince = ConfigurationManager.AppSettings.Get("ShipFromState");
            add.Address.PostalCode = convertPostalCodeToUInt();
            add.Address.Country = ConfigurationManager.AppSettings.Get("ShipFromCountry");
            add.Address.Telephone = ConfigurationManager.AppSettings.Get("ShipFromTelephone");

            return add;
        }

        private static string[] getShipFromAddressLine()
        {
            string[] add = new string[1];
            add[0] = ConfigurationManager.AppSettings.Get("ShipFromAddr1");
            return add;
        }

        private static uint convertPostalCodeToUInt()
        {
            string zip = ConfigurationManager.AppSettings.Get("ShipFromZIP");
            uint temp = Convert.ToUInt32(zip, 16);
            return temp;
        }

        private static PartiesShipFromPartyName getShipFromPartyName()
        {            
            PartiesShipFromParty party = new();
            PartiesShipFromPartyName name = new();
            string shipFromName = ConfigurationManager.AppSettings.Get("ShipFromName");
            name.Value = shipFromName;
            name.lang = "en";
            party.Name = name;
            return name;
        }

        private static Charges fillCharges()
        {
            Charges ch = new();
            ChargesAdditionalCharge ac = new();

            ChargesAdditionalChargeDescription desc = new();
            desc.owner = "Shipping";
            desc.lang = "en";
            ac.Description = desc;

            ChargesAdditionalChargeTotal tot = new();
            tot.currency = "USD";
            tot.Value = 0;
            ac.Total = tot;
            ch.AdditionalCharge = ac;
            return ch;
        }

        private static void writeClassToFile(ProcessPurchaseOrder xml)
        {
            string fullPath = Globals.TestPath + "Serialized_ProcessPO.xml";
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            string convertedXML = Static_Functs.SerializeObject(xml);
            convertedXML = new Regex("\\<\\?xml.*\\?>").Replace(convertedXML, "");
            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (convertedXML.StartsWith(_byteOrderMarkUtf8, StringComparison.Ordinal))
            {
                convertedXML = convertedXML.Remove(0, _byteOrderMarkUtf8.Length);
            }
            Globals.xDoc = new();
            Globals.XMLDoc.LoadXml(convertedXML);
            Globals.xDoc = XDocument.Parse(convertedXML);
            Globals.XMLDoc.Save(fullPath);
        } 
    }
}