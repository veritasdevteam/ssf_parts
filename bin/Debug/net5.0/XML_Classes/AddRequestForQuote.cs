﻿using System;
using SSF_AutoParts.XML_Classes;

[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
public partial class AddRequestForQuote
{
    private ApplicationArea applicationAreaField;
    private DataArea dataAreaField;
    private string revisionField;
    private string environmentField;
    private string langField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public ApplicationArea ApplicationArea
    {
        get
        {
            return this.applicationAreaField;
        }
        set
        {
            this.applicationAreaField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public DataArea DataArea
    {
        get
        {
            return this.dataAreaField;
        }
        set
        {
            this.dataAreaField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string revision
    {
        get
        {
            return this.revisionField;
        }
        set
        {
            this.revisionField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string environment
    {
        get
        {
            return this.environmentField;
        }
        set
        {
            this.environmentField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string lang
    {
        get
        {
            return this.langField;
        }
        set
        {
            this.langField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class ApplicationArea
{

    private ApplicationAreaSender senderField;

    private System.DateTime creationDateTimeField;

    private string bODIdField;

   
    public ApplicationAreaSender Sender
    {
        get
        {
            return this.senderField;
        }
        set
        {
            this.senderField = value;
        }
    }

    /// <remarks/>
    public System.DateTime CreationDateTime
    {
        get
        {
            return this.creationDateTimeField;
        }
        set
        {
            this.creationDateTimeField = value;
        }
    }

    /// <remarks/>
    public string BODId
    {
        get
        {
            return this.bODIdField;
        }
        set
        {
            this.bODIdField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class ApplicationAreaSender
{

    private string referenceIdField;

    private byte confirmationField;

   
    public string ReferenceId
    {
        get
        {
            return this.referenceIdField;
        }
        set
        {
            this.referenceIdField = value;
        }
    }

   
    public byte Confirmation
    {
        get
        {
            return this.confirmationField;
        }
        set
        {
            this.confirmationField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class DataArea
{

    private DataAreaAdd addField;

    private DataAreaRequestForQuote requestForQuoteField;

    /// <remarks/>
    public DataAreaAdd Add
    {
        get
        {
            return this.addField;
        }
        set
        {
            this.addField = value;
        }
    }

    /// <remarks/>
    public DataAreaRequestForQuote RequestForQuote
    {
        get
        {
            return this.requestForQuoteField;
        }
        set
        {
            this.requestForQuoteField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DataAreaAdd
{

    private string confirmField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string confirm
    {
        get
        {
            return this.confirmField;
        }
        set
        {
            this.confirmField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DataAreaRequestForQuote
{
    private Header headerField;
    private Line lineField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.aftermarket.org/oagis")]
    public Header Header
    {
        get
        {
            return this.headerField;
        }
        set
        {
            this.headerField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.aftermarket.org/oagis")]
    public Line Line
    {
        get
        {
            return this.lineField;
        }
        set
        {
            this.lineField = value;
        }
    }
}



/// <remarks/>
//[System.SerializableAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
//[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class DocumentIds
{

    private DocumentIdsCustomerDocumentId customerDocumentIdField;

    private DocumentIdsSupplierDocumentId supplierDocumentIdField;

    ///// <remarks/>
    public DocumentIdsCustomerDocumentId CustomerDocumentId
    {
        get
        {
            return this.customerDocumentIdField;
        }
        set
        {
            this.customerDocumentIdField = value;
        }
    }
  
    public DocumentIdsSupplierDocumentId SupplierDocumentId
    {
        get
        {
            return this.supplierDocumentIdField;
        }
        set
        {
            this.supplierDocumentIdField = value;
        }
    }
}

/// <remarks/>
//[System.SerializableAttribute()]
//[System.ComponentModel.DesignerCategoryAttribute("code")]
//[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DocumentIdsCustomerDocumentId
{

    private object idField;

    private byte revisionField;
   
    public object Id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    public byte Revision
    {
        get
        {
            return this.revisionField;
        }
        set
        {
            this.revisionField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DocumentIdsSupplierDocumentId
{

    private object idField;

    private byte revisionField;

    /// <remarks/>
    public object Id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    public byte Revision
    {
        get
        {
            return this.revisionField;
        }
        set
        {
            this.revisionField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class DocumentReferences
{
    private DocumentReferencesRFQDocumentReference rFQDocumentReferenceField;

    /// <remarks/>
    public DocumentReferencesRFQDocumentReference RFQDocumentReference
    {
        get
        {
            return this.rFQDocumentReferenceField;
        }
        set
        {
            this.rFQDocumentReferenceField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DocumentReferencesRFQDocumentReference
{

    private DocumentReferencesRFQDocumentReferenceDocumentIds documentIdsField;

    /// <remarks/>
    public DocumentReferencesRFQDocumentReferenceDocumentIds DocumentIds
    {
        get
        {
            return this.documentIdsField;
        }
        set
        {
            this.documentIdsField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DocumentReferencesRFQDocumentReferenceDocumentIds
{

    private DocumentReferencesRFQDocumentReferenceDocumentIdsCustomerDocumentId customerDocumentIdField;

    /// <remarks/>
    public DocumentReferencesRFQDocumentReferenceDocumentIdsCustomerDocumentId CustomerDocumentId
    {
        get
        {
            return this.customerDocumentIdField;
        }
        set
        {
            this.customerDocumentIdField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
public partial class DocumentReferencesRFQDocumentReferenceDocumentIdsCustomerDocumentId
{

    private ushort idField;

    /// <remarks/>
    public ushort Id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class ExtendedPrice
{

    private string currencyField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string currency
    {
        get
        {
            return this.currencyField;
        }
        set
        {
            this.currencyField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
public partial class TotalAmount
{

    private string currencyField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string currency
    {
        get
        {
            return this.currencyField;
        }
        set
        {
            this.currencyField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
public partial class HeaderPaymentTerms
{

    private object termIdField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
    public object TermId
    {
        get
        {
            return this.termIdField;
        }
        set
        {
            this.termIdField = value;
        }
    }
}


/// <remarks/>
public partial class ChargesAdditionalCharge
{

    private ChargesAdditionalChargeTotal totalField;

    private ChargesAdditionalChargeDescription descriptionField;


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ChargesAdditionalChargeTotal
    {

        private string currencyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    public partial class ChargesAdditionalChargeDescription
    {

        private string langField;
        private string ownerField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    public partial class Note
    {
        private string langField;
        private string authorField;
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lang
        {
            get
            {
                return this.langField;
            }
            set
            {
                this.langField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }
    }

    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
    //public partial class Parties
    //{

    //    private PartiesBillToParty billToPartyField;
    //    private PartiesShipToParty shipToPartyField;
    //    private PartiesShipFromParty shipFromPartyField;
    //    /// <remarks/>
    //    public PartiesBillToParty BillToParty
    //    {
    //        get
    //        {
    //            return this.billToPartyField;
    //        }
    //        set
    //        {
    //            this.billToPartyField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public PartiesShipToParty ShipToParty
    //    {
    //        get
    //        {
    //            return this.shipToPartyField;
    //        }
    //        set
    //        {
    //            this.shipToPartyField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public PartiesShipFromParty ShipFromParty
    //    {
    //        get
    //        {
    //            return this.shipFromPartyField;
    //        }
    //        set
    //        {
    //            this.shipFromPartyField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
    //public partial class PartiesBillToParty
    //{

    //    private PartiesBillToPartyPartyId partyIdField;
    //    private PartiesBillToPartyName nameField;
    //    private PartiesBillToPartyAddresses addressesField;
    //    private byte activeField;
    //    private byte oneTimeField;
    //    /// <remarks/>
    //    public PartiesBillToPartyPartyId PartyId
    //    {
    //        get
    //        {
    //            return this.partyIdField;
    //        }
    //        set
    //        {
    //            this.partyIdField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public PartiesBillToPartyName Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public PartiesBillToPartyAddresses Addresses
    //    {
    //        get
    //        {
    //            return this.addressesField;
    //        }
    //        set
    //        {
    //            this.addressesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte active
    //    {
    //        get
    //        {
    //            return this.activeField;
    //        }
    //        set
    //        {
    //            this.activeField = value;
    //        }
    //    }


        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
        //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.aftermarket.org/oagis", IsNullable = false)]
        //public partial class Line
        //{

        //    private byte lineNumberField;

        //    private LineOrderItem orderItemField;

        //    private byte orderQuantityField;

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        //    public byte LineNumber
        //    {
        //        get
        //        {
        //            return this.lineNumberField;
        //        }
        //        set
        //        {
        //            this.lineNumberField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public LineOrderItem OrderItem
        //    {
        //        get
        //        {
        //            return this.orderItemField;
        //        }
        //        set
        //        {
        //            this.orderItemField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        //    public byte OrderQuantity
        //    {
        //        get
        //        {
        //            return this.orderQuantityField;
        //        }
        //        set
        //        {
        //            this.orderQuantityField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
        //public partial class LineOrderItem
        //{

        //    private LineOrderItemItemIds itemIdsField;

        //    /// <remarks/>
        //    public LineOrderItemItemIds ItemIds
        //    {
        //        get
        //        {
        //            return this.itemIdsField;
        //        }
        //        set
        //        {
        //            this.itemIdsField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
        //public partial class LineOrderItemItemIds
        //{

        //    private CustomerItemId customerItemIdField;

        //    private LineOrderItemItemIdsSupplierItemId supplierItemIdField;

        //    private object manufacturerCodeField;

        //    private PrimaryShipFrom primaryShipFromField;

        //    private object catalogSearchField;

        //    private SearchId searchIdField;

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        //    public CustomerItemId CustomerItemId
        //    {
        //        get
        //        {
        //            return this.customerItemIdField;
        //        }
        //        set
        //        {
        //            this.customerItemIdField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public LineOrderItemItemIdsSupplierItemId SupplierItemId
        //    {
        //        get
        //        {
        //            return this.supplierItemIdField;
        //        }
        //        set
        //        {
        //            this.supplierItemIdField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public object ManufacturerCode
        //    {
        //        get
        //        {
        //            return this.manufacturerCodeField;
        //        }
        //        set
        //        {
        //            this.manufacturerCodeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        //    public PrimaryShipFrom PrimaryShipFrom
        //    {
        //        get
        //        {
        //            return this.primaryShipFromField;
        //        }
        //        set
        //        {
        //            this.primaryShipFromField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        //    public object CatalogSearch
        //    {
        //        get
        //        {
        //            return this.catalogSearchField;
        //        }
        //        set
        //        {
        //            this.catalogSearchField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://www.ssfparts.com/ssfconnect")]
        //    public SearchId SearchId
        //    {
        //        get
        //        {
        //            return this.searchIdField;
        //        }
        //        set
        //        {
        //            this.searchIdField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.openapplications.org/oagis")]
        //[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.openapplications.org/oagis", IsNullable = false)]
        //public partial class CustomerItemId
        //{

        //    private object idField;

        //    /// <remarks/>
        //    public object Id
        //    {
        //        get
        //        {
        //            return this.idField;
        //        }
        //        set
        //        {
        //            this.idField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.aftermarket.org/oagis")]
        //public partial class LineOrderItemItemIdsSupplierItemId
        //{

        //    private string idField;

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.openapplications.org/oagis")]
        //    public string Id
        //    {
        //        get
        //        {
        //            return this.idField;
        //        }
        //        set
        //        {
        //            this.idField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
        //[System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
        //public partial class PrimaryShipFrom
        //{

        //    private object locationField;

        //    /// <remarks/>
        //    public object Location
        //    {
        //        get
        //        {
        //            return this.locationField;
        //        }
        //        set
        //        {
        //            this.locationField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://www.ssfparts.com/ssfconnect")]
        //[System.Xml.Serialization.XmlRootAttribute(Namespace = "https://www.ssfparts.com/ssfconnect", IsNullable = false)]
        //public partial class SearchId
        //{

        //    private byte sequenceField;

        //    /// <remarks/>
        //    public byte Sequence
        //    {
        //        get
        //        {
        //            return this.sequenceField;
        //        }
        //        set
        //        {
        //            this.sequenceField = value;
        //        }
        //    }
        //}
    }


